package com.dms.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Documententry.
 */
@Entity
@Table(name = "documententry")
public class Documententry implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 30)
    @Column(name = "document_code", length = 30, nullable = false, unique = true)
    private String documentCode;

    @NotNull
    @Size(max = 60)
    @Column(name = "document_name", length = 60, nullable = false)
    private String documentName;

    @NotNull
    @Column(name = "from_field", nullable = false)
    private LocalDate fromField;

    @NotNull
    @Column(name = "to_field", nullable = false)
    private LocalDate toField;

    @Size(max = 300)
    @Column(name = "notes", length = 300)
    private String notes;

    
    @Lob
    @Column(name = "content", nullable = false)
    private byte[] content;

    @Column(name = "content_content_type", nullable = false)
    private String contentContentType;

    @NotNull
    @Size(max = 10)
    @Column(name = "from_to_1", length = 10, nullable = false)
    private String fromTo1;

    @Size(max = 10)
    @Column(name = "from_to_2", length = 10)
    private String fromTo2;

    @Size(max = 10)
    @Column(name = "from_to_3", length = 10)
    private String fromTo3;

    @Size(max = 10)
    @Column(name = "from_to_4", length = 10)
    private String fromTo4;

    @Size(max = 10)
    @Column(name = "from_to_5", length = 10)
    private String fromTo5;

    @Size(max = 10)
    @Column(name = "from_to_6", length = 10)
    private String fromTo6;

    @Size(max = 10)
    @Column(name = "from_to_7", length = 10)
    private String fromTo7;

    @Size(max = 10)
    @Column(name = "from_to_8", length = 10)
    private String fromTo8;

    @Size(max = 10)
    @Column(name = "from_to_9", length = 10)
    private String fromTo9;

    @OneToOne    @JoinColumn(unique = true)
    private Documenttype documenttypeForfromTo1;

    @OneToOne    @JoinColumn(unique = true)
    private Documenttype documenttypeForfromTo2;

    @OneToOne    @JoinColumn(unique = true)
    private Documenttype documenttypeForfromTo3;

    @OneToOne    @JoinColumn(unique = true)
    private Documenttype documenttypeForfromTo4;

    @OneToOne    @JoinColumn(unique = true)
    private Documenttype documenttypeForfromTo5;

    @OneToOne    @JoinColumn(unique = true)
    private Documenttype documenttypeForfromTo6;

    @OneToOne    @JoinColumn(unique = true)
    private Patient patient;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocumentCode() {
        return documentCode;
    }

    public Documententry documentCode(String documentCode) {
        this.documentCode = documentCode;
        return this;
    }

    public void setDocumentCode(String documentCode) {
        this.documentCode = documentCode;
    }

    public String getDocumentName() {
        return documentName;
    }

    public Documententry documentName(String documentName) {
        this.documentName = documentName;
        return this;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public LocalDate getFromField() {
        return fromField;
    }

    public Documententry fromField(LocalDate fromField) {
        this.fromField = fromField;
        return this;
    }

    public void setFromField(LocalDate fromField) {
        this.fromField = fromField;
    }

    public LocalDate getToField() {
        return toField;
    }

    public Documententry toField(LocalDate toField) {
        this.toField = toField;
        return this;
    }

    public void setToField(LocalDate toField) {
        this.toField = toField;
    }

    public String getNotes() {
        return notes;
    }

    public Documententry notes(String notes) {
        this.notes = notes;
        return this;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public byte[] getContent() {
        return content;
    }

    public Documententry content(byte[] content) {
        this.content = content;
        return this;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getContentContentType() {
        return contentContentType;
    }

    public Documententry contentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
        return this;
    }

    public void setContentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
    }

    public String getFromTo1() {
        return fromTo1;
    }

    public Documententry fromTo1(String fromTo1) {
        this.fromTo1 = fromTo1;
        return this;
    }

    public void setFromTo1(String fromTo1) {
        this.fromTo1 = fromTo1;
    }

    public String getFromTo2() {
        return fromTo2;
    }

    public Documententry fromTo2(String fromTo2) {
        this.fromTo2 = fromTo2;
        return this;
    }

    public void setFromTo2(String fromTo2) {
        this.fromTo2 = fromTo2;
    }

    public String getFromTo3() {
        return fromTo3;
    }

    public Documententry fromTo3(String fromTo3) {
        this.fromTo3 = fromTo3;
        return this;
    }

    public void setFromTo3(String fromTo3) {
        this.fromTo3 = fromTo3;
    }

    public String getFromTo4() {
        return fromTo4;
    }

    public Documententry fromTo4(String fromTo4) {
        this.fromTo4 = fromTo4;
        return this;
    }

    public void setFromTo4(String fromTo4) {
        this.fromTo4 = fromTo4;
    }

    public String getFromTo5() {
        return fromTo5;
    }

    public Documententry fromTo5(String fromTo5) {
        this.fromTo5 = fromTo5;
        return this;
    }

    public void setFromTo5(String fromTo5) {
        this.fromTo5 = fromTo5;
    }

    public String getFromTo6() {
        return fromTo6;
    }

    public Documententry fromTo6(String fromTo6) {
        this.fromTo6 = fromTo6;
        return this;
    }

    public void setFromTo6(String fromTo6) {
        this.fromTo6 = fromTo6;
    }

    public String getFromTo7() {
        return fromTo7;
    }

    public Documententry fromTo7(String fromTo7) {
        this.fromTo7 = fromTo7;
        return this;
    }

    public void setFromTo7(String fromTo7) {
        this.fromTo7 = fromTo7;
    }

    public String getFromTo8() {
        return fromTo8;
    }

    public Documententry fromTo8(String fromTo8) {
        this.fromTo8 = fromTo8;
        return this;
    }

    public void setFromTo8(String fromTo8) {
        this.fromTo8 = fromTo8;
    }

    public String getFromTo9() {
        return fromTo9;
    }

    public Documententry fromTo9(String fromTo9) {
        this.fromTo9 = fromTo9;
        return this;
    }

    public void setFromTo9(String fromTo9) {
        this.fromTo9 = fromTo9;
    }

    public Documenttype getDocumenttypeForfromTo1() {
        return documenttypeForfromTo1;
    }

    public Documententry documenttypeForfromTo1(Documenttype documenttype) {
        this.documenttypeForfromTo1 = documenttype;
        return this;
    }

    public void setDocumenttypeForfromTo1(Documenttype documenttype) {
        this.documenttypeForfromTo1 = documenttype;
    }

    public Documenttype getDocumenttypeForfromTo2() {
        return documenttypeForfromTo2;
    }

    public Documententry documenttypeForfromTo2(Documenttype documenttype) {
        this.documenttypeForfromTo2 = documenttype;
        return this;
    }

    public void setDocumenttypeForfromTo2(Documenttype documenttype) {
        this.documenttypeForfromTo2 = documenttype;
    }

    public Documenttype getDocumenttypeForfromTo3() {
        return documenttypeForfromTo3;
    }

    public Documententry documenttypeForfromTo3(Documenttype documenttype) {
        this.documenttypeForfromTo3 = documenttype;
        return this;
    }

    public void setDocumenttypeForfromTo3(Documenttype documenttype) {
        this.documenttypeForfromTo3 = documenttype;
    }

    public Documenttype getDocumenttypeForfromTo4() {
        return documenttypeForfromTo4;
    }

    public Documententry documenttypeForfromTo4(Documenttype documenttype) {
        this.documenttypeForfromTo4 = documenttype;
        return this;
    }

    public void setDocumenttypeForfromTo4(Documenttype documenttype) {
        this.documenttypeForfromTo4 = documenttype;
    }

    public Documenttype getDocumenttypeForfromTo5() {
        return documenttypeForfromTo5;
    }

    public Documententry documenttypeForfromTo5(Documenttype documenttype) {
        this.documenttypeForfromTo5 = documenttype;
        return this;
    }

    public void setDocumenttypeForfromTo5(Documenttype documenttype) {
        this.documenttypeForfromTo5 = documenttype;
    }

    public Documenttype getDocumenttypeForfromTo6() {
        return documenttypeForfromTo6;
    }

    public Documententry documenttypeForfromTo6(Documenttype documenttype) {
        this.documenttypeForfromTo6 = documenttype;
        return this;
    }

    public void setDocumenttypeForfromTo6(Documenttype documenttype) {
        this.documenttypeForfromTo6 = documenttype;
    }

    public Patient getPatient() {
        return patient;
    }

    public Documententry patient(Patient patient) {
        this.patient = patient;
        return this;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Documententry documententry = (Documententry) o;
        if (documententry.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), documententry.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Documententry{" +
            "id=" + getId() +
            ", documentCode='" + getDocumentCode() + "'" +
            ", documentName='" + getDocumentName() + "'" +
            ", fromField='" + getFromField() + "'" +
            ", toField='" + getToField() + "'" +
            ", notes='" + getNotes() + "'" +
            ", content='" + getContent() + "'" +
            ", contentContentType='" + getContentContentType() + "'" +
            ", fromTo1='" + getFromTo1() + "'" +
            ", fromTo2='" + getFromTo2() + "'" +
            ", fromTo3='" + getFromTo3() + "'" +
            ", fromTo4='" + getFromTo4() + "'" +
            ", fromTo5='" + getFromTo5() + "'" +
            ", fromTo6='" + getFromTo6() + "'" +
            ", fromTo7='" + getFromTo7() + "'" +
            ", fromTo8='" + getFromTo8() + "'" +
            ", fromTo9='" + getFromTo9() + "'" +
            "}";
    }
}
