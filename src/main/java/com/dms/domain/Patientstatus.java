package com.dms.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Patientstatus.
 */
@Entity
@Table(name = "patientstatus")
public class Patientstatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 20)
    @Column(name = "patient_status_code", length = 20, nullable = false, unique = true)
    private String patientStatusCode;

    @NotNull
    @Size(max = 20)
    @Column(name = "patient_status_name", length = 20, nullable = false)
    private String patientStatusName;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPatientStatusCode() {
        return patientStatusCode;
    }

    public Patientstatus patientStatusCode(String patientStatusCode) {
        this.patientStatusCode = patientStatusCode;
        return this;
    }

    public void setPatientStatusCode(String patientStatusCode) {
        this.patientStatusCode = patientStatusCode;
    }

    public String getPatientStatusName() {
        return patientStatusName;
    }

    public Patientstatus patientStatusName(String patientStatusName) {
        this.patientStatusName = patientStatusName;
        return this;
    }

    public void setPatientStatusName(String patientStatusName) {
        this.patientStatusName = patientStatusName;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Patientstatus patientstatus = (Patientstatus) o;
        if (patientstatus.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), patientstatus.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Patientstatus{" +
            "id=" + getId() +
            ", patientStatusCode='" + getPatientStatusCode() + "'" +
            ", patientStatusName='" + getPatientStatusName() + "'" +
            "}";
    }
}
