package com.dms.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Documenttype.
 */
@Entity
@Table(name = "documenttype")
public class Documenttype implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "document_type_name", length = 50, nullable = false)
    private String documentTypeName;

    @NotNull
    @Column(name = "document_type_category", nullable = false)
    private Integer documentTypeCategory;

    @ManyToOne
    @JsonIgnoreProperties("documenttypes")
    private Practice practice;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocumentTypeName() {
        return documentTypeName;
    }

    public Documenttype documentTypeName(String documentTypeName) {
        this.documentTypeName = documentTypeName;
        return this;
    }

    public void setDocumentTypeName(String documentTypeName) {
        this.documentTypeName = documentTypeName;
    }

    public Integer getDocumentTypeCategory() {
        return documentTypeCategory;
    }

    public Documenttype documentTypeCategory(Integer documentTypeCategory) {
        this.documentTypeCategory = documentTypeCategory;
        return this;
    }

    public void setDocumentTypeCategory(Integer documentTypeCategory) {
        this.documentTypeCategory = documentTypeCategory;
    }

    public Practice getPractice() {
        return practice;
    }

    public Documenttype practice(Practice practice) {
        this.practice = practice;
        return this;
    }

    public void setPractice(Practice practice) {
        this.practice = practice;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Documenttype documenttype = (Documenttype) o;
        if (documenttype.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), documenttype.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Documenttype{" +
            "id=" + getId() +
            ", documentTypeName='" + getDocumentTypeName() + "'" +
            ", documentTypeCategory=" + getDocumentTypeCategory() +
            "}";
    }
}
