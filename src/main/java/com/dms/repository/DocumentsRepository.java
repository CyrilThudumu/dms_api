package com.dms.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dms.domain.Documents;

/**
 * Spring Data  repository for the Documents entity.
 */
@Repository
public interface DocumentsRepository extends JpaRepository<Documents, Long>, JpaSpecificationExecutor<Documents> {

	
	
	/**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param doctorCode  The given doctorCode.
     * @param pageRequest   The information of the requested page.
     * @return  A page of Documents entries when the doctorCode matches . The content of
     *          the returned page depends from the page request given as a method parameter.
     */
	
	 
    @Query("select documents from Documents documents  where documents.doctorCode =:doctorCode")
    Page<Documents> findByDocumentName(@Param("doctorCode") String doctorCode, Pageable page);
    
    /**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param patientCode  The given patientCode.
     * @return  A page of Documents entries whose toDos and fromDOS for a patient Code. The content of
     *          the returned page depends from the page request given as a method parameter.
     */
    
    @Query("select documents from Documents documents  where documents.patientCode =:patientCode and documents.practiceCode = :practiceCode")
    Page<Documents> getDOSForPatient(@Param("patientCode") String patientCode,@Param("practiceCode") String practiceCode, Pageable page);
   
    /**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param doctorCode  The given patientCode.
     * @return  A page of Documents entries whose toDos and fromDOS for a patient Code. The content of
     *          the returned page depends from the page request given as a method parameter.
     */
    @Query("select documents from Documents documents  where documents.doctorCode =:doctorCode and documents.patientCode = :patientCode")
    Page<Documents> getDOSForDoctor(@Param("doctorCode") String doctorCode,@Param("patientCode") String practiceCode, Pageable page);
   
    /**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param attorneyCode  The given patientCode.
     * @param fromDos   The information of the requested page.
     * @param toDos   The information of the requested page.
     * @return  A page of Documents entries whose toDos and fromDOS for a patient Code. The content of
     *          the returned page depends from the page request given as a method parameter.
     */
    @Query("select documents from Documents documents  where documents.attorneyCode =:attorneyCode and documents.patientCode = :patientCode ")
    Page<Documents> getDOSForAttorney(@Param("attorneyCode") String attorneyCode, @Param("patientCode")String patientCode, Pageable page);
   
    /**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param attorneyCode  The given patientCode.
     * @param fromDos   The information of the requested page.
     * @param toDos   The information of the requested page.
     * @return  A page of Documents entries whose toDos and fromDOS for a patient Code. The content of
     *          the returned page depends from the page request given as a method parameter.
     */
    @Query("select documents from Documents documents  where documents.attorneyCode =:attorneyCode and documents.patientCode = :patientCode and documents.processedFlag = TRUE")
    Page<Documents> findAttorneyProcessedDOS(@Param("attorneyCode") String attorneyCode, @Param("patientCode")String patientCode, Pageable page);
   
    
    
    
    /**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param patientCode  The given patientCode.
     * @param fromDos   The information of the requested page.
     * @param toDos   The information of the requested page.
     * @return  A page of Documents entries whose toDos and fromDOS for a patient Code. The content of
     *          the returned page depends from the page request given as a method parameter.
     */
	    /*@Query("select documents from Documents documents  where documents.patientCode =:patientCode AND documents.fromDos = :fromDos AND documents.toDos = :toDos")
	    Page<Documents> findByPatientCode(@Param("patientCode") String patientCode, @Param("fromDos") String fromDos, @Param("toDos") String toDos, Pageable page);
	    */
	    
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("update Documents documents SET documents.status = ?1, documents.statusDate = ?2,documents.arbAmount = ?3, documents.sentDate = ?4,"
    		+ "documents.paymentAmount = ?5, documents.paymentDate = ?6, documents.arbNotes = ?7, documents.natureOfDispute = ?8, "
    		+ "documents.attorneyCode = ?9 WHERE documents.patientCode = ?10 AND documents.fromDos = ?11 AND documents.toDos = ?12")
    public void updatePatientDOS(String status, String statusDate, String arbAmount,String sentDate,String paymentAmount, 
    		String paymentDate, String arbNotes,String natureOfDispute,String attorneyCode, String patientCode, String fromDos, String toDos);
    
    /*@Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("update Documents documents SET documents.status = :status, documents.statusDate = (:statusDate is null or statusDate = :statusDate),documents.arbAmount = :arbAmount, documents.sentDate= :sentDate,"
    		+ "documents.paymentAmount = :paymentAmount, documents.paymentDate = :paymentDate, documents.arbNotes = :arbNotes, documents.natureOfDispute = :natureOfDispute, "
    		+ "documents.attorneyCode = :attorneyCode WHERE documents.patientCode = :patientCode AND documents.fromDos = :fromDos AND documents.toDos = :toDos")
    public void updatePatientDOS(@Param("status") String status, @Param("statusDate") String statusDate,@Param("arbAmount") String arbAmount,@Param("sentDate") String sentDate,@Param("paymentAmount") String paymentAmount, 
    		@Param("paymentDate") String paymentDate, @Param("arbNotes") String arbNotes, @Param("natureOfDispute") String natureOfDispute,@Param("attorneyCode") String attorneyCode,@Param("patientCode") String patientCode, @Param("fromDos") String fromDos,@Param("toDos") String toDos);
*/
    @Modifying
    @Query("update Documents documents SET documents.processedFlag = ?1, documents.processedDate = ?2 WHERE documents.attorneyCode = ?3 AND documents.fromDos = ?4 and documents.toDos = ?5")
    public void updatePatientDOSToProcessed(boolean processedFlag, String processedDate, String attorneyCode, String fromDOS, String toDOS);
	
    

    /**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param patientCode  The given patientCode.
     * @param practiceCode  The given practiceCode.
     * @param FromDOS  The given FromDOS
     * @param ToDOS  The given ToDOS
     * @return  A page of Documents entries whose toDos and fromDOS for a patient Code. The content of
     *          the returned page depends from the page request given as a method parameter.
     */
    
    @Query("select documents from Documents documents  where documents.patientCode =:patientCode and documents.practiceCode = :practiceCode and documents.fromDos = :fromDos and documents.toDos = :toDos")
    Page<Documents> getPatientDocsDownload(@Param("patientCode") String patientCode,@Param("practiceCode") String practiceCode,@Param("fromDos") String fromDos,@Param("toDos") String toDos, Pageable page);

    
    /**
     * This query method invokes the JPQL query that is configured by using the {@code @Query} annotation.
     * @param attorneyCode  The given patientCode.
     * @param patientCode   The information of the requested page.
     * @param processedFlag   The information of the requested page.
     * @return  A page of Documents entries whose toDos and fromDOS for a patient Code. The content of
     *          the returned page depends from the page request given as a method parameter.
     */
    @Query("select documents from Documents documents  where documents.attorneyCode =:attorneyCode and documents.patientCode = :patientCode and documents.processedFlag = TRUE ")
    Page<Documents> getHistoryDOSForAttorney(@Param("attorneyCode") String attorneyCode, @Param("patientCode")String patientCode, Pageable page);
   
    
    @Query("select documents from Documents documents  where documents.attorneyCode = ?1 and documents.fromDos = ?2 and documents.toDos = ?3 and documents.processedFlag = TRUE")
    Page<Documents> searchPatientHistoryByAttorney(String attorneyCode, String fromDos,String toDos, Pageable page);
    
    @Query("select documents from Documents documents  where documents.attorneyCode = ?1 and documents.processedDate >= ?2 and documents.processedDate <= ?3 and documents.processedFlag = TRUE")
    Page<Documents> searchPatientHistoryDocs(String attorneyCode, String processedFrom,String processedTo, Pageable page);
    
    
    
    
    
    /*@Query("select documents from Documents documents  where documents.attorneyCode = ?1 and documents.fromDos = ?2 and documents.toDos = ?3 and documents.processedFlag = TRUE")
    Page<Documents> searchPatientHistoryByAttorney(String attorneyCode, String fromDos,String toDos, Pageable page);
    */
    
   
	   
   /* @Query("select documents from Documents documents  where documents.attorneyCode =:attorneyCode and documents.fromDos = :fromDos and documents.toDos = :toDos and documents.processedFlag = TRUE ")
    Page<Documents> searchPatientHistoryByAttorney(@Param("attorneyCode") String attorneyCode, @Param("fromDos")String fromDos,@Param("toDos")String toDos, Pageable page);
   */
	    
}
