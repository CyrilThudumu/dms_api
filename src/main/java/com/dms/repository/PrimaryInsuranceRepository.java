package com.dms.repository;

import com.dms.domain.Casetype;
import com.dms.domain.PrimaryInsurance;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Casetype entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PrimaryInsuranceRepository extends JpaRepository<PrimaryInsurance, Long>, JpaSpecificationExecutor<PrimaryInsurance> {

}
