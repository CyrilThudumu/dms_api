package com.dms.repository;

import com.dms.domain.Patientstatus;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Patientstatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PatientstatusRepository extends JpaRepository<Patientstatus, Long>, JpaSpecificationExecutor<Patientstatus> {

}
