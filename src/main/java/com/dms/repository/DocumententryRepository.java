package com.dms.repository;

import com.dms.domain.Documententry;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Documententry entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DocumententryRepository extends JpaRepository<Documententry, Long>, JpaSpecificationExecutor<Documententry> {

}
