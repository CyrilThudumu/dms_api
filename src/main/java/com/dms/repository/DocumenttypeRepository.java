package com.dms.repository;

import com.dms.domain.Documenttype;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Documenttype entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DocumenttypeRepository extends JpaRepository<Documenttype, Long>, JpaSpecificationExecutor<Documenttype> {

}
