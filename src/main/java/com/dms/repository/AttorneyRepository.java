package com.dms.repository;

import com.dms.domain.Attorney;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Attorney entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AttorneyRepository extends JpaRepository<Attorney, Long>, JpaSpecificationExecutor<Attorney> {

}
