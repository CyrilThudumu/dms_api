package com.dms.repository;

import com.dms.domain.Casetype;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Casetype entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CasetypeRepository extends JpaRepository<Casetype, Long>, JpaSpecificationExecutor<Casetype> {

}
