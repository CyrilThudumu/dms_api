package com.dms.repository;

import com.dms.domain.Documents;
import com.dms.domain.Patient;
import com.dms.domain.User;
import com.dms.service.dto.PatientChildDTO;
import com.dms.service.dto.PatientDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Patient entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PatientRepository extends JpaRepository<Patient, Long>, JpaSpecificationExecutor<Patient> {

//    @Query("select patient from Patient patient where patient.attorny.login = ?#{principal.username}")
//    List<Patient> findByAttornyIsCurrentUser();
//
//    @Query("select patient from Patient patient where patient.doctor.login = ?#{principal.username}")
//    List<Patient> findByDoctorIsCurrentUser();

    @Query(value = "select distinct patient from Patient patient left join fetch patient.edocuments",
        countQuery = "select count(distinct patient) from Patient patient")
    Page<Patient> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct patient from Patient patient left join fetch patient.edocuments")
    List<Patient> findAllWithEagerRelationships();

    @Query("select  patient from Patient patient where patient.id =:id")
    Optional<Patient> findOneWithEagerRelationships(@Param("id") Long id);

    @Query(value = "select DISTINCT  patient from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=:attorneyCode")
    //@Query(value = "select patient.patientCode, patient.firstName, patient.lastName,patient.primaryInsClaimNo,patient.primaryInsPolicyNo,patient.doa from Patient patient inner join Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=:attorneyCode")
    Page<Patient> getAttorneyByPatients(@Param("attorneyCode") String attorneyCode, Pageable page);
    
    @Query(value = "select DISTINCT  patient from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=:attorneyCode and documents.processedFlag = TRUE")
    //@Query(value = "select patient.patientCode, patient.firstName, patient.lastName,patient.primaryInsClaimNo,patient.primaryInsPolicyNo,patient.doa from Patient patient inner join Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=:attorneyCode")
    Page<Patient> patientsProcessedByAttorney(@Param("attorneyCode") String attorneyCode, Pageable page);
    
    

    @Query(value = "select DISTINCT  patient from Patient patient where patient.practiceCode=:practiceCode")
    Page<Patient> getPatientsForPractice(@Param("practiceCode") String practiceCode, Pageable page);

    
    @Query(value = "select DISTINCT patient from Patient patient inner join fetch Documents documents on patient.patientCode = documents.patientCode where documents.doctorCode=:doctorCode")
    Page<Patient> getDoctorPatients(@Param("doctorCode") String doctorCode, Pageable page);
    
    @Query(value = "select DISTINCT patient from Patient patient inner join fetch Documents documents on patient.patientCode = documents.patientCode where documents.doctorCode= ?1 and patient.patientCode like ?2%")
    Page<Patient> docPatientSearch(String doctorCode,String searchKey, Pageable page);
    
    @Query(value = "select DISTINCT patient from Patient patient inner join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode= ?1 and patient.patientCode like ?2%")
    Page<Patient> attorneyPatientSearch(String attorneyCode,String searchKey, Pageable page);
    
    
    
    @Query(value = "select patient from Patient patient  where patient.patientCode=:patientCode")
    Optional<Patient> getPatientByCode(@Param("patientCode") String patientCode);
    
    
    @Query(value = "select DISTINCT  patient from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=:attorneyCode and documents.processedFlag = TRUE")
    
    Page<Patient> searchPatientHistoryByAttorney(@Param("attorneyCode") String attorneyCode, Pageable page);
    
    @Query(value = "select DISTINCT patient.patientCode, patient.firstName,patient.lastName,patient.sex,patient.mobileNo,patient.practiceCode, patient.doa  from Patient patient left join fetch Documents documents on patient.patientCode = documents.patientCode where documents.attorneyCode=:attorneyCode")
    Patient findByPatientCode(@Param("attorneyCode") String attorneyCode);
    
}
