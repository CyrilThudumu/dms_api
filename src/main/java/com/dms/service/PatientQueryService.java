package com.dms.service;

import io.github.jhipster.service.QueryService;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.domain.Edocument_;
import com.dms.domain.Patient; // for static metamodels
import com.dms.domain.Patient_;
import com.dms.domain.User;
import com.dms.repository.PatientRepository;
import com.dms.service.dto.PatientCriteria;
import com.dms.service.dto.PatientDTO;
import com.dms.service.mapper.PatientMapper;

/**
 * Service for executing complex queries for Patient entities in the database.
 * The main input is a {@link PatientCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PatientDTO} or a {@link Page} of {@link PatientDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PatientQueryService extends QueryService<Patient> {

    private final Logger log = LoggerFactory.getLogger(PatientQueryService.class);

    private final PatientRepository patientRepository;

    private final PatientMapper patientMapper;
    
    @PersistenceContext
    private EntityManager entityManager;
    
    public PatientQueryService(PatientRepository patientRepository, PatientMapper patientMapper) {
        this.patientRepository = patientRepository;
        this.patientMapper = patientMapper;
    }

    /**
     * Return a {@link List} of {@link PatientDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PatientDTO> findByCriteria(PatientCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Patient> specification = createSpecification(criteria);
        return patientMapper.toDto(patientRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PatientDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PatientDTO> findByCriteria(PatientCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Patient> specification = createSpecification(criteria);
        return patientRepository.findAll(specification, page)
            .map(patientMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PatientCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Patient> specification = createSpecification(criteria);
        return patientRepository.count(specification);
    }

    
   
    
    /**
     * Function to convert PatientCriteria to a {@link Specification}
     */
    private Specification<Patient> createSpecification(PatientCriteria criteria) {
        Specification<Patient> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Patient_.id));
            }
            if (criteria.getPatientCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPatientCode(), Patient_.patientCode));
            }
            if (criteria.getFirstName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFirstName(), Patient_.firstName));
            }
            if (criteria.getLastName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastName(), Patient_.lastName));
            }
            if (criteria.getSex() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSex(), Patient_.sex));
            }
            if (criteria.getAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress(), Patient_.address));
            }
            if (criteria.getCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCity(), Patient_.city));
            }
            if (criteria.getState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getState(), Patient_.state));
            }
            if (criteria.getZipCode() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getZipCode(), Patient_.zipCode));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), Patient_.email));
            }
            if (criteria.getMobileNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMobileNo(), Patient_.mobileNo));
            }
            if (criteria.getEnable() != null) {
                specification = specification.and(buildSpecification(criteria.getEnable(), Patient_.enable));
            }
            if (criteria.getPatientStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPatientStatus(), Patient_.patientStatus));
            }
            if (criteria.getStatusNotes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatusNotes(), Patient_.statusNotes));
            }
            if (criteria.getDob() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDob(), Patient_.dob));
            }
            if (criteria.getCaseType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCaseType(), Patient_.caseType));
            }
            if (criteria.getWorkPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getWorkPhone(), Patient_.workPhone));
            }
            if (criteria.getAge() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAge(), Patient_.age));
            }
            if (criteria.getInsFirstName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsFirstName(), Patient_.insFirstName));
            }
            if (criteria.getInsLastName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsLastName(), Patient_.insLastName));
            }
            if (criteria.getInsInitial() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsInitial(), Patient_.insInitial));
            }
            if (criteria.getInsAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsAddress(), Patient_.insAddress));
            }
            if (criteria.getInsCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsCity(), Patient_.insCity));
            }
            if (criteria.getInsState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsState(), Patient_.insState));
            }
            if (criteria.getInsZipCode() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getInsZipCode(), Patient_.insZipCode));
            }
            if (criteria.getInsPhoneNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsPhoneNo(), Patient_.insPhoneNo));
            }
            if (criteria.getPrimaryInsCo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryInsCo(), Patient_.primaryInsCo));
            }
            if (criteria.getPrimaryInsClaimNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryInsClaimNo(), Patient_.primaryInsClaimNo));
            }
            if (criteria.getPrimaryInsPolicyNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryInsPolicyNo(), Patient_.primaryInsPolicyNo));
            }
            if (criteria.getPrimaryInsRelation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryInsRelation(), Patient_.primaryInsRelation));
            }
            if (criteria.getPrimaryInsCertifier() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryInsCertifier(), Patient_.primaryInsCertifier));
            }
            
            if (criteria.getPrimaryInsAdjuster() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryInsAdjuster(), Patient_.primaryInsAdjuster));
            }
            if (criteria.getPrimaryInsAdjusterFax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryInsAdjusterFax(), Patient_.primaryInsAdjusterFax));
            }
            
            if (criteria.getPrimaryInsNotes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryInsNotes(), Patient_.primaryInsNotes));
            }
            if (criteria.getDoa() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDoa(), Patient_.doa));
            }
            
            if (criteria.getInsLastName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsLastName(), Patient_.insLastName));
            }
            if (criteria.getInsFirstName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsFirstName(), Patient_.insFirstName));
            }
            
            if (criteria.getInsInitial() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsInitial(), Patient_.insInitial));
            }
            if (criteria.getInsAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsAddress(), Patient_.insAddress));
            }
            
            if (criteria.getInsCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsCity(), Patient_.insCity));
            }
            if (criteria.getInsState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsState(), Patient_.insState));
            }
            if (criteria.getInsZipCode() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getInsZipCode(), Patient_.insZipCode));
            }
            if (criteria.getInsPhoneNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsPhoneNo(), Patient_.insPhoneNo));
            }
            
            if (criteria.getSecInsClaimNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecInsClaimNo(), Patient_.secInsClaimNo));
            }
            if (criteria.getSecInsPolicyNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecInsPolicyNo(), Patient_.secInsPolicyNo));
            }
            if (criteria.getSecInsRelation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecInsRelation(), Patient_.secInsRelation));
            }
            if (criteria.getSecInsNotes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecInsNotes(), Patient_.secInsNotes));
            }
            
            if (criteria.getSecInsCertifier() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecInsCertifier(), Patient_.secInsCertifier));
            }
            
            if (criteria.getSecInsAdjuster() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecInsAdjuster(), Patient_.secInsAdjuster));
            }
            
            if (criteria.getSecInsAdjusterFax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecInsAdjusterFax(), Patient_.secInsAdjusterFax));
            }
            
            if (criteria.getAddnInfo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddnInfo(), Patient_.addnInfo));
            }
            
            if (criteria.getAttorneyFirstName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttorneyFirstName(), Patient_.attorneyFirstName));
            }
            if (criteria.getAttorneyLastName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttorneyLastName(), Patient_.attorneyLastName));
            }

            if (criteria.getAttorneyCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttorneyCity(), Patient_.attorneyCity));
            }
            if (criteria.getAttorneyState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttorneyState(), Patient_.attorneyState));
            }

            if (criteria.getAttorneyFaxNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttorneyFaxNo(), Patient_.attorneyFaxNo));
            }
            if (criteria.getAttorneyMobileNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttorneyMobileNo(), Patient_.attorneyMobileNo));
            }

            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), Patient_.createdBy));
            }
            if (criteria.getCreationDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreationDate(), Patient_.creationDate));
            }
            if (criteria.getUpdatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUpdatedBy(), Patient_.updatedBy));
            }
            if (criteria.getUpdatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdatedDate(), Patient_.updatedDate));
            }
            if(criteria.getPracticeCode() != null){
            	specification = specification.and(buildStringSpecification(criteria.getPracticeCode(), Patient_.practiceCode));
            }
         /*   if (criteria.getAttorneyLogin() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttorneyLogin(), Patient_.attorneyLogin));
            }
            if (criteria.getDoctorLogin() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDoctorLogin(), Patient_.doctorLogin));
            }

            if (criteria.getDoctorId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDoctorId(), Patient_.doctorId));
            }

            if (criteria.getAttorneyId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAttorneyId(), Patient_.attorneyId));
            }*/


            
//            if (criteria.getAttornyId() != null) {
//                specification = specification.and(buildSpecification(criteria.getAttornyId(),
//                    root -> root.join(Patient_.attorny, JoinType.LEFT).get(User_.id)));
//            }
//            if (criteria.getDoctorId() != null) {
//                specification = specification.and(buildSpecification(criteria.getDoctorId(),
//                    root -> root.join(Patient_.doctor, JoinType.LEFT).get(User_.id)));
//            }
            if (criteria.getEdocumentId() != null) {
                specification = specification.and(buildSpecification(criteria.getEdocumentId(),
                    root -> root.join(Patient_.edocuments, JoinType.LEFT).get(Edocument_.id)));
            }
        }
        return specification;
    }
    
    public List<Patient> searchPatient(Patient patients) {
    	List<Patient> patient = patientRepository.findAll(new Specification<Patient>(){
			public Predicate toPredicate(Root<Patient> root, CriteriaQuery< ?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList();
				if (patients.getFirstName() != null) {
					predicates.add(cb.like(cb.lower(root.get("firstName")), "%" + patients.getFirstName().toLowerCase() + "%"));
				}
				if (patients.getDob() != null) {
					predicates.add(cb.equal(root.get("DOB"), patients.getDob()));
				}
				if (patients.getDoa() != null) {
					predicates.add(cb.equal(root.get("DOA"), patients.getDoa()));
				}
				if (patients.getPrimaryInsClaimNo() != null) {
					predicates.add(cb.like(cb.lower(root.get("primaryIns_claim_no")), "%" + patients.getPrimaryInsClaimNo().toLowerCase() + "%"));
				}
				if (patients.getPrimaryInsPolicyNo() != null) {
					predicates.add(cb.like(cb.lower(root.get("primary_policy_no")), "%" + patients.getPrimaryInsPolicyNo().toLowerCase() + "%"));
				}
				if (patients.getPracticeCode() != null) {
					predicates.add(cb.like(cb.lower(root.get("practice_code")), 
							patients.getPracticeCode().toLowerCase()));
				}
				return cb.and(predicates.toArray(new Predicate[0]));
			}
		});
    	return patient;
    	
    }
 
   


}
