package com.dms.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.dms.domain.*; // for static metamodels
import com.dms.repository.CasetypeRepository;
import com.dms.repository.PrimaryInsuranceRepository;
import com.dms.service.dto.CasetypeCriteria;
import com.dms.service.dto.CasetypeDTO;
import com.dms.service.dto.PrimaryInsuranceCriteria;
import com.dms.service.dto.PrimaryInsuranceDTO;
import com.dms.service.mapper.CasetypeMapper;
import com.dms.service.mapper.PrimaryInsuranceMapper;

/**
 * Service for executing complex queries for PrimaryInsurance entities in the database.
 * The main input is a {@link PrimaryInsuranceCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PrimaryInsuranceDTO} or a {@link Page} of {@link PrimaryInsuranceDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PrimaryInsuranceQueryService extends QueryService<PrimaryInsurance> {

    private final Logger log = LoggerFactory.getLogger(PrimaryInsuranceQueryService.class);

    private final PrimaryInsuranceRepository primaryInsuranceRepository;

    private final PrimaryInsuranceMapper primaryInsuranceMapper;

    public PrimaryInsuranceQueryService(PrimaryInsuranceRepository primaryInsuranceRepository, PrimaryInsuranceMapper primaryInsuranceMapper) {
        this.primaryInsuranceRepository = primaryInsuranceRepository;
        this.primaryInsuranceMapper = primaryInsuranceMapper;
    }

   /* *//**
     * Return a {@link List} of {@link CasetypeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     *//*
    @Transactional(readOnly = true)
    public List<CasetypeDTO> findByCriteria(CasetypeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Casetype> specification = createSpecification(criteria);
        return casetypeMapper.toDto(casetypeRepository.findAll(specification));
    }*/

    /**
     * Return a {@link Page} of {@link CasetypeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PrimaryInsuranceDTO> findByCriteria(PrimaryInsuranceCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PrimaryInsurance> specification = createSpecification(criteria);
        return primaryInsuranceRepository.findAll(specification, page)
            .map(primaryInsuranceMapper::toDto);
    }

  /*  *//**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     *//*
    @Transactional(readOnly = true)
    public long countByCriteria(CasetypeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Casetype> specification = createSpecification(criteria);
        return casetypeRepository.count(specification);
    }
*/
    /**
     * Function to convert CasetypeCriteria to a {@link Specification}
     */
    private Specification<PrimaryInsurance> createSpecification(PrimaryInsuranceCriteria criteria) {
        Specification<PrimaryInsurance> specification = Specification.where(null);
        if (criteria != null) {/*
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), PrimaryInsurance_.id));
            }
            if (criteria.getCaseTypeCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCaseTypeCode(), PrimaryInsurance_.caseTypeCode));
            }
            if (criteria.getCaseTypeName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCaseTypeName(), PrimaryInsurance_.caseTypeName));
            }
        */}
        return specification;
    }
}
