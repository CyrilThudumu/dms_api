package com.dms.service;

import com.dms.service.dto.EdocumentDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Edocument.
 */
public interface EdocumentService {

    /**
     * Save a edocument.
     *
     * @param edocumentDTO the entity to save
     * @return the persisted entity
     */
    EdocumentDTO save(EdocumentDTO edocumentDTO);

    /**
     * Get all the edocuments.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<EdocumentDTO> findAll(Pageable pageable);


    /**
     * Get the "id" edocument.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<EdocumentDTO> findOne(Long id);

    /**
     * Delete the "id" edocument.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
