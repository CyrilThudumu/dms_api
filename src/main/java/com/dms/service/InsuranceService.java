package com.dms.service;

import com.dms.service.dto.CasetypeDTO;
import com.dms.service.dto.InsuranceDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing insurance.
 */
public interface InsuranceService {

  /*  *//**
     * Save a casetype.
     *
     * @param casetypeDTO the entity to save
     * @return the persisted entity
     *//*
    CasetypeDTO save(CasetypeDTO casetypeDTO);*/

    /**
     * Get all the Insurance.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<InsuranceDTO> findAll(Pageable pageable);


  /*  *//**
     * Get the "id" casetype.
     *
     * @param id the id of the entity
     * @return the entity
     *//*
    Optional<CasetypeDTO> findOne(Long id);

    *//**
     * Delete the "id" casetype.
     *
     * @param id the id of the entity
     *//*
    void delete(Long id);*/
}
