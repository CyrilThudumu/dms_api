package com.dms.service.impl;

import com.dms.service.PracticeService;
import com.dms.domain.Practice;
import com.dms.repository.PracticeRepository;
import com.dms.service.dto.PracticeDTO;
import com.dms.service.mapper.PracticeMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Practice.
 */
@Service
@Transactional
public class PracticeServiceImpl implements PracticeService {

    private final Logger log = LoggerFactory.getLogger(PracticeServiceImpl.class);

    private final PracticeRepository practiceRepository;

    private final PracticeMapper practiceMapper;

    public PracticeServiceImpl(PracticeRepository practiceRepository, PracticeMapper practiceMapper) {
        this.practiceRepository = practiceRepository;
        this.practiceMapper = practiceMapper;
    }

    /**
     * Save a practice.
     *
     * @param practiceDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PracticeDTO save(PracticeDTO practiceDTO) {
        log.debug("Request to save Practice : {}", practiceDTO);
        
        Practice practice = practiceMapper.toEntity(practiceDTO);
        practice = practiceRepository.save(practice);
        return practiceMapper.toDto(practice);
    }

    /**
     * Get all the practices.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PracticeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Practices");
        return practiceRepository.findAll(pageable)
            .map(practiceMapper::toDto);
    }


    /**
     * Get one practice by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PracticeDTO> findOne(Long id) {
        log.debug("Request to get Practice : {}", id);
        return practiceRepository.findById(id)
            .map(practiceMapper::toDto);
    }
    
    /**
     * Get one practice by practiceName.
     *
     * @param practiceName the practiceName of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PracticeDTO> findByPracticeName(String practiceName, Pageable page) {
        log.debug("Request to get Practice : {}", practiceName);
        return practiceRepository.findByPracticeName(practiceName,page)
            .map(practiceMapper::toDto);
    }

    /**
     * Delete the practice by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Practice : {}", id);
        practiceRepository.deleteById(id);
    }
    
    /**
     * Get one practice by practiceName.
     *
     * @param practiceName the practiceName of the entity
     * @return the entity
     */
  /*  @Override
    @Transactional(readOnly = true)
    public Optional<PracticeDTO> findPracticeInfo(String practiceName) {
        log.debug("Request to get Practice : {}", practiceName);
        return practiceRepository.findByPracticeName(practiceName).map(practiceMapper::toDto);
            
    }*/

    
}
