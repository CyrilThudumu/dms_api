package com.dms.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.domain.Documents;
import com.dms.repository.DocumentsRepository;
import com.dms.service.DocumentsService;
import com.dms.service.dto.DocumentsDTO;
import com.dms.service.dto.PatientDTO;
import com.dms.service.mapper.DocumentsMapper;

/**
 * Service Implementation for managing documents.
 */
@Service
@Transactional
public class DocumentServiceImpl implements DocumentsService {

    private final Logger log = LoggerFactory.getLogger(DocumentServiceImpl.class);

    private final DocumentsRepository documentsRepository;

    private final DocumentsMapper documentsMapper;

   
	public DocumentServiceImpl(DocumentsRepository documentsRepository, DocumentsMapper documentsMapper) {
        this.documentsRepository = documentsRepository;
        this.documentsMapper = documentsMapper;
    }

    /**
     * Save a document.
     *
     * @param documentDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DocumentsDTO save(DocumentsDTO documentsDTO) {
        log.debug("Request to save Document : {}", documentsDTO);

        Documents documents = documentsMapper.toEntity(documentsDTO);
        documents = documentsRepository.save(documents);
        return documentsMapper.toDto(documents);
    }
    
    /**
	 * Get DOS for a  patient based on patientCode.
	 *
	 * @param documentsDTO the DocumentsDTO of the DTO
	 * @return the Documents Entity
	 */
    /*@Transactional(readOnly = true)
    @Override
    public Page<DocumentsDTO> findByPatientCode(DocumentsDTO documentsDTO, Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", documentsDTO);
        return documentsRepository.findByPatientCode(documentsDTO.getPatientCode(),documentsDTO.getFromDos(),documentsDTO.getToDos(), page).map(documentsMapper::toDto);
        
    }*/
    
    @Override
    @Transactional
    public void updatePatientDOS(DocumentsDTO documentsDTO){
        log.info("Finding Docoument entries by search term: {} and page request: {}", documentsDTO);
        documentsRepository.updatePatientDOS(documentsDTO.getStatus(),documentsDTO.getStatusDate(),documentsDTO.getArbAmount(),
        		documentsDTO.getSentDate(),documentsDTO.getPaymentAmount(),documentsDTO.getPaymentDate(),documentsDTO.getArbNotes(),
        		documentsDTO.getNatureOfDispute(),documentsDTO.getAttorneyCode(),documentsDTO.getPatientCode(),documentsDTO.getFromDos(),
        		documentsDTO.getToDos());
    }
    /**
	 * Get DOS for a  patient based on doctorCode.
	 *
	 * @param documentsDTO the DocumentsDTO of the DTO
	 * @return the Documents Entity
	 */
    
    @Transactional(readOnly = true)
    @Override
    public Page<DocumentsDTO> findByDoctorCode(String doctorCode,String patientCode, Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", doctorCode, patientCode);
        return documentsRepository.getDOSForDoctor(doctorCode,patientCode, page).map(documentsMapper::toDto);
        
    }
    
    /**
	 * Get DOS for a  patient based on patientCode.
	 *
	 * @param documentsDTO the DocumentsDTO of the DTO
	 * @return the Documents Entity
	 */
    @Transactional(readOnly = true)
    @Override
    public Page<DocumentsDTO> findByPatientCode(String patientCode,String practiceCode, Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", patientCode);
        return documentsRepository.getDOSForPatient(patientCode,practiceCode, page).map(documentsMapper::toDto);
        
    }
    
    /**
   	 * Get DOS for a  patient based on attorneyCode.
   	 *
   	 * @param documentsDTO the DocumentsDTO of the DTO
   	 * @return the Documents Entity
   	 */
       @Transactional(readOnly = true)
       @Override
       public Page<DocumentsDTO> findByAttorneyCode(String attorneyCode, String patientCode, Pageable page) {
           log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode);
           return documentsRepository.getDOSForAttorney(attorneyCode,patientCode, page).map(documentsMapper::toDto);
           
       }
       /**
      	 * Get DOS for a  patient based on attorneyCode.
      	 *
      	 * @param documentsDTO the DocumentsDTO of the DTO
      	 * @return the Documents Entity
      	 */
          @Transactional(readOnly = true)
          @Override
          public Page<DocumentsDTO> getAttorneyProcessedDOS(String attorneyCode, String patientCode, Pageable page) {
              log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode,patientCode);
              return documentsRepository.findAttorneyProcessedDOS(attorneyCode,patientCode, page).map(documentsMapper::toDto);
              
          }
       
       
       @Override
       @Transactional
       public void updatePatientDOSToProcessed(DocumentsDTO documentsDTO){
           log.info("Updating DOS to Processed By Attorney: {} and page request: {}", documentsDTO);
           String pattern = "yyyy-MM-dd";
   			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
   			String date = simpleDateFormat.format(new Date());
   			documentsDTO.setProcessedDate(date);
           documentsRepository.updatePatientDOSToProcessed(documentsDTO.isProcessedFlag(),documentsDTO.getProcessedDate(),documentsDTO.getAttorneyCode(), documentsDTO.getFromDos(), documentsDTO.getToDos());
       }
       
       /**
   	 * Get DOS for a  patient based on patientCode and practice code.
   	 *
   	 * @param documentsDTO the DocumentsDTO of the DTO
   	 * @return the Documents Entity
   	 */
       @Transactional(readOnly = true)
       @Override
       public Page<DocumentsDTO> downloadPatientDocs(String patientCode, String practiceCode,String fromDOS, String toDOS, Pageable page) {
           log.info("Finding Docoument entries by search term: {} and page request: {}", patientCode);
           
           return documentsRepository.getPatientDocsDownload(patientCode,practiceCode,fromDOS,toDOS, page).map(documentsMapper::toDto);
       }
       
      /* @Transactional(readOnly = true)
       @Override
       public Optional<DocumentsDTO> downloadPatientDocs(DocumentsDTO dtoObj) {
           log.info("Finding Docoument entries by search term: {} and page request: {}", dtoObj);
           
          // Optional<Documents> documents = documentsRepository.getPatientDocsDownload(documentsDTO.getPatientCode(),documentsDTO.getPracticeCode(),documentsDTO.getFromDos(),documentsDTO.getToDos());
         
           //return documentsRepository.getPatientDocsDownload(documentsDTO.getPatientCode(),documentsDTO.getPracticeCode(),documentsDTO.getFromDos(),documentsDTO.getToDos()).map(documentsMapper::toDto);
           return documentsRepository.getPatientDocsDownload(dtoObj.getPatientCode(),dtoObj.getPracticeCode(),dtoObj.getFromDos(),dtoObj.getToDos()).map(documentsMapper::toDto);
       }*/
       
    
       /**
      	 * Get DOS for a  patient based on attorneyCode.
      	 *
      	 * @param documentsDTO the DocumentsDTO of the DTO
      	 * @return the Documents Entity
      	 */
          @Transactional(readOnly = true)
          @Override
          public Page<DocumentsDTO> findDocsHistoryByAttorneyCode(String attorneyCode, String patientCode, Pageable page) {
              log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode,patientCode);
              return documentsRepository.getHistoryDOSForAttorney(attorneyCode,patientCode, page).map(documentsMapper::toDto);
          }
      	
      	/*@Override
      	public Page<DocumentsDTO> searchAttorneyPatientHistory(DocumentsDTO documentsDTO, Pageable page) {
      		log.debug("Request to fetch patients history For Attorney : {}", documentsDTO);
      		return documentsRepository.searchPatientHistoryByAttorney(documentsDTO.getAttorneyCode(),documentsDTO.getFromDos(),documentsDTO.getToDos(),documentsDTO.getSearchKey(), page).map(documentsMapper::toDto);
      		
      	}*/
      	
      	@Override
      	public Page<DocumentsDTO> searchAttorneyPatientHistory(String attorneyCode, String fromDOS, String toDOS,  Pageable page) {
      		log.debug("Request to fetch patients history For Attorney : {}", attorneyCode);
      		return documentsRepository.searchPatientHistoryByAttorney(attorneyCode,fromDOS,toDOS, page).map(documentsMapper::toDto);
      		
      	}
      	
    	@Override
      	public Page<DocumentsDTO> getDocumentsForHistorySearch(String attorneyCode, String processedFrom, String processedTo, Pageable page) {
      		log.debug("Request to fetch patients docs history For Attorney : {}", attorneyCode);
      		return documentsRepository.searchPatientHistoryDocs(attorneyCode,processedFrom,processedTo, page).map(documentsMapper::toDto);
      		
      	}
      	

    	/**
    	 * Delete the Document by id.
    	 *
    	 * @param id the id of the entity
    	 */
    	@Override
    	public void delete(Long id) {
    		log.debug("Request to delete Document : {}", id);
    		documentsRepository.deleteById(id);
    		
    	}

    	
    	/*@Override
      	public Page<DocumentsDTO> searchAttorneyPatientHistory(String attorneyCode, String fromDOS, String toDOS,  Pageable page) {
      		log.debug("Request to fetch patients history For Attorney : {}", attorneyCode);
      		return documentsRepository.searchPatientHistoryByAttorney(attorneyCode,fromDOS,toDOS, page).map(documentsMapper::toDto);
      		
      	}*/
      	
    	
    	
}
