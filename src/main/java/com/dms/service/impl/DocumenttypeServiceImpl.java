package com.dms.service.impl;

import com.dms.service.DocumenttypeService;
import com.dms.domain.Documenttype;
import com.dms.repository.DocumenttypeRepository;
import com.dms.service.dto.DocumenttypeDTO;
import com.dms.service.mapper.DocumenttypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Documenttype.
 */
@Service
@Transactional
public class DocumenttypeServiceImpl implements DocumenttypeService {

    private final Logger log = LoggerFactory.getLogger(DocumenttypeServiceImpl.class);

    private final DocumenttypeRepository documenttypeRepository;

    private final DocumenttypeMapper documenttypeMapper;

    public DocumenttypeServiceImpl(DocumenttypeRepository documenttypeRepository, DocumenttypeMapper documenttypeMapper) {
        this.documenttypeRepository = documenttypeRepository;
        this.documenttypeMapper = documenttypeMapper;
    }

    /**
     * Save a documenttype.
     *
     * @param documenttypeDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DocumenttypeDTO save(DocumenttypeDTO documenttypeDTO) {
        log.debug("Request to save Documenttype : {}", documenttypeDTO);

        Documenttype documenttype = documenttypeMapper.toEntity(documenttypeDTO);
        documenttype = documenttypeRepository.save(documenttype);
        return documenttypeMapper.toDto(documenttype);
    }

    /**
     * Get all the documenttypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DocumenttypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Documenttypes");
        return documenttypeRepository.findAll(pageable)
            .map(documenttypeMapper::toDto);
    }


    /**
     * Get one documenttype by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DocumenttypeDTO> findOne(Long id) {
        log.debug("Request to get Documenttype : {}", id);
        return documenttypeRepository.findById(id)
            .map(documenttypeMapper::toDto);
    }

    /**
     * Delete the documenttype by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Documenttype : {}", id);
        documenttypeRepository.deleteById(id);
    }
}
