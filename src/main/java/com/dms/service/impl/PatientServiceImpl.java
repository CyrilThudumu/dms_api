package com.dms.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.domain.Patient;
import com.dms.repository.PatientRepository;
import com.dms.service.PatientService;
import com.dms.service.dto.PatientDTO;
import com.dms.service.mapper.PatientMapper;

/**
 * Service Implementation for managing Patient.
 */
@Service
@Transactional
public class PatientServiceImpl implements PatientService {

	private final Logger log = LoggerFactory.getLogger(PatientServiceImpl.class);

	private final PatientRepository patientRepository;

	private final PatientMapper patientMapper;

	public PatientServiceImpl(PatientRepository patientRepository, PatientMapper patientMapper) {
		this.patientRepository = patientRepository;
		this.patientMapper = patientMapper;
	}

	/**
	 * Save a patient.
	 *
	 * @param patientDTO the entity to save
	 * @return the persisted entity
	 */
	@Override
	public PatientDTO save(PatientDTO patientDTO) {
		log.debug("Request to save Patient : {}", patientDTO);
		
		/*DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
		DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("MM-dd-yyy", Locale.ENGLISH);
		
		log.info("1. DOA: "+patientDTO.getDoa());
		LocalDate dateDoa = LocalDate.parse(patientDTO.getDoa(), inputFormatter);
		String formatterDoa = outputFormatter.format(dateDoa);
		patientDTO.setDoa(formatterDoa);
		log.info("2. DOA: "+patientDTO.getDoa());
		
		
		log.info("1. DOB:  "+patientDTO.getDob());
		LocalDate dateDob = LocalDate.parse(patientDTO.getDob(), inputFormatter);
		String formatterDob = outputFormatter.format(dateDob);
		patientDTO.setDob(formatterDob);
		log.info("2. DOB:  "+patientDTO.getDob());
		
		log.info("1. FirstVisit:  "+patientDTO.getFirstVisit());
		LocalDate dateFirstVisit = LocalDate.parse(patientDTO.getFirstVisit(), inputFormatter);
		String formatterFirstVisit = outputFormatter.format(dateFirstVisit);
		patientDTO.setFirstVisit(formatterFirstVisit);
		log.info("2. FirstVisit:  "+patientDTO.getFirstVisit());

		log.info("1. CreationDate: "+patientDTO.getCreationDate());
		LocalDate dateCreateDate = LocalDate.parse(patientDTO.getCreationDate(), inputFormatter);
		String formatterCreateDate = outputFormatter.format(dateCreateDate);
		patientDTO.setCreationDate(formatterCreateDate);
		log.info("2. CreationDate: "+patientDTO.getCreationDate());
		
		log.info("1. UpdateDate:  "+patientDTO.getUpdatedDate());
		LocalDate dateUpdateDate = LocalDate.parse(patientDTO.getUpdatedDate(), inputFormatter);
		String formatterUpdateDate = outputFormatter.format(dateUpdateDate);
		patientDTO.setUpdatedDate(formatterUpdateDate);
		log.info("2. UpdateDate:  "+patientDTO.getUpdatedDate());*/
		Patient patient = patientMapper.toEntity(patientDTO);
		
		patient = patientRepository.save(patient);
		return patientMapper.toDto(patient);
	}

	/**
	 * Get all the patients.
	 *
	 * @param pageable the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<PatientDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Patients");
		return patientRepository.findAll(pageable).map(patientMapper::toDto);
	}

	/**
	 * Get all the Patient with eager load of many-to-many relationships.
	 *
	 * @return the list of entities
	 */
	public Page<PatientDTO> findAllWithEagerRelationships(Pageable pageable) {
		return patientRepository.findAllWithEagerRelationships(pageable).map(patientMapper::toDto);
	}

	/**
	 * Get one patient by id.
	 *
	 * @param id the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<PatientDTO> findOne(Long id) {
		log.debug("Request to get Patient : {}", id);
		return patientRepository.findOneWithEagerRelationships(id).map(patientMapper::toDto);
	}
	
	
	/**
	 * Get one patient by patientCode.
	 *
	 * @param id the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public PatientDTO findOneByPatientCode(String attorneyCode) {
		log.info("attorneyCode is: "+attorneyCode);
		log.info("data is: "+patientRepository.findByPatientCode(attorneyCode));
		Patient patient = patientRepository.findByPatientCode(attorneyCode);
		PatientDTO patientDTO = new PatientDTO();
		patientDTO.setPatientCode(patient.getPatientCode());
		patientDTO.setFirstName(patient.getFirstName());
		patientDTO.setLastName(patient.getLastName());
		patientDTO.setSex(patient.getSex());
		patientDTO.setMobileNo(patient.getMobileNo());
		patientDTO.setPracticeCode(patient.getPracticeCode());
		patientDTO.setDoa(patient.getDoa());
		return patientDTO;
		
	}
	
	

	/**
	 * Delete the patient by id.
	 *
	 * @param id the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Patient : {}", id);
		patientRepository.deleteById(id);
	}

	
	@Override
	public Page<PatientDTO> getAttorneyPatients(String attorneyCode,Pageable page) {
		log.debug("Request to fetch patients by Attorney : {}", attorneyCode);
		 return patientRepository.getAttorneyByPatients(attorneyCode,page).map(patientMapper::toDto);
	}
	

	
	
	
	@Override
	public Page<PatientDTO> getPatientsForPractice(String practiceCode,Pageable page) {
		log.debug("Request to fetch patients for a practice : {}", practiceCode);
		 return patientRepository.getPatientsForPractice(practiceCode,page).map(patientMapper::toDto);
	}
	
	
	
	
	@Override
	public Page<PatientDTO> getDoctorPatients(String doctorCode, Pageable page) {
		log.debug("Request to fetch patients by Doctor : {}", doctorCode);
		 return patientRepository.getDoctorPatients(doctorCode,page).map(patientMapper::toDto);
	}

	@Override
	public Page<PatientDTO> getDocPatientSearch(String doctorCode,
			String searchKey, Pageable page) {
		// TODO Auto-generated method stub
		 return patientRepository.docPatientSearch(doctorCode,searchKey, page).map(patientMapper::toDto);
		
	}
	
	@Override
	public Page<PatientDTO> getAttorneyPatientSearch(String attorneyCode,
			String searchKey, Pageable page) {
		// TODO Auto-generated method stub
		 return patientRepository.attorneyPatientSearch(attorneyCode,searchKey, page).map(patientMapper::toDto);
		
	}

	@Override
	public Optional<PatientDTO> getPatientByCode(String patientCode) {
		log.debug("Request to fetch patients history For Attorney : {}", patientCode);
		 return patientRepository.getPatientByCode(patientCode).map(patientMapper::toDto);
	}

	@Override
	public Page<PatientDTO> patientsProcessedByAttorney(String attorneyCode,
			Pageable page) {
		// TODO Auto-generated method stub
		return patientRepository.patientsProcessedByAttorney(attorneyCode,page).map(patientMapper::toDto);	}
		

}
