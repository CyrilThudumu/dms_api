package com.dms.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.repository.PatientRepository;
import com.dms.service.ReportsService;
import com.dms.service.dto.ReportsDTO;

/**
 * Service Implementation for managing Patient.
 */
@Service
@Transactional
public class ReportsServiceImpl implements ReportsService{

	private final Logger log = LoggerFactory.getLogger(ReportsServiceImpl.class);

	private final PatientRepository patientRepository;


	public ReportsServiceImpl(PatientRepository patientRepository) {
		this.patientRepository = patientRepository;
	}

	
	/**
	 * Get all the patients.
	 *
	 * @param pageable the pagination information
	 * @return the list of entities
	 */
	/*@Override
	@Transactional(readOnly = true)
	public Page<PatientDTO> findAll(ReportsDTO reportsDTO, Pageable pageable) {
		log.debug("Request to get all Patients");
		return patientRepository.findAll(pageable).map(patientMapper::toDto);
	}*/


	@Override
	public Page<ReportsDTO> findByCriteria(ReportsDTO reportsDTO,
			Pageable pageable) {
		// TODO Auto-generated method stub
		//ReportsDTO  reportsDTO = new ReportsDTO(); 
		return null;
	}

}
