package com.dms.service.impl;

import com.dms.service.DocumententryService;
import com.dms.domain.Documententry;
import com.dms.repository.DocumententryRepository;
import com.dms.service.dto.DocumententryDTO;
import com.dms.service.mapper.DocumententryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Documententry.
 */
@Service
@Transactional
public class DocumententryServiceImpl implements DocumententryService {

    private final Logger log = LoggerFactory.getLogger(DocumententryServiceImpl.class);

    private final DocumententryRepository documententryRepository;

    private final DocumententryMapper documententryMapper;

    public DocumententryServiceImpl(DocumententryRepository documententryRepository, DocumententryMapper documententryMapper) {
        this.documententryRepository = documententryRepository;
        this.documententryMapper = documententryMapper;
    }

    /**
     * Save a documententry.
     *
     * @param documententryDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DocumententryDTO save(DocumententryDTO documententryDTO) {
        log.debug("Request to save Documententry : {}", documententryDTO);

        Documententry documententry = documententryMapper.toEntity(documententryDTO);
        documententry = documententryRepository.save(documententry);
        return documententryMapper.toDto(documententry);
    }

    /**
     * Get all the documententries.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DocumententryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Documententries");
        return documententryRepository.findAll(pageable)
            .map(documententryMapper::toDto);
    }


    /**
     * Get one documententry by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DocumententryDTO> findOne(Long id) {
        log.debug("Request to get Documententry : {}", id);
        return documententryRepository.findById(id)
            .map(documententryMapper::toDto);
    }

    /**
     * Delete the documententry by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Documententry : {}", id);
        documententryRepository.deleteById(id);
    }
}
