package com.dms.service.dto;

import java.io.Serializable;

import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the Documententry entity. This class is used in DocumententryResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /documententries?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DocumententryCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter documentCode;

    private StringFilter documentName;

    private LocalDateFilter fromField;

    private LocalDateFilter toField;

    private StringFilter notes;

    private StringFilter fromTo1;

    private StringFilter fromTo2;

    private StringFilter fromTo3;

    private StringFilter fromTo4;

    private StringFilter fromTo5;

    private StringFilter fromTo6;

    private StringFilter fromTo7;

    private StringFilter fromTo8;

    private StringFilter fromTo9;

    private LongFilter documenttypeForfromTo1Id;

    private LongFilter documenttypeForfromTo2Id;

    private LongFilter documenttypeForfromTo3Id;

    private LongFilter documenttypeForfromTo4Id;

    private LongFilter documenttypeForfromTo5Id;

    private LongFilter documenttypeForfromTo6Id;

    private LongFilter patientId;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDocumentCode() {
        return documentCode;
    }

    public void setDocumentCode(StringFilter documentCode) {
        this.documentCode = documentCode;
    }

    public StringFilter getDocumentName() {
        return documentName;
    }

    public void setDocumentName(StringFilter documentName) {
        this.documentName = documentName;
    }

    public LocalDateFilter getFromField() {
        return fromField;
    }

    public void setFromField(LocalDateFilter fromField) {
        this.fromField = fromField;
    }

    public LocalDateFilter getToField() {
        return toField;
    }

    public void setToField(LocalDateFilter toField) {
        this.toField = toField;
    }

    public StringFilter getNotes() {
        return notes;
    }

    public void setNotes(StringFilter notes) {
        this.notes = notes;
    }

    public StringFilter getFromTo1() {
        return fromTo1;
    }

    public void setFromTo1(StringFilter fromTo1) {
        this.fromTo1 = fromTo1;
    }

    public StringFilter getFromTo2() {
        return fromTo2;
    }

    public void setFromTo2(StringFilter fromTo2) {
        this.fromTo2 = fromTo2;
    }

    public StringFilter getFromTo3() {
        return fromTo3;
    }

    public void setFromTo3(StringFilter fromTo3) {
        this.fromTo3 = fromTo3;
    }

    public StringFilter getFromTo4() {
        return fromTo4;
    }

    public void setFromTo4(StringFilter fromTo4) {
        this.fromTo4 = fromTo4;
    }

    public StringFilter getFromTo5() {
        return fromTo5;
    }

    public void setFromTo5(StringFilter fromTo5) {
        this.fromTo5 = fromTo5;
    }

    public StringFilter getFromTo6() {
        return fromTo6;
    }

    public void setFromTo6(StringFilter fromTo6) {
        this.fromTo6 = fromTo6;
    }

    public StringFilter getFromTo7() {
        return fromTo7;
    }

    public void setFromTo7(StringFilter fromTo7) {
        this.fromTo7 = fromTo7;
    }

    public StringFilter getFromTo8() {
        return fromTo8;
    }

    public void setFromTo8(StringFilter fromTo8) {
        this.fromTo8 = fromTo8;
    }

    public StringFilter getFromTo9() {
        return fromTo9;
    }

    public void setFromTo9(StringFilter fromTo9) {
        this.fromTo9 = fromTo9;
    }

    public LongFilter getDocumenttypeForfromTo1Id() {
        return documenttypeForfromTo1Id;
    }

    public void setDocumenttypeForfromTo1Id(LongFilter documenttypeForfromTo1Id) {
        this.documenttypeForfromTo1Id = documenttypeForfromTo1Id;
    }

    public LongFilter getDocumenttypeForfromTo2Id() {
        return documenttypeForfromTo2Id;
    }

    public void setDocumenttypeForfromTo2Id(LongFilter documenttypeForfromTo2Id) {
        this.documenttypeForfromTo2Id = documenttypeForfromTo2Id;
    }

    public LongFilter getDocumenttypeForfromTo3Id() {
        return documenttypeForfromTo3Id;
    }

    public void setDocumenttypeForfromTo3Id(LongFilter documenttypeForfromTo3Id) {
        this.documenttypeForfromTo3Id = documenttypeForfromTo3Id;
    }

    public LongFilter getDocumenttypeForfromTo4Id() {
        return documenttypeForfromTo4Id;
    }

    public void setDocumenttypeForfromTo4Id(LongFilter documenttypeForfromTo4Id) {
        this.documenttypeForfromTo4Id = documenttypeForfromTo4Id;
    }

    public LongFilter getDocumenttypeForfromTo5Id() {
        return documenttypeForfromTo5Id;
    }

    public void setDocumenttypeForfromTo5Id(LongFilter documenttypeForfromTo5Id) {
        this.documenttypeForfromTo5Id = documenttypeForfromTo5Id;
    }

    public LongFilter getDocumenttypeForfromTo6Id() {
        return documenttypeForfromTo6Id;
    }

    public void setDocumenttypeForfromTo6Id(LongFilter documenttypeForfromTo6Id) {
        this.documenttypeForfromTo6Id = documenttypeForfromTo6Id;
    }

    public LongFilter getPatientId() {
        return patientId;
    }

    public void setPatientId(LongFilter patientId) {
        this.patientId = patientId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DocumententryCriteria that = (DocumententryCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(documentCode, that.documentCode) &&
            Objects.equals(documentName, that.documentName) &&
            Objects.equals(fromField, that.fromField) &&
            Objects.equals(toField, that.toField) &&
            Objects.equals(notes, that.notes) &&
            Objects.equals(fromTo1, that.fromTo1) &&
            Objects.equals(fromTo2, that.fromTo2) &&
            Objects.equals(fromTo3, that.fromTo3) &&
            Objects.equals(fromTo4, that.fromTo4) &&
            Objects.equals(fromTo5, that.fromTo5) &&
            Objects.equals(fromTo6, that.fromTo6) &&
            Objects.equals(fromTo7, that.fromTo7) &&
            Objects.equals(fromTo8, that.fromTo8) &&
            Objects.equals(fromTo9, that.fromTo9) &&
            Objects.equals(documenttypeForfromTo1Id, that.documenttypeForfromTo1Id) &&
            Objects.equals(documenttypeForfromTo2Id, that.documenttypeForfromTo2Id) &&
            Objects.equals(documenttypeForfromTo3Id, that.documenttypeForfromTo3Id) &&
            Objects.equals(documenttypeForfromTo4Id, that.documenttypeForfromTo4Id) &&
            Objects.equals(documenttypeForfromTo5Id, that.documenttypeForfromTo5Id) &&
            Objects.equals(documenttypeForfromTo6Id, that.documenttypeForfromTo6Id) &&
            Objects.equals(patientId, that.patientId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        documentCode,
        documentName,
        fromField,
        toField,
        notes,
        fromTo1,
        fromTo2,
        fromTo3,
        fromTo4,
        fromTo5,
        fromTo6,
        fromTo7,
        fromTo8,
        fromTo9,
        documenttypeForfromTo1Id,
        documenttypeForfromTo2Id,
        documenttypeForfromTo3Id,
        documenttypeForfromTo4Id,
        documenttypeForfromTo5Id,
        documenttypeForfromTo6Id,
        patientId
        );
    }

    @Override
    public String toString() {
        return "DocumententryCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (documentCode != null ? "documentCode=" + documentCode + ", " : "") +
                (documentName != null ? "documentName=" + documentName + ", " : "") +
                (fromField != null ? "fromField=" + fromField + ", " : "") +
                (toField != null ? "toField=" + toField + ", " : "") +
                (notes != null ? "notes=" + notes + ", " : "") +
                (fromTo1 != null ? "fromTo1=" + fromTo1 + ", " : "") +
                (fromTo2 != null ? "fromTo2=" + fromTo2 + ", " : "") +
                (fromTo3 != null ? "fromTo3=" + fromTo3 + ", " : "") +
                (fromTo4 != null ? "fromTo4=" + fromTo4 + ", " : "") +
                (fromTo5 != null ? "fromTo5=" + fromTo5 + ", " : "") +
                (fromTo6 != null ? "fromTo6=" + fromTo6 + ", " : "") +
                (fromTo7 != null ? "fromTo7=" + fromTo7 + ", " : "") +
                (fromTo8 != null ? "fromTo8=" + fromTo8 + ", " : "") +
                (fromTo9 != null ? "fromTo9=" + fromTo9 + ", " : "") +
                (documenttypeForfromTo1Id != null ? "documenttypeForfromTo1Id=" + documenttypeForfromTo1Id + ", " : "") +
                (documenttypeForfromTo2Id != null ? "documenttypeForfromTo2Id=" + documenttypeForfromTo2Id + ", " : "") +
                (documenttypeForfromTo3Id != null ? "documenttypeForfromTo3Id=" + documenttypeForfromTo3Id + ", " : "") +
                (documenttypeForfromTo4Id != null ? "documenttypeForfromTo4Id=" + documenttypeForfromTo4Id + ", " : "") +
                (documenttypeForfromTo5Id != null ? "documenttypeForfromTo5Id=" + documenttypeForfromTo5Id + ", " : "") +
                (documenttypeForfromTo6Id != null ? "documenttypeForfromTo6Id=" + documenttypeForfromTo6Id + ", " : "") +
                (patientId != null ? "patientId=" + patientId + ", " : "") +
            "}";
    }

}
