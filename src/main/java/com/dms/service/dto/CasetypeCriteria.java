package com.dms.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the Casetype entity. This class is used in CasetypeResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /casetypes?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CasetypeCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter caseTypeCode;

    private StringFilter caseTypeName;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCaseTypeCode() {
        return caseTypeCode;
    }

    public void setCaseTypeCode(StringFilter caseTypeCode) {
        this.caseTypeCode = caseTypeCode;
    }

    public StringFilter getCaseTypeName() {
        return caseTypeName;
    }

    public void setCaseTypeName(StringFilter caseTypeName) {
        this.caseTypeName = caseTypeName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CasetypeCriteria that = (CasetypeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(caseTypeCode, that.caseTypeCode) &&
            Objects.equals(caseTypeName, that.caseTypeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        caseTypeCode,
        caseTypeName
        );
    }

    @Override
    public String toString() {
        return "CasetypeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (caseTypeCode != null ? "caseTypeCode=" + caseTypeCode + ", " : "") +
                (caseTypeName != null ? "caseTypeName=" + caseTypeName + ", " : "") +
            "}";
    }

}
