package com.dms.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the Patient entity. This class is used in PatientResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /patients?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PatientCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;
    
    private StringFilter practice;

    private StringFilter patientCode;

    private StringFilter firstName;

    private StringFilter lastName;

    private StringFilter sex;

    private StringFilter address;

    private StringFilter city;

    private StringFilter state;

    private IntegerFilter zipCode;

    private StringFilter email;

    private StringFilter mobileNo;

    private BooleanFilter enable;

    private StringFilter patientStatus;

    private StringFilter statusNotes;

    private LocalDateFilter dob;

    private StringFilter caseType;

    private StringFilter workPhone;

    private IntegerFilter age;
    
    private StringFilter insFirstName;

    private StringFilter insLastName;

    private StringFilter insInitial;

    private StringFilter insAddress;

    private StringFilter insCity;

    private StringFilter insState;

    private IntegerFilter insZipCode;

    private StringFilter insPhoneNo;

    private StringFilter patientInsCo;

    private StringFilter primaryInsClaimNo;

    private StringFilter primaryInsPolicyNo;

    private StringFilter primaryInsRelation;

    private StringFilter primaryInsCertifier;

    private StringFilter primaryInsAdjuster;
    
    private StringFilter primaryInsAdjusterFax;
    
    private StringFilter primaryInsNotes;    
    
      
    private LocalDateFilter doa;

    private StringFilter secInsClaimNo;

    private StringFilter secInsPolicyNo;

    private StringFilter secInsRelation;

    private StringFilter secInsNotes;
 
    private StringFilter secInsCertifier;
    
    private StringFilter secInsAdjuster;
    
    private StringFilter secInsAdjusterFax;
    
    private StringFilter addnInfo;
    
    private StringFilter attorneyFirstName;
    
    private StringFilter attorneyLastName;
    
    private StringFilter attoneyAddress;

    private StringFilter attorneyCity;

    private StringFilter attorneyState;

    private IntegerFilter attoneyZipCode;
    
    private StringFilter attorneyMobileNo;
    
    private StringFilter attorneyFaxNo;
    
    private StringFilter practiceCode;

    private StringFilter createdBy;

    private LocalDateFilter creationDate;

    private StringFilter updatedBy;

    private LocalDateFilter updatedDate;
    
    private StringFilter attorneyLogin;

    private StringFilter doctorLogin;

    private LongFilter doctorId;

    private LongFilter edocumentId;
    
    
    

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getPatientCode() {
        return patientCode;
    }

    public void setPatientCode(StringFilter patientCode) {
        this.patientCode = patientCode;
    }

    public StringFilter getFirstName() {
        return firstName;
    }

    public void setFirstName(StringFilter firstName) {
        this.firstName = firstName;
    }

    public StringFilter getLastName() {
        return lastName;
    }

    public void setLastName(StringFilter lastName) {
        this.lastName = lastName;
    }

    public StringFilter getSex() {
        return sex;
    }

    public void setSex(StringFilter sex) {
        this.sex = sex;
    }

    public StringFilter getAddress() {
        return address;
    }

    public void setAddress(StringFilter address) {
        this.address = address;
    }

    public StringFilter getCity() {
        return city;
    }

    public void setCity(StringFilter city) {
        this.city = city;
    }

    public StringFilter getState() {
        return state;
    }

    public void setState(StringFilter state) {
        this.state = state;
    }

    public IntegerFilter getZipCode() {
        return zipCode;
    }

    public void setZipCode(IntegerFilter zipCode) {
        this.zipCode = zipCode;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(StringFilter mobileNo) {
        this.mobileNo = mobileNo;
    }

    public BooleanFilter getEnable() {
        return enable;
    }

    public void setEnable(BooleanFilter enable) {
        this.enable = enable;
    }

    public StringFilter getPatientStatus() {
        return patientStatus;
    }

    public void setPatientStatus(StringFilter patientStatus) {
        this.patientStatus = patientStatus;
    }

    public StringFilter getStatusNotes() {
        return statusNotes;
    }

    public void setStatusNotes(StringFilter statusNotes) {
        this.statusNotes = statusNotes;
    }

    public LocalDateFilter getDob() {
        return dob;
    }

    public void setDob(LocalDateFilter dob) {
        this.dob = dob;
    }

    public StringFilter getCaseType() {
        return caseType;
    }

    public void setCaseType(StringFilter caseType) {
        this.caseType = caseType;
    }

    public StringFilter getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(StringFilter workPhone) {
        this.workPhone = workPhone;
    }

    public IntegerFilter getAge() {
        return age;
    }

    public void setAge(IntegerFilter age) {
        this.age = age;
    }

    public StringFilter getInsFirstName() {
		return insFirstName;
	}

	public void setInsFirstName(StringFilter insFirstName) {
		this.insFirstName = insFirstName;
	}

	public StringFilter getInsLastName() {
		return insLastName;
	}

	public void setInsLastName(StringFilter insLastName) {
		this.insLastName = insLastName;
	}

	public StringFilter getInsInitial() {
		return insInitial;
	}

	public void setInsInitial(StringFilter insInitial) {
		this.insInitial = insInitial;
	}

	public StringFilter getInsAddress() {
		return insAddress;
	}

	public void setInsAddress(StringFilter insAddress) {
		this.insAddress = insAddress;
	}

	public StringFilter getInsCity() {
		return insCity;
	}

	public void setInsCity(StringFilter insCity) {
		this.insCity = insCity;
	}

	public StringFilter getInsState() {
		return insState;
	}

	public void setInsState(StringFilter insState) {
		this.insState = insState;
	}

	public IntegerFilter getInsZipCode() {
		return insZipCode;
	}

	public void setInsZipCode(IntegerFilter insZipCode) {
		this.insZipCode = insZipCode;
	}

	public StringFilter getInsPhoneNo() {
		return insPhoneNo;
	}

	public void setInsPhoneNo(StringFilter insPhoneNo) {
		this.insPhoneNo = insPhoneNo;
	}

	public StringFilter getPrimaryInsCo() {
        return patientInsCo;
    }

    public void setPrimaryInsCo(StringFilter primaryInsCo) {
        this.patientInsCo = primaryInsCo;
    }

    public StringFilter getPrimaryInsClaimNo() {
        return primaryInsClaimNo;
    }

    public void setPrimaryInsClaimNo(StringFilter primaryInsClaimNo) {
        this.primaryInsClaimNo = primaryInsClaimNo;
    }

    public StringFilter getPrimaryInsPolicyNo() {
        return primaryInsPolicyNo;
    }

    public void setPrimaryInsPolicyNo(StringFilter primaryInsPolicyNo) {
        this.primaryInsPolicyNo = primaryInsPolicyNo;
    }

    public StringFilter getPrimaryInsRelation() {
        return primaryInsRelation;
    }

    public void setPrimaryInsRelation(StringFilter primaryInsRelation) {
        this.primaryInsRelation = primaryInsRelation;
    }

    public StringFilter getPrimaryInsNotes() {
        return primaryInsNotes;
    }

    public void setPrimaryInsNotes(StringFilter primaryInsNotes) {
        this.primaryInsNotes = primaryInsNotes;
    }

    public LocalDateFilter getDoa() {
        return doa;
    }

    public void setDoa(LocalDateFilter doa) {
        this.doa = doa;
    }

    public StringFilter getSecInsClaimNo() {
        return secInsClaimNo;
    }

    public void setSecInsClaimNo(StringFilter secInsClaimNo) {
        this.secInsClaimNo = secInsClaimNo;
    }

    public StringFilter getSecInsPolicyNo() {
        return secInsPolicyNo;
    }

    public void setSecInsPolicyNo(StringFilter secInsPolicyNo) {
        this.secInsPolicyNo = secInsPolicyNo;
    }

    public StringFilter getSecInsRelation() {
        return secInsRelation;
    }

    public void setSecInsRelation(StringFilter secInsRelation) {
        this.secInsRelation = secInsRelation;
    }

    public StringFilter getSecInsNotes() {
        return secInsNotes;
    }

    public void setSecInsNotes(StringFilter secInsNotes) {
        this.secInsNotes = secInsNotes;
    }

    public StringFilter getAddnInfo() {
        return addnInfo;
    }

    public void setAddnInfo(StringFilter addnInfo) {
        this.addnInfo = addnInfo;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateFilter getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateFilter creationDate) {
        this.creationDate = creationDate;
    }

    public StringFilter getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(StringFilter updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDateFilter getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateFilter updatedDate) {
        this.updatedDate = updatedDate;
    }

    
    
    
//    public LongFilter getAttornyId() {
//        return attornyId;
//    }
//
//    public void setAttornyId(LongFilter attornyId) {
//        this.attornyId = attornyId;
//    }

    public StringFilter getPatientInsCo() {
		return patientInsCo;
	}

	public void setPatientInsCo(StringFilter patientInsCo) {
		this.patientInsCo = patientInsCo;
	}

	public StringFilter getPrimaryInsCertifier() {
		return primaryInsCertifier;
	}

	public void setPrimaryInsCertifier(StringFilter primaryInsCertifier) {
		this.primaryInsCertifier = primaryInsCertifier;
	}

	public StringFilter getPrimaryInsAdjuster() {
		return primaryInsAdjuster;
	}

	public void setPrimaryInsAdjuster(StringFilter primaryInsAdjuster) {
		this.primaryInsAdjuster = primaryInsAdjuster;
	}

	public StringFilter getPrimaryInsAdjusterFax() {
		return primaryInsAdjusterFax;
	}

	public void setPrimaryInsAdjusterFax(StringFilter primaryInsAdjusterFax) {
		this.primaryInsAdjusterFax = primaryInsAdjusterFax;
	}

	
	public StringFilter getSecInsCertifier() {
		return secInsCertifier;
	}

	public void setSecInsCertifier(StringFilter secInsCertifier) {
		this.secInsCertifier = secInsCertifier;
	}

	public StringFilter getSecInsAdjuster() {
		return secInsAdjuster;
	}

	public void setSecInsAdjuster(StringFilter secInsAdjuster) {
		this.secInsAdjuster = secInsAdjuster;
	}

	public StringFilter getSecInsAdjusterFax() {
		return secInsAdjusterFax;
	}

	public void setSecInsAdjusterFax(StringFilter secInsAdjusterFax) {
		this.secInsAdjusterFax = secInsAdjusterFax;
	}

	public StringFilter getAttorneyLastName() {
		return attorneyLastName;
	}

	public void setAttorneyLastName(StringFilter attorneyLastName) {
		this.attorneyLastName = attorneyLastName;
	}

	public StringFilter getAttoneyAddress() {
		return attoneyAddress;
	}

	public void setAttoneyAddress(StringFilter attoneyAddress) {
		this.attoneyAddress = attoneyAddress;
	}

	public StringFilter getAttorneyCity() {
		return attorneyCity;
	}

	public void setAttorneyCity(StringFilter attorneyCity) {
		this.attorneyCity = attorneyCity;
	}

	public StringFilter getAttorneyState() {
		return attorneyState;
	}

	public void setAttorneyState(StringFilter attorneyState) {
		this.attorneyState = attorneyState;
	}

	public IntegerFilter getAttoneyZipCode() {
		return attoneyZipCode;
	}

	public void setAttoneyZipCode(IntegerFilter attoneyZipCode) {
		this.attoneyZipCode = attoneyZipCode;
	}

	public StringFilter getAttorneyMobileNo() {
		return attorneyMobileNo;
	}

	public void setAttorneyMobileNo(StringFilter attorneyMobileNo) {
		this.attorneyMobileNo = attorneyMobileNo;
	}

	public StringFilter getAttorneyFaxNo() {
		return attorneyFaxNo;
	}

	public void setAttorneyFaxNo(StringFilter attorneyFaxNo) {
		this.attorneyFaxNo = attorneyFaxNo;
	}

	public StringFilter getPracticeCode() {
		return practiceCode;
	}

	public void setPracticeCode(StringFilter practiceCode) {
		this.practiceCode = practiceCode;
	}

	public StringFilter getAttorneyLogin() {
		return attorneyLogin;
	}

	public void setAttornyLogin(StringFilter attorneyLogin) {
		this.attorneyLogin = attorneyLogin;
	}

	public StringFilter getDoctorLogin() {
		return doctorLogin;
	}

	public void setDoctorLogin(StringFilter doctorLogin) {
		this.doctorLogin = doctorLogin;
	}

	public LongFilter getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(LongFilter doctorId) {
        this.doctorId = doctorId;
    }

    public LongFilter getEdocumentId() {
        return edocumentId;
    }

    public void setEdocumentId(LongFilter edocumentId) {
        this.edocumentId = edocumentId;
    }

    

    public StringFilter getAttorneyFirstName() {
		return attorneyFirstName;
	}

	public void setAttorneyFirstName(StringFilter attorneyFirstName) {
		this.attorneyFirstName = attorneyFirstName;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PatientCriteria that = (PatientCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(practice, that.practice)&&
            Objects.equals(patientCode, that.patientCode) &&
            Objects.equals(firstName, that.firstName) &&
            Objects.equals(lastName, that.lastName) &&
            Objects.equals(sex, that.sex) &&
            Objects.equals(address, that.address) &&
            Objects.equals(city, that.city) &&
            Objects.equals(state, that.state) &&
            Objects.equals(zipCode, that.zipCode) &&
            Objects.equals(email, that.email) &&
            Objects.equals(mobileNo, that.mobileNo) &&
            Objects.equals(enable, that.enable) &&
            Objects.equals(patientStatus, that.patientStatus) &&
            Objects.equals(statusNotes, that.statusNotes) &&
            Objects.equals(dob, that.dob) &&
            Objects.equals(caseType, that.caseType) &&
            Objects.equals(workPhone, that.workPhone) &&
            Objects.equals(age, that.age) &&
            Objects.equals(insFirstName, that.insFirstName) &&
            Objects.equals(insLastName, that.insLastName) &&
            Objects.equals(insInitial, that.insInitial) &&
            Objects.equals(insAddress, that.insAddress) &&
            Objects.equals(insCity, that.insCity) &&
            Objects.equals(insState, that.insState) &&
            Objects.equals(insZipCode, that.insZipCode) &&
            Objects.equals(insPhoneNo, that.insPhoneNo) &&
            Objects.equals(patientInsCo, that.patientInsCo) &&
            Objects.equals(primaryInsClaimNo, that.primaryInsClaimNo) &&
            Objects.equals(primaryInsPolicyNo, that.primaryInsPolicyNo) &&
            Objects.equals(primaryInsRelation, that.primaryInsRelation) &&
            Objects.equals(primaryInsNotes, that.primaryInsNotes) &&
            Objects.equals(doa, that.doa) &&
            Objects.equals(secInsClaimNo, that.secInsClaimNo) &&
            Objects.equals(secInsPolicyNo, that.secInsPolicyNo) &&
            Objects.equals(secInsRelation, that.secInsRelation) &&
            Objects.equals(secInsNotes, that.secInsNotes) &&
            Objects.equals(addnInfo, that.addnInfo) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(creationDate, that.creationDate) &&
            Objects.equals(updatedBy, that.updatedBy) &&
            Objects.equals(updatedDate, that.updatedDate) &&
//            Objects.equals(attornyId, that.attornyId) &&
            Objects.equals(doctorId, that.doctorId) &&
            Objects.equals(edocumentId, that.edocumentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        practice,
        patientCode,
        firstName,
        lastName,
        sex,
        address,
        city,
        state,
        zipCode,
        email,
        mobileNo,
        enable,
        patientStatus,
        statusNotes,
        dob,
        caseType,
        workPhone,
        age,
        insFirstName,
        insLastName,
        insInitial,
        insAddress,
        insCity,
        insState,
        insZipCode,
        insPhoneNo,
        patientInsCo,
        primaryInsClaimNo,
        primaryInsPolicyNo,
        primaryInsRelation,
        primaryInsNotes,
        doa,
        secInsClaimNo,
        secInsPolicyNo,
        secInsRelation,
        secInsNotes,
        addnInfo,
        createdBy,
        creationDate,
        updatedBy,
        updatedDate,
//        attornyId,
        doctorId,
        edocumentId
        );
    }

    @Override
    public String toString() {
        return "PatientCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (practice != null ? "practice=" + practice + ", " : "") +
                (patientCode != null ? "patientCode=" + patientCode + ", " : "") +
                (firstName != null ? "firstName=" + firstName + ", " : "") +
                (lastName != null ? "lastName=" + lastName + ", " : "") +
                (sex != null ? "sex=" + sex + ", " : "") +
                (address != null ? "address=" + address + ", " : "") +
                (city != null ? "city=" + city + ", " : "") +
                (state != null ? "state=" + state + ", " : "") +
                (zipCode != null ? "zipCode=" + zipCode + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (mobileNo != null ? "mobileNo=" + mobileNo + ", " : "") +
                (enable != null ? "enable=" + enable + ", " : "") +
                (patientStatus != null ? "patientStatus=" + patientStatus + ", " : "") +
                (statusNotes != null ? "statusNotes=" + statusNotes + ", " : "") +
                (dob != null ? "dob=" + dob + ", " : "") +
                (caseType != null ? "caseType=" + caseType + ", " : "") +
                (workPhone != null ? "workPhone=" + workPhone + ", " : "") +
                (age != null ? "age=" + age + ", " : "") +
                (insFirstName != null ? "insFirstName=" + insFirstName + ", " : "") +
                (insLastName != null ? "insLastName=" + insLastName + ", " : "") +
                (insInitial != null ? "insInitial=" + insInitial + ", " : "") +
                (insAddress != null ? "insAddress=" + insAddress + ", " : "") +
                (insCity != null ? "insCity=" + insCity + ", " : "") +
                (insState != null ? "insState=" + insState + ", " : "") +
                (insZipCode != null ? "insZipCode=" + insZipCode + ", " : "") +
                (insPhoneNo != null ? "insPhoneNo=" + insPhoneNo + ", " : "") +
                (patientInsCo != null ? "primaryInsCo=" + patientInsCo + ", " : "") +
                (primaryInsClaimNo != null ? "primaryInsClaimNo=" + primaryInsClaimNo + ", " : "") +
                (primaryInsPolicyNo != null ? "primaryInsPolicyNo=" + primaryInsPolicyNo + ", " : "") +
                (primaryInsRelation != null ? "primaryInsRelation=" + primaryInsRelation + ", " : "") +
                (primaryInsNotes != null ? "primaryInsNotes=" + primaryInsNotes + ", " : "") +
                (doa != null ? "doa=" + doa + ", " : "") +
                (secInsClaimNo != null ? "secInsClaimNo=" + secInsClaimNo + ", " : "") +
                (secInsPolicyNo != null ? "secInsPolicyNo=" + secInsPolicyNo + ", " : "") +
                (secInsRelation != null ? "secInsRelation=" + secInsRelation + ", " : "") +
                (secInsNotes != null ? "secInsNotes=" + secInsNotes + ", " : "") +
                (addnInfo != null ? "addnInfo=" + addnInfo + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (creationDate != null ? "creationDate=" + creationDate + ", " : "") +
                (updatedBy != null ? "updatedBy=" + updatedBy + ", " : "") +
                (updatedDate != null ? "updatedDate=" + updatedDate + ", " : "") +
//                (attornyId != null ? "attornyId=" + attornyId + ", " : "") +
                (doctorId != null ? "doctorId=" + doctorId + ", " : "") +
                (edocumentId != null ? "edocumentId=" + edocumentId + ", " : "") +
            "}";
    }

	public StringFilter getPractice() {
		return practice;
	}

	public void setPractice(StringFilter practice) {
		this.practice = practice;
	}

}
