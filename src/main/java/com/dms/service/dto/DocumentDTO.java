package com.dms.service.dto;

import java.util.ArrayList;

import javax.validation.constraints.Size;

public class DocumentDTO {



	//change it to document name in domain.
	private ArrayList<String> documentTypes;
	private ArrayList<String> dosFrom;
	private ArrayList<String> dosTo;

	@Size(max = 20)
	public String doa;
	
	@Size(max = 20)
	private String dob;
	
	private ArrayList<String> pageNumber;
	
	@Size(max = 20)
	//change it to practiceCode later
	private String practice;

	private String searchBy;
	
	private String searchValue;
	
	//change it to patientId later
	@Size(max = 10)
	private String patientItem;
	
	private String pageNumberStr;

	@Size(max = 15)
	//change it to doctorId later.
	private String doctor;
	
	@Size(max = 10)
	private String attorneyId;
	
	@Size(max = 50)
	private String filePath;

	public ArrayList<String> getDocumentTypes() {
		return documentTypes;
	}

	public void setDocumentTypes(ArrayList<String> documentTypes) {
		this.documentTypes = documentTypes;
	}

	public ArrayList<String> getDosFrom() {
		return dosFrom;
	}

	public void setDosFrom(ArrayList<String> dosFrom) {
		this.dosFrom = dosFrom;
	}

	public ArrayList<String> getDosTo() {
		return dosTo;
	}

	public void setDosTo(ArrayList<String> dosTo) {
		this.dosTo = dosTo;
	}

	public String getDoa() {
		return doa;
	}

	public void setDoa(String doa) {
		this.doa = doa;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public ArrayList<String> getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(ArrayList<String> pageNumber) {
		this.pageNumber = pageNumber;
	}

	public String getPractice() {
		return practice;
	}

	public void setPractice(String practice) {
		this.practice = practice;
	}

	public String getSearchBy() {
		return searchBy;
	}

	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public String getPatientItem() {
		return patientItem;
	}

	public void setPatientItem(String patientItem) {
		this.patientItem = patientItem;
	}

	public String getPageNumberStr() {
		return pageNumberStr;
	}

	public void setPageNumberStr(String pageNumberStr) {
		this.pageNumberStr = pageNumberStr;
	}

	public String getDoctor() {
		return doctor;
	}

	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}

	public String getAttorneyId() {
		return attorneyId;
	}

	public void setAttorneyId(String attorneyId) {
		this.attorneyId = attorneyId;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	

	@Override
	public String toString() {
		return "DocumentDTO [documentTypes=" + documentTypes + ", dosFrom="
				+ dosFrom + ", dosTo=" + dosTo + ", doa=" + doa + ", dob="
				+ dob + ", pageNumber=" + pageNumber + ", practice=" + practice
				+ ", searchBy=" + searchBy + ", searchValue=" + searchValue
				+ ", patientItem=" + patientItem + ", pageNumberStr="
				+ pageNumberStr + ", doctor=" + doctor + ", attorneyId="
				+ attorneyId + ", filePath=" + filePath + "]";
	}
	
	
	
	
}
