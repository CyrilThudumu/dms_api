package com.dms.service.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.jpa.domain.Specification;

import com.dms.domain.Documents;

public class ReportsCriteria implements Serializable {

	private final List<ReportsSearchCriteria> params;
	 
    public ReportsCriteria() {
        params = new ArrayList<ReportsSearchCriteria>();
    }
 
    public ReportsCriteria with(String key, String operation, Object value) {
        params.add(new ReportsSearchCriteria(key, operation, value));
        return this;
    }
 
    public Specification<Documents> build() {
        if (params.size() == 0) {
            return null;
        }
 
        List<Specification> specs = params.stream()
          .map(ReportsSpecification::new)
          .collect(Collectors.toList());
         
        Specification result = specs.get(0);
 
        for (int i = 1; i < params.size(); i++) {
            result = params.get(i)
              .isOrPredicate()
                ? Specification.where(result)
                  .or(specs.get(i))
                : Specification.where(result)
                  .and(specs.get(i));
        }       
        return result;
    }
}
