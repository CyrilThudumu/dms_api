package com.dms.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Casetype entity.
 */
public class CasetypeDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 20)
    private String caseTypeCode;

    @NotNull
    @Size(max = 20)
    private String caseTypeName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaseTypeCode() {
        return caseTypeCode;
    }

    public void setCaseTypeCode(String caseTypeCode) {
        this.caseTypeCode = caseTypeCode;
    }

    public String getCaseTypeName() {
        return caseTypeName;
    }

    public void setCaseTypeName(String caseTypeName) {
        this.caseTypeName = caseTypeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CasetypeDTO casetypeDTO = (CasetypeDTO) o;
        if (casetypeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), casetypeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CasetypeDTO{" +
            "id=" + getId() +
            ", caseTypeCode='" + getCaseTypeCode() + "'" +
            ", caseTypeName='" + getCaseTypeName() + "'" +
            "}";
    }
}
