package com.dms.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Insurance entity.
 */
public class InsuranceDTO implements Serializable {

    private Long id;

    @Size(max = 20)
    private String insuranceCode;

    @NotNull
    @Size(max = 20)
    private String insuranceName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInsuranceCode() {
        return insuranceCode;
    }

    public void setInsuranceCode(String insuranceCode) {
        this.insuranceCode = insuranceCode;
    }

    public String getInsuranceName() {
        return insuranceName;
    }

    public void setInsuranceName(String insuranceName) {
        this.insuranceName = insuranceName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InsuranceDTO insuranceDTO = (InsuranceDTO) o;
        if (insuranceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), insuranceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InsuranceDTO{" +
            "id=" + getId() +
            ", insuranceCode='" + getInsuranceCode() + "'" +
            ", insuranceName='" + getInsuranceName() + "'" +
            "}";
    }
}
