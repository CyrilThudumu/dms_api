package com.dms.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

/**
 * A DTO for the Patient entity.
 */
public class PatientDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8791522842650538253L;

	private Long id;

    @NotNull
    private String patientCode;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    private String initial;
    
    private String sex;

    private String address;

    private String city;

    private String state;

    
    private Integer zipCode;

    private String email;

    private String mobileNo;

    private Boolean enable;

    private String patientStatus;

    private String statusNotes;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dob;

    private String caseType;

    private String workPhone;

    private String homePhone; 
    
    private Integer age;

    private String insFirstName;

    private String insLastName;

    private String insInitial;

    private String insAddress;

    private String insCity;

    private String insState;

    private Integer insZipCode;

    private String insPhoneNo;

    private String primaryInsCo;

    private String primaryInsClaimNo;

    private String primaryInsPolicyNo;

    private String primaryInsRelation;
    
    private String primaryInsCertifier;

    private String primaryInsAdjuster;
    
    private String primaryInsAdjusterFax;
    
    private String primaryInsNotes;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate doa;
    

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)    
    private LocalDate firstVisit;
    
    private String secInsCo;
    

    private String secInsClaimNo;

    private String secInsPolicyNo;

    private String secInsRelation;

    private String secInsNotes;
    
    private String secInsCertifier;
    
    private String secInsAdjuster;
    
    private String secInsAdjusterFax;
    
    private String addnInfo;

    private String attorneyFirstName;

    
    private String attorneyLastName;
    
    private String attoneyAddress;

    private String attorneyCity;

    private String attorneyState;

    private Integer attoneyZipCode;
    
    private String attorneyMobileNo;
    
    private String attorneyFaxNo;
    
    private String practiceCode;
    
    private String missingInfo;
    
    private String createdBy;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate creationDate;

    private String updatedBy;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate updatedDate;

//    private Long attornyId;

    private String attornyLogin;

    private Long doctorId;

    private String doctorLogin;

    private Set<EdocumentDTO> edocuments = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPatientCode() {
        return patientCode;
    }

    public void setPatientCode(String patientCode) {
        this.patientCode = patientCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    
    public String getInitial() {
		return initial;
	}

	public void setInitial(String initial) {
		this.initial = initial;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getZipCode() {
        return zipCode;
    }

    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Boolean isEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getPatientStatus() {
        return patientStatus;
    }

    public void setPatientStatus(String patientStatus) {
        this.patientStatus = patientStatus;
    }

    public String getStatusNotes() {
        return statusNotes;
    }

    public void setStatusNotes(String statusNotes) {
        this.statusNotes = statusNotes;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getInsFirstName() {
        return insFirstName;
    }

    public void setInsFirstName(String insFirstName) {
        this.insFirstName = insFirstName;
    }

    public String getInsLastName() {
        return insLastName;
    }

    public void setInsLastName(String insLastName) {
        this.insLastName = insLastName;
    }

    public String getInsInitial() {
        return insInitial;
    }

    public void setInsInitial(String insInitial) {
        this.insInitial = insInitial;
    }

    public String getInsAddress() {
        return insAddress;
    }

    public void setInsAddress(String insAddress) {
        this.insAddress = insAddress;
    }

    public String getInsCity() {
        return insCity;
    }

    public void setInsCity(String insCity) {
        this.insCity = insCity;
    }

    public String getInsState() {
        return insState;
    }

    public void setInsState(String insState) {
        this.insState = insState;
    }

    public Integer getInsZipCode() {
        return insZipCode;
    }

    public void setInsZipCode(Integer insZipCode) {
        this.insZipCode = insZipCode;
    }

    public String getInsPhoneNo() {
        return insPhoneNo;
    }

    public void setInsPhoneNo(String insPhoneNo) {
        this.insPhoneNo = insPhoneNo;
    }

    public String getPrimaryInsCo() {
        return primaryInsCo;
    }

    public void setPrimaryInsCo(String primaryInsCo) {
        this.primaryInsCo = primaryInsCo;
    }

    public String getPrimaryInsClaimNo() {
        return primaryInsClaimNo;
    }

    public void setPrimaryInsClaimNo(String primaryInsClaimNo) {
        this.primaryInsClaimNo = primaryInsClaimNo;
    }

    public String getPrimaryInsPolicyNo() {
        return primaryInsPolicyNo;
    }

    public void setPrimaryInsPolicyNo(String primaryInsPolicyNo) {
        this.primaryInsPolicyNo = primaryInsPolicyNo;
    }

    public String getPrimaryInsRelation() {
        return primaryInsRelation;
    }

    public void setPrimaryInsRelation(String primaryInsRelation) {
        this.primaryInsRelation = primaryInsRelation;
    }

    public String getPrimaryInsNotes() {
        return primaryInsNotes;
    }

    public void setPrimaryInsNotes(String primaryInsNotes) {
        this.primaryInsNotes = primaryInsNotes;
    }

    public LocalDate getDoa() {
        return doa;
    }

    public void setDoa(LocalDate doa) {
        this.doa = doa;
    }

    public String getSecInsClaimNo() {
        return secInsClaimNo;
    }

    public void setSecInsClaimNo(String secInsClaimNo) {
        this.secInsClaimNo = secInsClaimNo;
    }

    public String getSecInsPolicyNo() {
        return secInsPolicyNo;
    }

    public void setSecInsPolicyNo(String secInsPolicyNo) {
        this.secInsPolicyNo = secInsPolicyNo;
    }

    public String getSecInsRelation() {
        return secInsRelation;
    }

    public void setSecInsRelation(String secInsRelation) {
        this.secInsRelation = secInsRelation;
    }

    public String getSecInsNotes() {
        return secInsNotes;
    }

    public void setSecInsNotes(String secInsNotes) {
        this.secInsNotes = secInsNotes;
    }

    public String getAddnInfo() {
        return addnInfo;
    }

    public void setAddnInfo(String addnInfo) {
        this.addnInfo = addnInfo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }

//    public Long getAttornyId() {
//        return attornyId;
//    }
//
//    public void setAttornyId(Long userId) {
//        this.attornyId = userId;
//    }

    public String getAttornyLogin() {
        return attornyLogin;
    }

    public void setAttornyLogin(String userLogin) {
        this.attornyLogin = userLogin;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long userId) {
        this.doctorId = userId;
    }

    public String getDoctorLogin() {
        return doctorLogin;
    }

    public void setDoctorLogin(String userLogin) {
        this.doctorLogin = userLogin;
    }

    public Set<EdocumentDTO> getEdocuments() {
        return edocuments;
    }

    public void setEdocuments(Set<EdocumentDTO> edocuments) {
        this.edocuments = edocuments;
    }
    


	public String getPrimaryInsCertifier() {
		return primaryInsCertifier;
	}

	public void setPrimaryInsCertifier(String primaryInsCertifier) {
		this.primaryInsCertifier = primaryInsCertifier;
	}

	public String getPrimaryInsAdjuster() {
		return primaryInsAdjuster;
	}

	public void setPrimaryInsAdjuster(String primaryInsAdjuster) {
		this.primaryInsAdjuster = primaryInsAdjuster;
	}

	public String getPrimaryInsAdjusterFax() {
		return primaryInsAdjusterFax;
	}

	public void setPrimaryInsAdjusterFax(String primaryInsAdjusterFax) {
		this.primaryInsAdjusterFax = primaryInsAdjusterFax;
	}

	public LocalDate getFirstVisit() {
		return firstVisit;
	}

	public void setFirstVisit(LocalDate firstVisit) {
		this.firstVisit = firstVisit;
	}

	public String getSecInsCo() {
		return secInsCo;
	}

	public void setSecInsCo(String secInsCo) {
		this.secInsCo = secInsCo;
	}

	public String getSecInsCertifier() {
		return secInsCertifier;
	}

	public void setSecInsCertifier(String secInsCertifier) {
		this.secInsCertifier = secInsCertifier;
	}

	public String getSecInsAdjuster() {
		return secInsAdjuster;
	}

	public void setSecInsAdjuster(String secInsAdjuster) {
		this.secInsAdjuster = secInsAdjuster;
	}

	public String getSecInsAdjusterFax() {
		return secInsAdjusterFax;
	}

	public void setSecInsAdjusterFax(String secInsAdjusterFax) {
		this.secInsAdjusterFax = secInsAdjusterFax;
	}

	public String getAttorneyFirstName() {
		return attorneyFirstName;
	}

	public void setAttorneyFirstName(String attorneyFirstName) {
		this.attorneyFirstName = attorneyFirstName;
	}

	public String getAttorneyLastName() {
		return attorneyLastName;
	}

	public void setAttorneyLastName(String attorneyLastName) {
		this.attorneyLastName = attorneyLastName;
	}

	public String getAttoneyAddress() {
		return attoneyAddress;
	}

	public void setAttoneyAddress(String attoneyAddress) {
		this.attoneyAddress = attoneyAddress;
	}

	public String getAttorneyCity() {
		return attorneyCity;
	}

	public void setAttorneyCity(String attorneyCity) {
		this.attorneyCity = attorneyCity;
	}

	public String getAttorneyState() {
		return attorneyState;
	}

	public void setAttorneyState(String attorneyState) {
		this.attorneyState = attorneyState;
	}

	
	public Integer getAttoneyZipCode() {
		return attoneyZipCode;
	}

	public void setAttoneyZipCode(Integer attoneyZipCode) {
		this.attoneyZipCode = attoneyZipCode;
	}

	public String getAttorneyMobileNo() {
		return attorneyMobileNo;
	}

	public void setAttorneyMobileNo(String attorneyMobileNo) {
		this.attorneyMobileNo = attorneyMobileNo;
	}

	public String getAttorneyFaxNo() {
		return attorneyFaxNo;
	}

	public void setAttorneyFaxNo(String attorneyFaxNo) {
		this.attorneyFaxNo = attorneyFaxNo;
	}

	public String getPracticeCode() {
		return practiceCode;
	}

	public void setPracticeCode(String practiceCode) {
		this.practiceCode = practiceCode;
	}
	public String getMissingInfo() {
		return missingInfo;
	}

	public void setMissingInfo(String missingInfo) {
		this.missingInfo = missingInfo;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PatientDTO patientDTO = (PatientDTO) o;
        if (patientDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), patientDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PatientDTO{" +
            "id=" + getId() +
            ", patientCode='" + getPatientCode() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", initial='" + getInitial() + "'" +
            ", homePhone='" + getHomePhone() + "'" +
            ", sex='" + getSex() + "'" +
            ", address='" + getAddress() + "'" +
            ", city='" + getCity() + "'" +
            ", state='" + getState() + "'" +
            ", zipCode=" + getZipCode() +
            ", email='" + getEmail() + "'" +
            ", mobileNo='" + getMobileNo() + "'" +
            ", enable='" + isEnable() + "'" +
            ", patientStatus='" + getPatientStatus() + "'" +
            ", statusNotes='" + getStatusNotes() + "'" +
            ", dob='" + getDob() + "'" +
            ", caseType='" + getCaseType() + "'" +
            ", workPhone=" + getWorkPhone() +
            ", age=" + getAge() +
            ", insFirstName='" + getInsFirstName() + "'" +
            ", insLastName='" + getInsLastName() + "'" +
            ", insInitial='" + getInsInitial() + "'" +
            ", insAddress='" + getInsAddress() + "'" +
            ", insCity='" + getInsCity() + "'" +
            ", insState='" + getInsState() + "'" +
            ", insZipCode=" + getInsZipCode() +
            ", insPhoneNo=" + getInsPhoneNo() +
            ", primaryInsCo='" + getPrimaryInsCo() + "'" +
            ", primaryInsClaimNo='" + getPrimaryInsClaimNo() + "'" +
            ", primaryInsPolicyNo='" + getPrimaryInsPolicyNo() + "'" +
            ", primaryInsRelation='" + getPrimaryInsRelation() + "'" +
            ", primaryInsNotes='" + getPrimaryInsNotes() + "'" +
            ", doa='" + getDoa() + "'" +
            ", secInsClaimNo='" + getSecInsClaimNo() + "'" +
            ", secInsPolicyNo='" + getSecInsPolicyNo() + "'" +
            ", secInsRelation='" + getSecInsRelation() + "'" +
            ", secInsNotes='" + getSecInsNotes() + "'" +
            ", addnInfo='" + getAddnInfo() + "'" +
            ", attorneyMobileNo='" + getAttorneyMobileNo() + "'" +
            ", attorneyFaxNo='" + getAttorneyFaxNo() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
//            ", attornyId=" + getAttornyId() +
            ", attornyLogin='" + getAttornyLogin() + "'" +
            ", doctorId=" + getDoctorId() +
            ", doctorLogin='" + getDoctorLogin() + "'" +
            ", primaryInsCertifier='" + getPrimaryInsCertifier() + "'" +
            ", primaryInsAdjuster='" + getPrimaryInsAdjuster() + "'" +
            ", primaryInsAdjusterFax='" + getPrimaryInsAdjusterFax()+ "'" +
            ", firstVisit='" + getFirstVisit()+ "'" +
            ", secInsCertifier='" + getSecInsCertifier()+ "'" +
            ", secInsAdjuster='" + getSecInsAdjuster()+ "'" +
            ", secInsAdjusterFax='" + getSecInsAdjusterFax()+ "'" +
            ", attorneyFirstName='" + getAttorneyFirstName()+ "'" +
            ", attorneyFirstName='" + getAttorneyLastName()+ "'" +
            ", attoneyAddress='" + getAttoneyAddress()+ "'" +
            ", attorneyCity='" + getAttorneyCity()+ "'" +
            ", attorneyState='" + getAttorneyState()+ "'" +
            ", attoneyZipCode='" + getAttoneyZipCode()+ "'" +			
            ", practiceCode='" + getPracticeCode()+ "'" +			
            ", missingInfo='" + getMissingInfo()+ "'" +	
            "}";
    }
}
