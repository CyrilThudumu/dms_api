package com.dms.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A DTO for the Documents entity.
 */


public class DocumentsDTO implements Serializable{

	

	private static final long serialVersionUID = 1L;

	
	private Long id;
	public String documentTypes;
	public String fromDos;
	public String toDos;
	public String patientCode;
	public String documentName;
	public String practiceCode;
	public String doctorCode;
	public String attorneyCode;
	public String filePath;
	public String status;
	public String statusDate;
	public String arbAmount;
	public String arbNotes;
	public String paymentAmount;
	public String sentDate;
	public String paymentDate;
	public String natureOfDispute;
	public String processedDate;
	public byte[] fileContent;
	private boolean processedFlag;
	private String searchKey;
	
	
	
	
	public DocumentsDTO(Long id, String documentTypes, String fromDos,
			String toDos, String patientCode, String documentName,
			String practiceCode, String doctorCode, String attorneyCode,
			String filePath, String status, String statusDate,
			String arbAmount, String arbNotes, String paymentAmount,
			String sentDate, String paymentDate, String natureOfDispute,
			String processedDate, byte[] fileContent, boolean processedFlag, String searchKey) {
		super();
		this.id = id;
		this.documentTypes = documentTypes;
		this.fromDos = fromDos;
		this.toDos = toDos;
		this.patientCode = patientCode;
		this.documentName = documentName;
		this.practiceCode = practiceCode;
		this.doctorCode = doctorCode;
		this.attorneyCode = attorneyCode;
		this.filePath = filePath;
		this.status = status;
		this.statusDate = statusDate;
		this.arbAmount = arbAmount;
		this.arbNotes = arbNotes;
		this.paymentAmount = paymentAmount;
		this.sentDate = sentDate;
		this.paymentDate = paymentDate;
		this.natureOfDispute = natureOfDispute;
		this.processedDate = processedDate;
		this.fileContent = fileContent;
		this.processedFlag = processedFlag;
		this.searchKey = searchKey;
	}

	public DocumentsDTO() {
	}

	public boolean isProcessedFlag() {
		return processedFlag;
	}

	public void setProcessedFlag(boolean processedFlag) {
		this.processedFlag = processedFlag;
	}

	public byte[] getFileContent() {
		return fileContent;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getFromDos() {
		return fromDos;
	}

	public void setFromDos(String fromDos) {
		this.fromDos = fromDos;
	}

	public String getToDos() {
		return toDos;
	}

	public void setToDos(String toDos) {
		this.toDos = toDos;
	}

	public String getPracticeCode() {
		return practiceCode;
	}

	public void setPracticeCode(String practiceCode) {
		this.practiceCode = practiceCode;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getAttorneyCode() {
		return attorneyCode;
	}

	public void setAttorneyCode(String attorneyCode) {
		this.attorneyCode = attorneyCode;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getDocumentTypes() {
		return documentTypes;
	}

	public void setDocumentTypes(String documentTypes) {
		this.documentTypes = documentTypes;
	}

	public String getArbAmount() {
		return arbAmount;
	}

	public void setArbAmount(String arbAmount) {
		this.arbAmount = arbAmount;
	}

	public String getArbNotes() {
		return arbNotes;
	}

	public void setArbNotes(String arbNotes) {
		this.arbNotes = arbNotes;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(String statusDate) {
		this.statusDate = statusDate;
	}

	public String getSentDate() {
		return sentDate;
	}

	public void setSentDate(String sentDate) {
		this.sentDate = sentDate;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getNatureOfDispute() {
		return natureOfDispute;
	}

	public void setNatureOfDispute(String natureOfDispute) {
		this.natureOfDispute = natureOfDispute;
	}

	public String getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(String processedDate) {
		this.processedDate = processedDate;
	}

	
	public String getSearchKey() {
		return searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	@Override
	public String toString() {
		return "DocumentsDTO [id=" + id + ", documentTypes=" + documentTypes
				+ ", fromDos=" + fromDos + ", toDos=" + toDos
				+ ", patientCode=" + patientCode + ", documentName="
				+ documentName + ", practiceCode=" + practiceCode
				+ ", doctorCode=" + doctorCode + ", attorneyCode="
				+ attorneyCode + ", filePath=" + filePath + ", status="
				+ status + ", statusDate=" + statusDate + ", arbAmount="
				+ arbAmount + ", arbNotes=" + arbNotes + ", paymentAmount="
				+ paymentAmount + ", sentDate=" + sentDate + ", paymentDate="
				+ paymentDate + ", natureOfDispute=" + natureOfDispute
				+ ", processedDate=" + processedDate + ", fileContent="
				+ Arrays.toString(fileContent) + ", processedFlag="
				+ processedFlag + "]";
	}

/*	@Override
	public String toString() {
		return "DocumentsDTO [id=" + id + ", documentTypes=" + documentTypes
				+ ", fromDos=" + fromDos + ", toDos=" + toDos
				+ ", patientCode=" + patientCode + ", documentName="
				+ documentName + ", practiceCode=" + practiceCode
				+ ", doctorCode=" + doctorCode + ", attorneyCode="
				+ attorneyCode + ", filePath=" + filePath + ", status="
				+ status + ", statusDate=" + statusDate + ", arbAmount="
				+ arbAmount + ", arbNotes=" + arbNotes + ", paymentAmount="
				+ paymentAmount + ", sentDate=" + sentDate + ", paymentDate="
				+ paymentDate + ", natureOfDispute=" + natureOfDispute
				+ ", processedDate=" + processedDate + ", fileContent="
				+ Arrays.toString(fileContent) + "]";
	}*/


	
	
	
	
	
}
