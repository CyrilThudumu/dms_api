package com.dms.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Patientstatus entity.
 */
public class PatientstatusDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 20)
    private String patientStatusCode;

    @NotNull
    @Size(max = 20)
    private String patientStatusName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPatientStatusCode() {
        return patientStatusCode;
    }

    public void setPatientStatusCode(String patientStatusCode) {
        this.patientStatusCode = patientStatusCode;
    }

    public String getPatientStatusName() {
        return patientStatusName;
    }

    public void setPatientStatusName(String patientStatusName) {
        this.patientStatusName = patientStatusName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PatientstatusDTO patientstatusDTO = (PatientstatusDTO) o;
        if (patientstatusDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), patientstatusDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PatientstatusDTO{" +
            "id=" + getId() +
            ", patientStatusCode='" + getPatientStatusCode() + "'" +
            ", patientStatusName='" + getPatientStatusName() + "'" +
            "}";
    }
}
