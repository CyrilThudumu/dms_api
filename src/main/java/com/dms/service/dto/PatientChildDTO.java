package com.dms.service.dto;

import java.time.LocalDate;




public class PatientChildDTO {

	private String patientCode;
	private String firstName;
	private String lastName;
	private String primaryInsClaimNo;
	private String primaryInsPolicyNo;
	private LocalDate doa;
	public String getPatientCode() {
		return patientCode;
	}
	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPrimaryInsClaimNo() {
		return primaryInsClaimNo;
	}
	public void setPrimaryInsClaimNo(String primaryInsClaimNo) {
		this.primaryInsClaimNo = primaryInsClaimNo;
	}
	public String getPrimaryInsPolicyNo() {
		return primaryInsPolicyNo;
	}
	public void setPrimaryInsPolicyNo(String primaryInsPolicyNo) {
		this.primaryInsPolicyNo = primaryInsPolicyNo;
	}
	public LocalDate getDoa() {
		return doa;
	}
	public void setDoa(LocalDate doa) {
		this.doa = doa;
	}

	@Override
	public String toString() {
		return "PatientChildDTO [patientCode=" + patientCode + ", firstName="
				+ firstName + ", lastName=" + lastName + ", primaryInsClaimNo="
				+ primaryInsClaimNo + ", primaryInsPolicyNo="
				+ primaryInsPolicyNo + ", doa=" + doa + "]";
	}
	public PatientChildDTO(String patientCode, String firstName,
			String lastName, String primaryInsClaimNo,
			String primaryInsPolicyNo, LocalDate doa) {
		super();
		this.patientCode = patientCode;
		this.firstName = firstName;
		this.lastName = lastName;
		this.primaryInsClaimNo = primaryInsClaimNo;
		this.primaryInsPolicyNo = primaryInsPolicyNo;
		this.doa = doa;
	}
	
	
	
	
}
