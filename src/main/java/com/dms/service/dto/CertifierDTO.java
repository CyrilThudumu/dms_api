package com.dms.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Certifier entity.
 */
public class CertifierDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 20)
    private String certifierCode;

    @NotNull
    @Size(max = 20)
    private String certifierName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getCertifierCode() {
		return certifierCode;
	}

	public void setCertifierCode(String certifierCode) {
		this.certifierCode = certifierCode;
	}

	public String getCertifierName() {
		return certifierName;
	}

	public void setCertifierName(String certifierName) {
		this.certifierName = certifierName;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CertifierDTO certifierDTO = (CertifierDTO) o;
        if (certifierDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), certifierDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CertifierDTO{" +
            "id=" + getId() +
            ", certifierCode='" + getCertifierCode() + "'" +
            ", certifierName='" + getCertifierName() + "'" +
            "}";
    }
}
