package com.dms.service.dto;

import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the Certifier entity. This class is used in CertifierResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /certifiers?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CertifierCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter certifierCode;

    private StringFilter certifierName;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCertifierCode() {
        return certifierCode;
    }

    public void setCertifierCode(StringFilter certifierCode) {
        this.certifierCode = certifierCode;
    }

    public StringFilter getCertifierName() {
        return certifierName;
    }

    public void setCertifierName(StringFilter certifierName) {
        this.certifierName = certifierName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CertifierCriteria that = (CertifierCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(certifierCode, that.certifierCode) &&
            Objects.equals(certifierName, that.certifierName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        certifierCode,
        certifierName
        );
    }

    @Override
    public String toString() {
        return "CertifierCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (certifierCode != null ? "caseTypeCode=" + certifierCode + ", " : "") +
                (certifierName != null ? "caseTypeName=" + certifierName + ", " : "") +
            "}";
    }

}
