package com.dms.service.dto;

import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the Practice entity. This class is used in PracticeResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /practices?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PracticeCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter practiceCode;

    private StringFilter practiceName;

    private StringFilter address1;

    private StringFilter address2;

    private StringFilter city;

    private StringFilter stateProvince;

    private IntegerFilter zipCode;
    
    private IntegerFilter tinNumber;

    private LongFilter documenttypeId;

    private LongFilter userId;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getPracticeCode() {
        return practiceCode;
    }

    public void setPracticeCode(StringFilter practiceCode) {
        this.practiceCode = practiceCode;
    }

    public StringFilter getPracticeName() {
        return practiceName;
    }

    public void setPracticeName(StringFilter practiceName) {
        this.practiceName = practiceName;
    }

    public StringFilter getAddress1() {
        return address1;
    }

    public void setAddress1(StringFilter address1) {
        this.address1 = address1;
    }

    public StringFilter getAddress2() {
        return address2;
    }

    public void setAddress2(StringFilter address2) {
        this.address2 = address2;
    }

    public StringFilter getCity() {
        return city;
    }

    public void setCity(StringFilter city) {
        this.city = city;
    }

    public StringFilter getStateProvince() {
        return stateProvince;
    }

    public void setStateProvince(StringFilter stateProvince) {
        this.stateProvince = stateProvince;
    }

    public IntegerFilter getZipCode() {
        return zipCode;
    }

    public void setZipCode(IntegerFilter zipCode) {
        this.zipCode = zipCode;
    }
    
    public IntegerFilter getTinNumber() {
		return tinNumber;
	}

	public void setTinNumber(IntegerFilter tinNumber) {
		this.tinNumber = tinNumber;
	}

	public LongFilter getDocumenttypeId() {
        return documenttypeId;
    }

    public void setDocumenttypeId(LongFilter documenttypeId) {
        this.documenttypeId = documenttypeId;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PracticeCriteria that = (PracticeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(practiceCode, that.practiceCode) &&
            Objects.equals(practiceName, that.practiceName) &&
            Objects.equals(address1, that.address1) &&
            Objects.equals(address2, that.address2) &&
            Objects.equals(city, that.city) &&
            Objects.equals(stateProvince, that.stateProvince) &&
            Objects.equals(zipCode, that.zipCode) &&
            Objects.equals(tinNumber, that.tinNumber) &&
            Objects.equals(documenttypeId, that.documenttypeId) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        practiceCode,
        practiceName,
        address1,
        address2,
        city,
        stateProvince,
        zipCode,
        tinNumber,
        documenttypeId,
        userId
        );
    }

    @Override
    public String toString() {
        return "PracticeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (practiceCode != null ? "practiceCode=" + practiceCode + ", " : "") +
                (practiceName != null ? "practiceName=" + practiceName + ", " : "") +
                (address1 != null ? "address1=" + address1 + ", " : "") +
                (address2 != null ? "address2=" + address2 + ", " : "") +
                (city != null ? "city=" + city + ", " : "") +
                (stateProvince != null ? "stateProvince=" + stateProvince + ", " : "") +
                (zipCode != null ? "zipCode=" + zipCode + ", " : "") +
                (tinNumber != null ? "tinNumber=" + tinNumber + ", " : "") +
                (documenttypeId != null ? "documenttypeId=" + documenttypeId + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
