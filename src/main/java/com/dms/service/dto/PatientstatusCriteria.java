package com.dms.service.dto;

import java.io.Serializable;


import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the Patientstatus entity. This class is used in PatientstatusResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /patientstatuses?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PatientstatusCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter patientStatusCode;

    private StringFilter patientStatusName;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getPatientStatusCode() {
        return patientStatusCode;
    }

    public void setPatientStatusCode(StringFilter patientStatusCode) {
        this.patientStatusCode = patientStatusCode;
    }

    public StringFilter getPatientStatusName() {
        return patientStatusName;
    }

    public void setPatientStatusName(StringFilter patientStatusName) {
        this.patientStatusName = patientStatusName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PatientstatusCriteria that = (PatientstatusCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(patientStatusCode, that.patientStatusCode) &&
            Objects.equals(patientStatusName, that.patientStatusName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        patientStatusCode,
        patientStatusName
        );
    }

    @Override
    public String toString() {
        return "PatientstatusCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (patientStatusCode != null ? "patientStatusCode=" + patientStatusCode + ", " : "") +
                (patientStatusName != null ? "patientStatusName=" + patientStatusName + ", " : "") +
            "}";
    }

}
