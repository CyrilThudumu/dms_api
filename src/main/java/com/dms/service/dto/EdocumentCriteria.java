package com.dms.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the Edocument entity. This class is used in EdocumentResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /edocuments?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EdocumentCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter documentCode;

    private StringFilter documentName;

    private LocalDateFilter fromField;

    private LocalDateFilter toField;

    private StringFilter notes;

    private StringFilter url;

    private LongFilter documenttypeId;

    private LongFilter patientId;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDocumentCode() {
        return documentCode;
    }

    public void setDocumentCode(StringFilter documentCode) {
        this.documentCode = documentCode;
    }

    public StringFilter getDocumentName() {
        return documentName;
    }

    public void setDocumentName(StringFilter documentName) {
        this.documentName = documentName;
    }

    public LocalDateFilter getFromField() {
        return fromField;
    }

    public void setFromField(LocalDateFilter fromField) {
        this.fromField = fromField;
    }

    public LocalDateFilter getToField() {
        return toField;
    }

    public void setToField(LocalDateFilter toField) {
        this.toField = toField;
    }

    public StringFilter getNotes() {
        return notes;
    }

    public void setNotes(StringFilter notes) {
        this.notes = notes;
    }

    public StringFilter getUrl() {
        return url;
    }

    public void setUrl(StringFilter url) {
        this.url = url;
    }

    public LongFilter getDocumenttypeId() {
        return documenttypeId;
    }

    public void setDocumenttypeId(LongFilter documenttypeId) {
        this.documenttypeId = documenttypeId;
    }

    public LongFilter getPatientId() {
        return patientId;
    }

    public void setPatientId(LongFilter patientId) {
        this.patientId = patientId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EdocumentCriteria that = (EdocumentCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(documentCode, that.documentCode) &&
            Objects.equals(documentName, that.documentName) &&
            Objects.equals(fromField, that.fromField) &&
            Objects.equals(toField, that.toField) &&
            Objects.equals(notes, that.notes) &&
            Objects.equals(url, that.url) &&
            Objects.equals(documenttypeId, that.documenttypeId) &&
            Objects.equals(patientId, that.patientId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        documentCode,
        documentName,
        fromField,
        toField,
        notes,
        url,
        documenttypeId,
        patientId
        );
    }

    @Override
    public String toString() {
        return "EdocumentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (documentCode != null ? "documentCode=" + documentCode + ", " : "") +
                (documentName != null ? "documentName=" + documentName + ", " : "") +
                (fromField != null ? "fromField=" + fromField + ", " : "") +
                (toField != null ? "toField=" + toField + ", " : "") +
                (notes != null ? "notes=" + notes + ", " : "") +
                (url != null ? "url=" + url + ", " : "") +
                (documenttypeId != null ? "documenttypeId=" + documenttypeId + ", " : "") +
                (patientId != null ? "patientId=" + patientId + ", " : "") +
            "}";
    }

}
