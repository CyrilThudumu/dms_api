package com.dms.service.util;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.dms.service.dto.PatientDTO;
import com.dms.service.impl.PatientServiceImpl;
import com.dms.web.rest.PracticeResource;

public class PatientUtil {

	private final Logger log = LoggerFactory.getLogger(PatientUtil.class);
	public PatientUtil() {

	}

	public void savePatietDemographic(PatientDTO patientDTO, MultipartFile file) {

		log.debug("Patient Util : {}");
		Path path;
		Path practiceDir = Paths.get(patientDTO.getPracticeCode());
		Path patientDir = Paths.get(patientDTO.getPatientCode());
		path = Paths.get("D:\\" + practiceDir + "\\" + patientDir);
		try {
			Files.createDirectories(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		OutputStream os = null;
		Path filePath = Paths.get(path + "\\" + file.getOriginalFilename());

		try {
			os = Files.newOutputStream(filePath);
			os.write(file.getBytes());
			os.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public File getPatietDemographic(String practiceCode, String patientCode ) {

		File root = new File("D:\\" + practiceCode + "\\" + patientCode);
		File returnFile = null;
		File[] list = root.listFiles();
		if (list == null) 
			
			return root;
		
		for (File f : list) {
			if (f.isDirectory()) {
				
					log.info("f. is Directory"+f.getAbsolutePath());
									
				
			} else {
				log.info(f.getAbsolutePath());
				returnFile= f.getAbsoluteFile();	
			}
		}
		return returnFile;
	}
}