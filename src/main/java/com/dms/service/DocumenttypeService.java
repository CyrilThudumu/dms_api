package com.dms.service;

import com.dms.service.dto.DocumenttypeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Documenttype.
 */
public interface DocumenttypeService {

    /**
     * Save a documenttype.
     *
     * @param documenttypeDTO the entity to save
     * @return the persisted entity
     */
    DocumenttypeDTO save(DocumenttypeDTO documenttypeDTO);

    /**
     * Get all the documenttypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DocumenttypeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" documenttype.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<DocumenttypeDTO> findOne(Long id);

    /**
     * Delete the "id" documenttype.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
