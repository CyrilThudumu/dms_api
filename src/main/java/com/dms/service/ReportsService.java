package com.dms.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.dms.service.dto.PatientDTO;
import com.dms.service.dto.ReportsDTO;

/**
 * Service Interface for managing Patient.
 */
public interface ReportsService {

	
	/**
	 * Get all the patients.
	 *
	 * @param pageable the pagination information
	 * @return the list of entities
	 */
	Page<ReportsDTO> findByCriteria(ReportsDTO reportsDTO, Pageable pageable);

	
	
}
