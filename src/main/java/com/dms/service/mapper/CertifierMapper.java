package com.dms.service.mapper;

import org.mapstruct.Mapper;

import com.dms.domain.Certifier;
import com.dms.service.dto.CertifierDTO;

/**
 * Mapper for the entity Certifier and its DTO CertifierDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CertifierMapper extends EntityMapper<CertifierDTO, Certifier> {

    default Certifier fromId(Long id) {
        if (id == null) {
            return null;
        }
        Certifier certifier = new Certifier();
        certifier.setId(id);
        return certifier;
    }
}
