package com.dms.service.mapper;

import org.mapstruct.Mapper;

import com.dms.domain.Insurance;
import com.dms.service.dto.InsuranceDTO;
import com.dms.service.mapper.EntityMapper;

/**
 * Mapper for the entity Insurance and its DTO InsuranceDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface InsuranceMapper extends EntityMapper<InsuranceDTO, Insurance> {

    default Insurance fromId(Long id) {
        if (id == null) {
            return null;
        }
        Insurance insurance = new Insurance();
        insurance.setId(id);
        return insurance;
    }
}
