package com.dms.service.mapper;

import com.dms.domain.*;
import com.dms.service.dto.AttorneyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Attorney and its DTO AttorneyDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AttorneyMapper extends EntityMapper<AttorneyDTO, Attorney> {



    default Attorney fromId(Long id) {
        if (id == null) {
            return null;
        }
        Attorney attorney = new Attorney();
        attorney.setId(id);
        return attorney;
    }
}
