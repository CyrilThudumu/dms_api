package com.dms.service.mapper;

import com.dms.domain.*;


import com.dms.service.dto.DocumententryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Documententry and its DTO DocumententryDTO.
 */
@Mapper(componentModel = "spring", uses = {DocumenttypeMapper.class, PatientMapper.class})
public interface DocumententryMapper extends EntityMapper<DocumententryDTO, Documententry> {

    @Mapping(source = "documenttypeForfromTo1.id", target = "documenttypeForfromTo1Id")
    @Mapping(source = "documenttypeForfromTo1.documentTypeName", target = "documenttypeForfromTo1DocumentTypeName")
    @Mapping(source = "documenttypeForfromTo2.id", target = "documenttypeForfromTo2Id")
    @Mapping(source = "documenttypeForfromTo2.documentTypeName", target = "documenttypeForfromTo2DocumentTypeName")
    @Mapping(source = "documenttypeForfromTo3.id", target = "documenttypeForfromTo3Id")
    @Mapping(source = "documenttypeForfromTo3.documentTypeName", target = "documenttypeForfromTo3DocumentTypeName")
    @Mapping(source = "documenttypeForfromTo4.id", target = "documenttypeForfromTo4Id")
    @Mapping(source = "documenttypeForfromTo4.documentTypeName", target = "documenttypeForfromTo4DocumentTypeName")
    @Mapping(source = "documenttypeForfromTo5.id", target = "documenttypeForfromTo5Id")
    @Mapping(source = "documenttypeForfromTo5.documentTypeName", target = "documenttypeForfromTo5DocumentTypeName")
    @Mapping(source = "documenttypeForfromTo6.id", target = "documenttypeForfromTo6Id")
    @Mapping(source = "documenttypeForfromTo6.documentTypeName", target = "documenttypeForfromTo6DocumentTypeName")
    @Mapping(source = "patient.id", target = "patientId")
    @Mapping(source = "patient.patientCode", target = "patientPatientCode")
    DocumententryDTO toDto(Documententry documententry);

    @Mapping(source = "documenttypeForfromTo1Id", target = "documenttypeForfromTo1")
    @Mapping(source = "documenttypeForfromTo2Id", target = "documenttypeForfromTo2")
    @Mapping(source = "documenttypeForfromTo3Id", target = "documenttypeForfromTo3")
    @Mapping(source = "documenttypeForfromTo4Id", target = "documenttypeForfromTo4")
    @Mapping(source = "documenttypeForfromTo5Id", target = "documenttypeForfromTo5")
    @Mapping(source = "documenttypeForfromTo6Id", target = "documenttypeForfromTo6")
    @Mapping(source = "patientId", target = "patient")
    Documententry toEntity(DocumententryDTO documententryDTO);

    default Documententry fromId(Long id) {
        if (id == null) {
            return null;
        }
        Documententry documententry = new Documententry();
        documententry.setId(id);
        return documententry;
    }
}
