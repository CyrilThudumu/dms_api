package com.dms.service.mapper;

import com.dms.domain.*;
import com.dms.service.dto.EdocumentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Edocument and its DTO EdocumentDTO.
 */
@Mapper(componentModel = "spring", uses = {DocumenttypeMapper.class})
public interface EdocumentMapper extends EntityMapper<EdocumentDTO, Edocument> {

    @Mapping(source = "documenttype.id", target = "documenttypeId")
    @Mapping(source = "documenttype.documentTypeName", target = "documenttypeDocumentTypeName")
    EdocumentDTO toDto(Edocument edocument);
	
	
    @Mapping(source = "documenttypeId", target = "documenttype")
    @Mapping(target = "patients", ignore = true)
    Edocument toEntity(EdocumentDTO edocumentDTO);

    default Edocument fromId(Long id) {
        if (id == null) {
            return null;
        }
        Edocument edocument = new Edocument();
        edocument.setId(id);
        return edocument;
    }
}
