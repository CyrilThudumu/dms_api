package com.dms.service.mapper;

import com.dms.domain.*;
import com.dms.service.dto.CasetypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Casetype and its DTO CasetypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CasetypeMapper extends EntityMapper<CasetypeDTO, Casetype> {



    default Casetype fromId(Long id) {
        if (id == null) {
            return null;
        }
        Casetype casetype = new Casetype();
        casetype.setId(id);
        return casetype;
    }
}
