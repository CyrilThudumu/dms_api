package com.dms.service.mapper;

import com.dms.domain.*;

import com.dms.service.dto.PracticeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Practice and its DTO PracticeDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface PracticeMapper extends EntityMapper<PracticeDTO, Practice> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    PracticeDTO toDto(Practice practice);

    @Mapping(target = "documenttypes", ignore = true)
    @Mapping(source = "userId", target = "user")
    Practice toEntity(PracticeDTO practiceDTO);

    default Practice fromId(Long id) {
        if (id == null) {
            return null;
        }
        Practice practice = new Practice();
        practice.setId(id);
        return practice;
    }
}
