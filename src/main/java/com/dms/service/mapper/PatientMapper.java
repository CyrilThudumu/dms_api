package com.dms.service.mapper;

import com.dms.domain.*;
import com.dms.service.dto.PatientDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Patient and its DTO PatientDTO.
 */
@Mapper(componentModel = "spring", uses = {PracticeMapper.class, EdocumentMapper.class})
public interface PatientMapper extends EntityMapper<PatientDTO, Patient> {

//    @Mapping(source = "attorny.id", target = "attornyId")
//    @Mapping(source = "attorny.login", target = "attornyLogin")
//    @Mapping(source = "doctor.id", target = "doctorId")
//    @Mapping(source = "doctor.login", target = "doctorLogin")
	//@Mapping(source = "practice.practiceCode", target = "practicePracticeCode")

    PatientDTO toDto(Patient patient);

//    @Mapping(source = "attornyId", target = "attorny")
//    @Mapping(source = "doctorId", target = "doctor")
	//@Mapping(source = "practicePracticeCode", target = "practice")

    Patient toEntity(PatientDTO patientDTO);

    default Patient fromId(Long id) {
        if (id == null) {
            return null;
        }
        Patient patient = new Patient();
        patient.setId(id);
        return patient;
    }
}
