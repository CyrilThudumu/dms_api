package com.dms.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import com.dms.service.dto.DocumentsDTO;
import com.dms.service.dto.PatientDTO;

/**
 * Service Interface for managing Patient.
 */
public interface DocumentsService {

    /**
     * Save a patient.
     *
     * @param patientDTO the entity to save
     * @return the persisted entity
     */
	DocumentsDTO save(DocumentsDTO documentsDTO);
	
	
	/**
	 * Get the "patientId" patient.
	 *
	 * @param patientId the patientId of the entity
	 * @return the entity
	 */
	// Page<DocumentsDTO> findByPatientCode(DocumentsDTO documentsDTO, Pageable page);
	 
	 Page<DocumentsDTO> findByDoctorCode(String doctorCode, String patientCode, Pageable page);
	 Page<DocumentsDTO> findByPatientCode(String patientCode, String practiceCode,Pageable page);
	 Page<DocumentsDTO> findByAttorneyCode(String attorneyCode, String patientCode, Pageable page);
	 Page<DocumentsDTO> getAttorneyProcessedDOS(String attorneyCode, String patientCode, Pageable page);

	 
	 void updatePatientDOS(DocumentsDTO documentsDTO);

	 void updatePatientDOSToProcessed(DocumentsDTO documentsDTO);

	//void updatePatientDOS(DocumentsDTO documentsDTO);	

	 //Optional<DocumentsDTO> downloadPatientDocs(DocumentsDTO documentsDTO);
	 Page<DocumentsDTO> downloadPatientDocs(String patientCode, String practiceCode, String fromDOS, String toDOS, Pageable page);
	 
	 Page<DocumentsDTO> findDocsHistoryByAttorneyCode(String attorneyCode, String patientCode, Pageable page);
	 
	 /**
		 * Get the "patientDTO" patient.
		 *
		 * @param id the attorneyCode of the entity
		 * @return the entity
		 */
	//Page<DocumentsDTO> searchAttorneyPatientHistory(DocumentsDTO documentsDTO, Pageable page);
	 Page<DocumentsDTO> searchAttorneyPatientHistory(String attorneyCode,String fromDOS, String toDOS,Pageable page);
	 
	 Page<DocumentsDTO> getDocumentsForHistorySearch(String attorneyCode,String processedFrom, String processedTo, Pageable page);
	 
//	 Page<DocumentsDTO> searchAttorneyPatientHistory(String attorneyCode,String fromDOS, String toDOS,Pageable page);
		

		/**
		 * Delete the "id" patient.
		 *
		 * @param id the id of the entity
		 */
		void delete(Long id);
		
}
