package com.dms.service;

import io.github.jhipster.service.QueryService;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.domain.Insurance; // for static metamodels
import com.dms.domain.Insurance_;
import com.dms.repository.InsuranceRepository;
import com.dms.service.dto.CasetypeDTO;
import com.dms.service.dto.InsuranceCriteria;
import com.dms.service.dto.InsuranceDTO;
import com.dms.service.mapper.InsuranceMapper;

/**
 * Service for executing complex queries for Insurance entities in the database.
 * The main input is a {@link InsuranceCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link InsuranceDTO} or a {@link Page} of {@link InsuranceDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class InsuranceQueryService extends QueryService<Insurance> {

    private final Logger log = LoggerFactory.getLogger(InsuranceQueryService.class);

    private final InsuranceRepository insuranceRepository;
    
    private final InsuranceMapper insuranceMapper;

    public InsuranceQueryService(InsuranceRepository insuranceRepository, InsuranceMapper insuranceMapper) {
        this.insuranceRepository = insuranceRepository;
        this.insuranceMapper = insuranceMapper;
    }

    /**
     * Return a {@link List} of {@link InsuranceDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<InsuranceDTO> findByCriteria(InsuranceCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Insurance> specification = createSpecification(criteria);
        return insuranceMapper.toDto(insuranceRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CasetypeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<InsuranceDTO> findByCriteria(InsuranceCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Insurance> specification = createSpecification(criteria);
        return insuranceRepository.findAll(specification, page)
            .map(insuranceMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(InsuranceCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Insurance> specification = createSpecification(criteria);
        return insuranceRepository.count(specification);
    }

    /**
     * Function to convert InsuranceCriteria to a {@link Specification}
     */
    private Specification<Insurance> createSpecification(InsuranceCriteria criteria) {
        Specification<Insurance> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Insurance_.id));
            }
            if (criteria.getInsuranceCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsuranceCode(), Insurance_.insuranceCode));
            }
            if (criteria.getInsuranceName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInsuranceName(), Insurance_.insuranceName));
            }
        }
        return specification;
    }
}
