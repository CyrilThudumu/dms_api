package com.dms.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.dms.domain.Edocument;
import com.dms.domain.*; // for static metamodels
import com.dms.repository.EdocumentRepository;
import com.dms.service.dto.EdocumentCriteria;
import com.dms.service.dto.EdocumentDTO;
import com.dms.service.mapper.EdocumentMapper;

/**
 * Service for executing complex queries for Edocument entities in the database.
 * The main input is a {@link EdocumentCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EdocumentDTO} or a {@link Page} of {@link EdocumentDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EdocumentQueryService extends QueryService<Edocument> {

    private final Logger log = LoggerFactory.getLogger(EdocumentQueryService.class);

    private final EdocumentRepository edocumentRepository;

    private final EdocumentMapper edocumentMapper;

    public EdocumentQueryService(EdocumentRepository edocumentRepository, EdocumentMapper edocumentMapper) {
        this.edocumentRepository = edocumentRepository;
        this.edocumentMapper = edocumentMapper;
    }

    /**
     * Return a {@link List} of {@link EdocumentDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EdocumentDTO> findByCriteria(EdocumentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Edocument> specification = createSpecification(criteria);
        return edocumentMapper.toDto(edocumentRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link EdocumentDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EdocumentDTO> findByCriteria(EdocumentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Edocument> specification = createSpecification(criteria);
        return edocumentRepository.findAll(specification, page)
            .map(edocumentMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EdocumentCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Edocument> specification = createSpecification(criteria);
        return edocumentRepository.count(specification);
    }

    /**
     * Function to convert EdocumentCriteria to a {@link Specification}
     */
    private Specification<Edocument> createSpecification(EdocumentCriteria criteria) {
        Specification<Edocument> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Edocument_.id));
            }
            if (criteria.getDocumentCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDocumentCode(), Edocument_.documentCode));
            }
            if (criteria.getDocumentName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDocumentName(), Edocument_.documentName));
            }
            if (criteria.getFromField() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFromField(), Edocument_.fromField));
            }
            if (criteria.getToField() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getToField(), Edocument_.toField));
            }
            if (criteria.getNotes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNotes(), Edocument_.notes));
            }
            if (criteria.getUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUrl(), Edocument_.url));
            }
            if (criteria.getDocumenttypeId() != null) {
                specification = specification.and(buildSpecification(criteria.getDocumenttypeId(),
                    root -> root.join(Edocument_.documenttype, JoinType.LEFT).get(Documenttype_.id)));
            }
            if (criteria.getPatientId() != null) {
                specification = specification.and(buildSpecification(criteria.getPatientId(),
                    root -> root.join(Edocument_.patients, JoinType.LEFT).get(Patient_.id)));
            }
        }
        return specification;
    }
}
