package com.dms.service;

import com.dms.service.dto.DocumententryDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Documententry.
 */
public interface DocumententryService {

    /**
     * Save a documententry.
     *
     * @param documententryDTO the entity to save
     * @return the persisted entity
     */
    DocumententryDTO save(DocumententryDTO documententryDTO);

    /**
     * Get all the documententries.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DocumententryDTO> findAll(Pageable pageable);


    /**
     * Get the "id" documententry.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<DocumententryDTO> findOne(Long id);

    /**
     * Delete the "id" documententry.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
