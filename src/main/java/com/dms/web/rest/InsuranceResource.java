package com.dms.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.dms.service.InsuranceQueryService;
import com.dms.service.InsuranceService;
import com.dms.service.dto.InsuranceCriteria;
import com.dms.service.dto.InsuranceDTO;
import com.dms.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Insurance.
 */
@RestController
@RequestMapping("/api")
public class InsuranceResource {

    private final Logger log = LoggerFactory.getLogger(InsuranceResource.class);

    private static final String ENTITY_NAME = "insurance";

   /* private final InsuranceService insuranceService;
    //private final CasetypeService casetypeService;
*/
    private final InsuranceQueryService insuranceQueryService;

    public InsuranceResource(InsuranceQueryService insuranceQueryService) {
        this.insuranceQueryService = insuranceQueryService;
    }
/*
    *//**
     * POST  /casetypes : Create a new casetype.
     *
     * @param casetypeDTO the casetypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new casetypeDTO, or with status 400 (Bad Request) if the casetype has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     *//*
    @PostMapping("/casetypes")
    @Timed
    public ResponseEntity<CasetypeDTO> createCasetype(@Valid @RequestBody CasetypeDTO casetypeDTO) throws URISyntaxException {
        log.debug("REST request to save Casetype : {}", casetypeDTO);
        if (casetypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new casetype cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CasetypeDTO result = casetypeService.save(casetypeDTO);
        return ResponseEntity.created(new URI("/api/casetypes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    *//**
     * PUT  /casetypes : Updates an existing casetype.
     *
     * @param casetypeDTO the casetypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated casetypeDTO,
     * or with status 400 (Bad Request) if the casetypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the casetypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     *//*
    @PutMapping("/casetypes")
    @Timed
    public ResponseEntity<CasetypeDTO> updateCasetype(@Valid @RequestBody CasetypeDTO casetypeDTO) throws URISyntaxException {
        log.debug("REST request to update Casetype : {}", casetypeDTO);
        if (casetypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CasetypeDTO result = casetypeService.save(casetypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, casetypeDTO.getId().toString()))
            .body(result);
    }*/

    /**
     * GET  /insurances : get all the insurances.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of Insurance in body
     */
    @GetMapping("/insurances")
    @Timed
    public ResponseEntity<List<InsuranceDTO>> getAllInsurances(InsuranceCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Insurances by criteria: {}", criteria);
        Page<InsuranceDTO> page = insuranceQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/insurances");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * GET  /insurances/count : count all the insurances.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/insurances/count")
    @Timed
    public ResponseEntity<Long> countCasetypes(InsuranceCriteria criteria) {
        log.debug("REST request to count Insurance by criteria: {}", criteria);
        return ResponseEntity.ok().body(insuranceQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /casetypes/:id : get the "id" casetype.
     *
     * @param id the id of the casetypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the casetypeDTO, or with status 404 (Not Found)
     *//*
    @GetMapping("/casetypes/{id}")
    @Timed
    public ResponseEntity<CasetypeDTO> getCasetype(@PathVariable Long id) {
        log.debug("REST request to get Casetype : {}", id);
        Optional<CasetypeDTO> casetypeDTO = casetypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(casetypeDTO);
    }*/

    /**
     * DELETE  /casetypes/:id : delete the "id" casetype.
     *
     * @param id the id of the casetypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
  /*  @DeleteMapping("/casetypes/{id}")
    @Timed
    public ResponseEntity<Void> deleteCasetype(@PathVariable Long id) {
        log.debug("REST request to delete Casetype : {}", id);
        casetypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }*/
}
