package com.dms.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.dms.service.CertifierQueryService;
import com.dms.service.CertifierService;
import com.dms.service.dto.CertifierCriteria;
import com.dms.service.dto.CertifierDTO;
import com.dms.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Certifier.
 */
@RestController
@RequestMapping("/api")
public class CertifierResource {

    private final Logger log = LoggerFactory.getLogger(CertifierResource.class);

    private static final String ENTITY_NAME = "certifier";


    private final CertifierQueryService certifierQueryService;

    public CertifierResource(CertifierQueryService certifierQueryService) {
        this.certifierQueryService = certifierQueryService;
    }

   
    /**
     * GET  /certifiers : get all the certifiers.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of certifier in body
     */
    @GetMapping("/certifiers")
    @Timed
    public ResponseEntity<List<CertifierDTO>> getAllCertifiers(CertifierCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Certifier by criteria: {}", criteria);
        Page<CertifierDTO> page = certifierQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/certifiers");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * GET  /casetypes/count : count all the casetypes.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
  /*  @GetMapping("/casetypes/count")
    @Timed
    public ResponseEntity<Long> countCasetypes(CasetypeCriteria criteria) {
        log.debug("REST request to count Casetypes by criteria: {}", criteria);
        return ResponseEntity.ok().body(casetypeQueryService.countByCriteria(criteria));
    }*/

    /**
     * GET  /casetypes/:id : get the "id" casetype.
     *
     * @param id the id of the casetypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the casetypeDTO, or with status 404 (Not Found)
     */
    /*@GetMapping("/casetypes/{id}")
    @Timed
    public ResponseEntity<CasetypeDTO> getCasetype(@PathVariable Long id) {
        log.debug("REST request to get Casetype : {}", id);
        Optional<CasetypeDTO> casetypeDTO = casetypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(casetypeDTO);
    }*/

    /**
     * DELETE  /casetypes/:id : delete the "id" casetype.
     *
     * @param id the id of the casetypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
   /* @DeleteMapping("/casetypes/{id}")
    @Timed
    public ResponseEntity<Void> deleteCasetype(@PathVariable Long id) {
        log.debug("REST request to delete Casetype : {}", id);
        casetypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }*/
}
