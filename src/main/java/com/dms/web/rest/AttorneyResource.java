package com.dms.web.rest;

import com.codahale.metrics.annotation.Timed;

import com.dms.service.AttorneyService;
import com.dms.web.rest.errors.BadRequestAlertException;
import com.dms.web.rest.util.HeaderUtil;
import com.dms.web.rest.util.PaginationUtil;
import com.dms.service.dto.AttorneyDTO;
import com.dms.service.dto.AttorneyCriteria;
import com.dms.service.AttorneyQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Attorney.
 */
@RestController
@RequestMapping("/api")
public class AttorneyResource {

    private final Logger log = LoggerFactory.getLogger(AttorneyResource.class);

    private static final String ENTITY_NAME = "attorney";

    private final AttorneyService attorneyService;

    private final AttorneyQueryService attorneyQueryService;

    public AttorneyResource(AttorneyService attorneyService, AttorneyQueryService attorneyQueryService) {
        this.attorneyService = attorneyService;
        this.attorneyQueryService = attorneyQueryService;
    }

    /**
     * POST  /attorneys : Create a new attorney.
     *
     * @param attorneyDTO the attorneyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new attorneyDTO, or with status 400 (Bad Request) if the attorney has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/attorneys")
    @Timed
    public ResponseEntity<AttorneyDTO> createAttorney(@Valid @RequestBody AttorneyDTO attorneyDTO) throws URISyntaxException {
        log.debug("REST request to save Attorney : {}", attorneyDTO);
        if (attorneyDTO.getId() != null) {
            throw new BadRequestAlertException("A new attorney cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AttorneyDTO result = attorneyService.save(attorneyDTO);
        return ResponseEntity.created(new URI("/api/attorneys/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /attorneys : Updates an existing attorney.
     *
     * @param attorneyDTO the attorneyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated attorneyDTO,
     * or with status 400 (Bad Request) if the attorneyDTO is not valid,
     * or with status 500 (Internal Server Error) if the attorneyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/attorneys")
    @Timed
    public ResponseEntity<AttorneyDTO> updateAttorney(@Valid @RequestBody AttorneyDTO attorneyDTO) throws URISyntaxException {
        log.debug("REST request to update Attorney : {}", attorneyDTO);
        if (attorneyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AttorneyDTO result = attorneyService.save(attorneyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, attorneyDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /attorneys : get all the attorneys.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of attorneys in body
     */
    @GetMapping("/attorneys")
    @Timed
    public ResponseEntity<List<AttorneyDTO>> getAllAttorneys(AttorneyCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Attorneys by criteria: {}", criteria);
        Page<AttorneyDTO> page = attorneyQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/attorneys");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * GET  /attorneys/count : count all the attorneys.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/attorneys/count")
    @Timed
    public ResponseEntity<Long> countAttorneys(AttorneyCriteria criteria) {
        log.debug("REST request to count Attorneys by criteria: {}", criteria);
        return ResponseEntity.ok().body(attorneyQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /attorneys/:id : get the "id" attorney.
     *
     * @param id the id of the attorneyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the attorneyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/attorneys/{id}")
    @Timed
    public ResponseEntity<AttorneyDTO> getAttorney(@PathVariable Long id) {
        log.debug("REST request to get Attorney : {}", id);
        Optional<AttorneyDTO> attorneyDTO = attorneyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(attorneyDTO);
    }

    /**
     * DELETE  /attorneys/:id : delete the "id" attorney.
     *
     * @param id the id of the attorneyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/attorneys/{id}")
    @Timed
    public ResponseEntity<Void> deleteAttorney(@PathVariable Long id) {
        log.debug("REST request to delete Attorney : {}", id);
        attorneyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
