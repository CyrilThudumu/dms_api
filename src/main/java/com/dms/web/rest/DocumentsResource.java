
package com.dms.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.codahale.metrics.annotation.Timed;
import com.dms.security.AuthoritiesConstants;
import com.dms.service.DoctorService;
import com.dms.service.DocumentsService;
import com.dms.service.PatientService;
import com.dms.service.dto.AttorneyHistoryDocDTO;
import com.dms.service.dto.DocumentsChildDTO;
import com.dms.service.dto.DocumentsDTO;
import com.dms.service.dto.PatientDTO;
import com.dms.service.util.DocumentsUtil;
import com.dms.web.rest.errors.BadRequestAlertException;
import com.dms.web.rest.util.HeaderUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;


/**
 * REST controller for managing documents.
 */
@RestController
@RequestMapping("/api")
public class DocumentsResource {

    private final Logger log = LoggerFactory.getLogger(DocumentsResource.class);

    private static final String ENTITY_NAME = "documents";

    @Autowired
    private DocumentsService documentsService;
    
    @Autowired
    private DoctorService doctorService;

    @Autowired
    private HttpServletResponse response;

    @Autowired
    private PatientService patientService;
    /*@Autowired
    private DocumentsUtil documentsUtil;*/
	/**
     * POST  /documents : Add documents.
     *
     * @param documentsDTO the documentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new documentsDTO, or with status 400 (Bad Request) if the documents isn't existss
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/documents")
    @Timed
    public ResponseEntity<ArrayList<String>> createDocuments(@RequestParam("File") final MultipartFile file,	@RequestParam("FormValue") String JSONArray) throws URISyntaxException {
        log.debug("REST request to save DocumentsDTO : {}", JSONArray);
        ArrayList<String> result =  null;
        try {
        	 ObjectMapper mapper = new ObjectMapper();
        	 ObjectMapper objectMapper=new ObjectMapper();
        	 objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);    
        	 List<DocumentsChildDTO> myObjects = mapper.readValue(JSONArray , new TypeReference<List<DocumentsChildDTO>>(){});
        	 log.debug("My Objects are:  "+myObjects);
        	 log.debug("My Objects list size is:  "+myObjects.size());
        	 int i=0;
        	 for (DocumentsChildDTO listItems : myObjects) {
        		 Gson gson = new Gson();
        		 
            	 //String jsonObj = gson.toJson(myObjects.get(0));
            	 String jsonObj = gson.toJson(listItems);
            	 System.out.println("Json Objects: " + jsonObj);
            	 DocumentsChildDTO documentsChildDTO = mapper.readValue(jsonObj, DocumentsChildDTO.class);
            	 if (documentsChildDTO.getId() != null) {
                     throw new BadRequestAlertException("A new document cannot already have an ID", ENTITY_NAME, "idexists");
                 }
            	// Optional<DoctorDTO> doctorDTO = doctorService.findOne(documentsChildDTO.getId());
            	// documentsChildDTO.setDoctorCode(doctorDTO.get().getDoctorCode());
            	 log.debug("Doctor Code is: "+documentsChildDTO.getDoctorCode());
            	 log.debug("DocumentTypes... "+documentsChildDTO.getDocumentTypes());
            	 log.debug("orig file name:  "+file.getOriginalFilename());
            	 log.debug("get bytes:  "+file.getBytes());
            	 log.debug("pages nums: "+documentsChildDTO.getPageNumber());
            	 log.debug("document types: "+documentsChildDTO.getDocumentTypes());
            	 log.debug("from DOS:  "+documentsChildDTO.getDosFrom());
            	 log.debug("toDOS:  "+documentsChildDTO.getDosTo());
            	 log.debug("Complete documents object:  "+documentsChildDTO);
            	 //call split pdf method to split all the files which are coming from UI...
            	 DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
            	 result= documentsUtil.splitToMultiplePDF(file.getOriginalFilename(), file.getBytes(), documentsChildDTO.getPageNumber(), 
            			 						  documentsChildDTO.getDocumentTypes(), documentsChildDTO.getDosFrom(), documentsChildDTO.getDosTo(),
            			 						  documentsChildDTO);
            	//return ResponseEntity.created(new URI("/api/documents/" + result)).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
            	 //result = new ResponseEntity.("File splitting process completed", HttpStatus.OK);  
            	 i++;
            	 log.debug("Iterations completed for this item:  "+listItems);
            	 log.info("total list of iterations:"+i);
			}
        	
        } catch (Exception e) {
            e.printStackTrace();
        }
      //  return result;
        return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString())).body(result);
    }

    /**
	 * GET /documents : get patient documents For MDM User.
	 *
	 * @param pageable the pagination information
	 * @param patientCode the patientCode which the requested entities should match
	 * @return the ResponseEntity with status 200 (OK) and the list of patients in
	 *         body
	 */
	
    @GetMapping(value = "/documentsByPat/{patientCode}/{practiceCode}")
    public Page<DocumentsDTO> findByPatientCode(@PathVariable String patientCode,@PathVariable String practiceCode, Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", patientCode, practiceCode);

       // Page <DocumentsDTO> documentsDTO = documentsService.findByPatientCode(patientCode,page);
       // log.info("Found {} Docoument entries. Returned page {} contains {} Docoument entries"+documentsDTO.getContent().size());
        DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
        Page <DocumentsDTO> documentsDTO = documentsUtil.getDOSFileContent(documentsService.findByPatientCode(patientCode,practiceCode,page));
        return documentsDTO;
    }
    /**
	 * GET /documents : get patient documents For Doctors.
	 *
	 * @param pageable the pagination information
	 * @param doctorCode the doctorCode which the requested entities should match
	 * @return the ResponseEntity with status 200 (OK) and the list of patients in
	 *         body
	 */
    
    @GetMapping(value = "/documentsByDoc/{doctorCode}/{patientCode}")
    public Page<DocumentsDTO> findByDoctorCode(@PathVariable String doctorCode,@PathVariable String patientCode,Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", doctorCode,patientCode);
        DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
        Page <DocumentsDTO> documentsDTO = documentsUtil.getDOSFileContent(documentsService.findByDoctorCode(doctorCode,patientCode,page));
        log.info("Found {} Docoument entries. Returned page {} contains {} Docoument entries");

        return documentsDTO;
    }
    
    /**
   	 * GET /documents : get patient documents For Attorneys.
   	 *
   	 * @param pageable the pagination information
   	 * @param attorneyCode the attorneyCode which the requested entities should match
   	 * @return the ResponseEntity with status 200 (OK) and the list of patients in
   	 *         body
   	 */
    @GetMapping(value = "/documentsByAttorney/{attorneyCode}/{patientCode}")
    public Page<DocumentsDTO> documentsForAttorney(@PathVariable String attorneyCode,@PathVariable String patientCode,Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode);
        if (attorneyCode == null) {
			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
		}
        if (patientCode == null) {
			throw new BadRequestAlertException("Invalid Patient Code", ENTITY_NAME, "patientCode is null");
		}
        DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
        Page <DocumentsDTO> documentsDTO = documentsUtil.getDOSFileContent(documentsService.findByAttorneyCode(attorneyCode,patientCode,page));

        return documentsDTO;
    }
    
    
    /**
   	 * GET /documents : get patient documents For Attorneys.
   	 *
   	 * @param pageable the pagination information
   	 * @param attorneyCode the attorneyCode which the requested entities should match
   	 * @return the ResponseEntity with status 200 (OK) and the list of patients in
   	 *         body
   	 */
    @GetMapping(value = "/documentsByAttorneyProcessed/{attorneyCode}/{patientCode}")
    public Page<DocumentsDTO> documentsForAttorneyProcessed(@PathVariable String attorneyCode,@PathVariable String patientCode,Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode);
        if (attorneyCode == null) {
			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
		}
        if (patientCode == null) {
			throw new BadRequestAlertException("Invalid Patient Code", ENTITY_NAME, "patientCode is null");
		}
        DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
        Page <DocumentsDTO> documentsDTO = documentsUtil.getDOSFileContent(documentsService.getAttorneyProcessedDOS(attorneyCode,patientCode,page));

        return documentsDTO;
    }
    
    
   /* @GetMapping(value = "/documentsByAttorney/{attorneyCode}/{practiceCode}")
    public Page<DocumentsDTO> findByAttorneyCode(@PathVariable String attorneyCode,@PathVariable String practiceCode,Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode,practiceCode);
        Page <DocumentsDTO> documentsDTO = documentsService.findByAttorneyCode(attorneyCode,practiceCode,page);
        log.info("Found {} Docoument entries. Returned page {} contains {} Docoument entries");

        return documentsDTO;
    }*/
   
   
    
    @PutMapping(value = "/documents")
	@PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + AuthoritiesConstants.USER_ADMIN +"\")")
    public void updatePatientDOS(@RequestBody DocumentsDTO documentsDTO,Pageable page) {
        log.info("Finding Docoument entries by search term: {} and page request: {}", documentsDTO);

        documentsService.updatePatientDOS(documentsDTO);
        log.info("Found {} Docoument entries. Returned page {} contains {} Docoument entries");

    }
    
    @PutMapping(value = "/documentsAttorneyProcessed")
    public void updatePatientDOSToProcessed(@RequestBody DocumentsDTO documentsDTO) {
        log.info("Updating Patient DOS to processed {} and page request: {}", documentsDTO);

        documentsService.updatePatientDOSToProcessed(documentsDTO);
        log.info("Found {} Docoument entries. Returned page {} contains {} Docoument entries");

    }
    
    /**
   	 * GET /documentsload : get patient documents For MDM User.
   	 *
   	 * @param patientCode the patientCode which the requested entities should match
   	 * @return the ResponseEntity with status 200 (OK) and the list of patients in
   	 *         body
   	 */
   	
    	@GetMapping(value = "/documentsload/{patientCode}/{practiceCode}/{fromDOS}/{toDOS}")
    	public ResponseEntity<byte[]> downloadPatientDocs(@PathVariable String patientCode, @PathVariable String practiceCode,@PathVariable String fromDOS,@PathVariable String toDOS, Pageable page) {
		log.info("Finding Docoument entries by search term: {} and page request: {}",patientCode);
		Page<DocumentsDTO> documentsdto = (documentsService.downloadPatientDocs(patientCode,practiceCode,fromDOS,toDOS, page));
		DocumentsUtil docsUtil = new DocumentsUtil(documentsService);
		String path = "D:\\"+documentsdto.getContent().get(0).getPracticeCode()+"\\"+documentsdto.
				getContent().get(0).getPatientCode() + "\\"+ documentsdto.getContent().get(0).getFromDos() + "-" + documentsdto.getContent().get(0).getToDos();
		log.info("Path to download is: : " + path);
//		 String path = getServletContext().getRealPath("data");

         File directory = new File(path);
         String[] files = directory.list();
        // log.info("files Size:: "+files.length);
		byte[] zippingDir = null;
		ResponseEntity<byte[]> response = null;
		if (files != null && files.length > 0) {
			try {
					log.debug("before zippingdir"+directory);
					zippingDir = docsUtil.zipPatientDocs(directory, files);
					log.debug("retuned bytearray: "+zippingDir);
					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.parseMediaType("application/zip"));
					String outputFilename = documentsdto.getContent().get(0).getPatientCode();
					log.debug("fileName: "+outputFilename);
					headers.setContentDispositionFormData(outputFilename,outputFilename);
					headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
					response = new ResponseEntity<>(zippingDir, headers, HttpStatus.OK);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
		log.debug("response returned" +response);
		
		return response;
       }
       
    	
    	
        /**
       	 * GET /attorneyHistoryDocuments : get patient documents For Attorneys.
       	 *
       	 * @param pageable the pagination information
       	 * @param attorneyCode the attorneyCode which the requested entities should match
       	 * @param patientCode the patientCode which the requested entities should match
       	 * @return the ResponseEntity with status 200 (OK) and the list of patients in
       	 *         body
       	 */
        @GetMapping(value = "/attorneyHistoryDocuments/{attorneyCode}/{patientCode}")
        public Page<DocumentsDTO> findDocsHistoryByAttorneyCode(@PathVariable String attorneyCode, @PathVariable String patientCode, Pageable page) {
            log.info("Finding Docoument entries by search term: {} and page request: {}", attorneyCode,patientCode);
            if (attorneyCode == null) {
    			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
    		}
            if (patientCode == null) {
    			throw new BadRequestAlertException("Invalid patient Code Code", ENTITY_NAME, "patientCode is null");
    		}
            
            
            
            DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
            Page <DocumentsDTO> documentsDTO = documentsUtil.getDOSFileContent(documentsService.findDocsHistoryByAttorneyCode(attorneyCode,patientCode,page));

            return documentsDTO;
        }
        
        
        
        /**
    	 * GET /attorneyPatientsHistorySearch/:attorneyCode : GET the "attorneyCode" patient.
    	 *
    	 * @param id the attorneyCode of the patientDTO to delete
    	 * @return the ResponseEntity with status 200 (OK)
    	 */
    	/*@GetMapping("/attorneyPatientsHistorySearch2")
    	@Timed
    	public ResponseEntity<List<DocumentsDTO>> searchAttorneyPatientHistory(@RequestBody DocumentsDTO documentsDTO, Pageable page) {
    		log.debug("REST request to get Attorney Patients: {}",documentsDTO);
    		if (documentsDTO == null) {
    			throw new BadRequestAlertException("Invalid Params", ENTITY_NAME, "Given Params are null");
    		}
            DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
    		Page<DocumentsDTO> documentDTO = documentsUtil.getDOSFileContent(documentsService.searchAttorneyPatientHistory(documentsDTO, page));
    		return new ResponseEntity<>(documentDTO.getContent(), HttpStatus.OK);
    	}*/
    	
        
    	@GetMapping("/attorneyPatientsHistorySearch/{attorneyCode}/{processedFrom}/{processedTo}")
    	@Timed
    	public ResponseEntity<List<DocumentsDTO>> searchAttorneyPatientHistory(@PathVariable String attorneyCode, @PathVariable String processedFrom, @PathVariable String processedTo, Pageable page) {
    		log.debug("REST request to get Attorney Patients: {}", attorneyCode,processedFrom,processedTo);
    		if (attorneyCode == null) {
    			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
    		}
    		if (processedFrom == null) {
    			throw new BadRequestAlertException("From Date should not be null  ", ENTITY_NAME, "processedFrom is null");
    		}
    		if (processedTo == null) {
    			throw new BadRequestAlertException("To Date should not be null ", ENTITY_NAME, "processedTo is null");
    		}
    		
    		
    		//get documents object by passing attorneyCode and processedFrom and processedTo params
    		//get patient object by passing attorneyCode and patientCode params
    		// add these object to a list
    		
    		ArrayList<AttorneyHistoryDocDTO> docTypeObj = new ArrayList<>();  
    		Page<DocumentsDTO> documentDTO = documentsService.getDocumentsForHistorySearch(attorneyCode,processedFrom,processedTo, page);
    		List<DocumentsDTO> documentsDtoList = new ArrayList<DocumentsDTO>();
    		for(DocumentsDTO dto: documentDTO){
    			AttorneyHistoryDocDTO attorneyHistoryDTO = new AttorneyHistoryDocDTO();
    			
    			if(documentsDtoList.isEmpty()){
    				documentsDtoList.add(dto);
    			}else{
    				for(DocumentsDTO iterateExistingDTO: documentsDtoList ){
    					if((iterateExistingDTO.getFromDos().equalsIgnoreCase(dto.getFromDos()))&&(iterateExistingDTO.getToDos().equalsIgnoreCase(dto.getToDos()))){
    						documentsDtoList.add(iterateExistingDTO);
    					}else{
    						AttorneyHistoryDocDTO attorneyHistoryDTO2 = new AttorneyHistoryDocDTO();
    					}
    				}
    			}
    			log.debug("Patient Code: "+dto.getPatientCode());
    			log.debug("From & To DOS are"+dto.getFromDos()+" "+dto.getToDos());
    			Optional<PatientDTO> patientDTO = patientService.getPatientByCode(dto.getPatientCode());
    			documentsDtoList.add(dto);
    			attorneyHistoryDTO.setDocumentsDTO(documentsDtoList);
    			log.debug("patientDTO Obj: "+patientDTO.get());
    			attorneyHistoryDTO.setPatientDTO(patientDTO.get());
    			docTypeObj.add(attorneyHistoryDTO);
    			log.debug("final object is: "+docTypeObj);
    			
    		}
    		log.debug("size : "+documentDTO.getContent().get(1));
    		
    		
    		//patientService.getAttorneyPatientHistory(attorneyCode, documentDTO2.getContent(),page);
    		
    		
    		
    		// DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);

    		
    		
    		
     		//Page<DocumentsDTO> documentDTO = documentsUtil.getDOSFileContent(documentsService.searchAttorneyPatientHistory(attorneyCode,fromDOS,toDOS, page));
     		//Page<DocumentsDTO> documentDTO = documentsService.searchAttorneyPatientHistory(attorneyCode,processedFrom,processedTo, page);
    		return new ResponseEntity<>(documentDTO.getContent(), HttpStatus.OK);
    	}
        
    	 /**
    	 * DELETE /documents/:id : delete the "id" patient.
    	 *
    	 * @param id the id of the patientDTO to delete
    	 * @return the ResponseEntity with status 200 (OK)
    	 */
    	@DeleteMapping("/documents/{id}/{path}")
    	@Timed
    	public ResponseEntity<Void> deleteDocument(@PathVariable Long id, @PathVariable String path) {
    		log.debug("REST request to delete Document : {}", id);
    		documentsService.delete(id);
    		DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);
    		documentsUtil.deleteDocumentType(path);
    		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    	}
    	
    	
    	/*@Bean
    	public HttpFirewall allowUrlEncodedSlashHttpFirewall() {
    	    StrictHttpFirewall firewall = new StrictHttpFirewall();
    	    firewall.setAllowUrlEncodedSlash(true);    
    	    return firewall;
    	}*/
    	
    	@Bean
    	public HttpFirewall defaultHttpFirewall() {
    	    return new DefaultHttpFirewall();
    	}
    	
       /* 
    	@GetMapping("/attorneyPatientsHistorySearch/{attorneyCode}/{fromDOS}/{toDOS}")
    	@Timed
    	public ResponseEntity<List<DocumentsDTO>> searchAttorneyPatientHistory(@PathVariable String attorneyCode, @PathVariable String fromDOS, @PathVariable String toDOS, Pageable page) {
    		log.debug("REST request to get Attorney Patients: {}", attorneyCode,fromDOS,toDOS);
    		if (attorneyCode == null) {
    			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
    		}
    		if (fromDOS == null) {
    			throw new BadRequestAlertException("Invalid fromDOS ", ENTITY_NAME, "fromDOS is null");
    		}
    		if (toDOS == null) {
    			throw new BadRequestAlertException("Invalid toDOS ", ENTITY_NAME, "toDOS is null");
    		}
    		
    		// DocumentsUtil documentsUtil = new DocumentsUtil(documentsService);

     		//Page<DocumentsDTO> documentDTO = documentsUtil.getDOSFileContent(documentsService.searchAttorneyPatientHistory(attorneyCode,fromDOS,toDOS, page));
     		Page<DocumentsDTO> documentDTO = documentsService.searchAttorneyPatientHistory(attorneyCode,fromDOS,toDOS, page);
    		return new ResponseEntity<>(documentDTO.getContent(), HttpStatus.OK);
    	}
    	
    	*/
    	
    	
        
      /* @GetMapping(value = "/documentsload")
       public ResponseEntity<byte[]> downloadPatientDocs(@RequestBody DocumentsDTO documentsDTO) {
		log.info("Finding Docoument entries by search term: {} and page request: {}",documentsDTO);
		Optional<DocumentsDTO> documentsdto = (documentsService.downloadPatientDocs(documentsDTO));
		DocumentsUtil docsUtil = new DocumentsUtil(documentsService);
		String path = "D:\\" + documentsdto.
				get().getPatientCode() + "\\"+ documentsdto.get().getFromDos() + "_" + documentsdto.get().getToDos();
		log.info("Path to download is: : " + path);
		File directory = new File(path);
		String[] files = directory.list();
		byte[] zippingDir;
		ResponseEntity<byte[]> response = null;
		try {
			zippingDir = docsUtil.zipPatientDocs(directory, files);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_PDF);
			String filename = documentsdto.get().getPatientCode();
			headers.setContentDispositionFormData(filename, filename);
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			response = new ResponseEntity<>(zippingDir,
					headers, HttpStatus.OK);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return response;
       }*/
    	
    	
       
}
