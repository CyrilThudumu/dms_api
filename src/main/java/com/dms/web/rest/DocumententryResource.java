package com.dms.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.dms.service.DocumententryService;
import com.dms.web.rest.errors.BadRequestAlertException;
import com.dms.web.rest.util.HeaderUtil;
import com.dms.web.rest.util.PaginationUtil;
import com.dms.service.dto.DocumententryDTO;
import com.dms.service.dto.DocumententryCriteria;
import com.dms.service.DocumententryQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Documententry.
 */
@RestController
@RequestMapping("/api")
public class DocumententryResource {

    private final Logger log = LoggerFactory.getLogger(DocumententryResource.class);

    private static final String ENTITY_NAME = "documententry";

    private final DocumententryService documententryService;

    private final DocumententryQueryService documententryQueryService;

    public DocumententryResource(DocumententryService documententryService, DocumententryQueryService documententryQueryService) {
        this.documententryService = documententryService;
        this.documententryQueryService = documententryQueryService;
    }

    /**
     * POST  /documententries : Create a new documententry.
     *
     * @param documententryDTO the documententryDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new documententryDTO, or with status 400 (Bad Request) if the documententry has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/documententries")
    @Timed
    public ResponseEntity<DocumententryDTO> createDocumententry(@Valid @RequestBody DocumententryDTO documententryDTO) throws URISyntaxException {
        log.debug("REST request to save Documententry : {}", documententryDTO);
        if (documententryDTO.getId() != null) {
            throw new BadRequestAlertException("A new documententry cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DocumententryDTO result = documententryService.save(documententryDTO);
        return ResponseEntity.created(new URI("/api/documententries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /documententries : Updates an existing documententry.
     *
     * @param documententryDTO the documententryDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated documententryDTO,
     * or with status 400 (Bad Request) if the documententryDTO is not valid,
     * or with status 500 (Internal Server Error) if the documententryDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/documententries")
    @Timed
    public ResponseEntity<DocumententryDTO> updateDocumententry(@Valid @RequestBody DocumententryDTO documententryDTO) throws URISyntaxException {
        log.debug("REST request to update Documententry : {}", documententryDTO);
        if (documententryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DocumententryDTO result = documententryService.save(documententryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, documententryDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /documententries : get all the documententries.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of documententries in body
     */
    @GetMapping("/documententries")
    @Timed
    public ResponseEntity<List<DocumententryDTO>> getAllDocumententries(DocumententryCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Documententries by criteria: {}", criteria);
        Page<DocumententryDTO> page = documententryQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/documententries");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * GET  /documententries/count : count all the documententries.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/documententries/count")
    @Timed
    public ResponseEntity<Long> countDocumententries(DocumententryCriteria criteria) {
        log.debug("REST request to count Documententries by criteria: {}", criteria);
        return ResponseEntity.ok().body(documententryQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /documententries/:id : get the "id" documententry.
     *
     * @param id the id of the documententryDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the documententryDTO, or with status 404 (Not Found)
     */
    @GetMapping("/documententries/{id}")
    @Timed
    public ResponseEntity<DocumententryDTO> getDocumententry(@PathVariable Long id) {
        log.debug("REST request to get Documententry : {}", id);
        Optional<DocumententryDTO> documententryDTO = documententryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(documententryDTO);
    }

    /**
     * DELETE  /documententries/:id : delete the "id" documententry.
     *
     * @param id the id of the documententryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/documententries/{id}")
    @Timed
    public ResponseEntity<Void> deleteDocumententry(@PathVariable Long id) {
        log.debug("REST request to delete Documententry : {}", id);
        documententryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
