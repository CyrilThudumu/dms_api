package com.dms.web.rest;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dms.domain.Documents;
import com.dms.repository.ReportsRepository;
import com.dms.service.ReportsService;
import com.dms.service.dto.ReportsCriteria;
import com.dms.service.dto.ReportsDTO;

/**
 * REST controller for managing Reports.
 */
@RestController
@RequestMapping("/api")
public class ReportsResource {

    private final Logger log = LoggerFactory.getLogger(ReportsResource.class);

    private static final String ENTITY_NAME = "certifier";

    private final ReportsService reportsService;
    
    @Autowired
    private ReportsRepository reportsRepository;
    

    public ReportsResource(ReportsService reportsService) {
    	this.reportsService = reportsService;
    }

 
    @PostMapping(value = "/masterReport")
    @ResponseBody
    public List<Documents> search(@RequestBody ReportsDTO reportsDTO) {
        ReportsCriteria builder = new ReportsCriteria();
        Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
        Matcher matcher = pattern.matcher(reportsDTO + ",");
        while (matcher.find()) {
            builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
        }
         
        Specification<Documents> spec = builder.build();
        return reportsRepository.findAll(spec);
    }
    
    
    /**
     * GET  /Reports : get content of the given Search Criteria.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of certifier in body
     */
 /*   @PostMapping("/getMasterReport")
    @Timed
    public ResponseEntity<List<ReportsDTO>> getPatientMasterReport(@RequestBody ReportsDTO reportsDTO, Pageable pageable) {
        log.debug("REST request to generate Master Report for a Patient: {}", reportsDTO);
        Page<ReportsDTO> page = reportsService.findByCriteria(reportsDTO, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/getMasterReport");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
*/
  
}
