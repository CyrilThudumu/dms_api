package com.dms.web.rest;

import io.github.jhipster.web.util.ResponseUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.dms.service.PracticeQueryService;
import com.dms.service.PracticeService;
import com.dms.service.dto.PracticeCriteria;
import com.dms.service.dto.PracticeDTO;
import com.dms.web.rest.errors.BadRequestAlertException;
import com.dms.web.rest.util.HeaderUtil;
import com.dms.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Practice.
 */
@RestController
@RequestMapping("/api")
public class PracticeResource {

	private final Logger log = LoggerFactory.getLogger(PracticeResource.class);

	private static final String ENTITY_NAME = "practice";

	private final PracticeService practiceService;

	private final PracticeQueryService practiceQueryService;

	public PracticeResource(PracticeService practiceService, PracticeQueryService practiceQueryService) {
		this.practiceService = practiceService;
		this.practiceQueryService = practiceQueryService;
	}

	/**
	 * POST /practices : Create a new practice.
	 *
	 * @param practiceDTO the practiceDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         practiceDTO, or with status 400 (Bad Request) if the practice has
	 *         already an ID
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	
	
	@PostMapping("/practices")
	@Timed
	@Bean(name = "multipartResolver")
	public ResponseEntity<PracticeDTO> createPractice(@Valid @RequestBody PracticeDTO practiceDTO)
			throws URISyntaxException {
		log.debug("REST request to save Practice : {}", practiceDTO);

		if (practiceDTO.getId() != null) {
			throw new BadRequestAlertException("A new practice cannot already have an ID", ENTITY_NAME, "idexists");
		}
		PracticeDTO result = practiceService.save(practiceDTO);
		return ResponseEntity.created(new URI("/api/practices/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /practices : Updates an existing practice.
	 *
	 * @param practiceDTO the practiceDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         practiceDTO, or with status 400 (Bad Request) if the practiceDTO is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         practiceDTO couldn't be updated
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PutMapping("/practices")
	@Timed
	public ResponseEntity<PracticeDTO> updatePractice(@Valid @RequestBody PracticeDTO practiceDTO)
			throws URISyntaxException {
		log.debug("REST request to update Practice : {}", practiceDTO);
		if (practiceDTO.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		PracticeDTO result = practiceService.save(practiceDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, practiceDTO.getId().toString())).body(result);
	}

	/**
	 * GET /practices : get all the practices.
	 *
	 * @param pageable the pagination information
	 * @param criteria the criterias which the requested entities should match
	 * @return the ResponseEntity with status 200 (OK) and the list of practices in
	 *         body
	 */
	@GetMapping("/practices")
	@Timed
	public ResponseEntity<List<PracticeDTO>> getAllPractices(PracticeCriteria criteria, Pageable pageable) {
		log.debug("REST request to get Practices by criteria: {}", criteria);
		Page<PracticeDTO> page = practiceQueryService.findByCriteria(criteria, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/practices");
		return ResponseEntity.ok().headers(headers).body(page.getContent());
	}

	/**
	 * GET /practices/count : count all the practices.
	 *
	 * @param criteria the criterias which the requested entities should match
	 * @return the ResponseEntity with status 200 (OK) and the count in body
	 */
	@GetMapping("/practices/count")
	@Timed
	public ResponseEntity<Long> countPractices(PracticeCriteria criteria) {
		log.debug("REST request to count Practices by criteria: {}", criteria);
		return ResponseEntity.ok().body(practiceQueryService.countByCriteria(criteria));
	}

	/**
	 * GET /practices/:id : get the "id" practice.
	 *
	 * @param id the id of the practiceDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         practiceDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/practices/{id}")
	@Timed
	public @ResponseBody ResponseEntity<PracticeDTO> getPractice(@PathVariable("id") Long id) {
		log.debug("REST request to get Practice : {}", id);
		Optional<PracticeDTO> practiceDTO = practiceService.findOne(id);
		return ResponseUtil.wrapOrNotFound(practiceDTO);
	}
	
	/**
	 * GET /practices/:practiceName : get the "id" practice.
	 *
	 * @param id the id of the practiceDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         practiceDTO, or with status 404 (Not Found)
	 */
	/*@GetMapping("/practice/{practiceName}")
	@Timed
	public @ResponseBody ResponseEntity<PracticeDTO> getPracticCode(@PathVariable("practiceName") String practiceName) {
		log.debug("REST request to get Practice : {}", practiceName);
		Optional<PracticeDTO> practiceDTO = practiceService.findByPracticeName(practiceName);
		return ResponseUtil.wrapOrNotFound(practiceDTO);
	}*/
	
	
	
	/**
	 * GET /practices/:searchKey : get the "searchKey" practice.
	 *
	 * @param searchKey the searchKey of the practiceDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         practiceDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/practice/{searchKey}")
	@Timed
	public @ResponseBody ResponseEntity<List<PracticeDTO>> getPracticeInfo(@PathVariable("searchKey") String pracName, Pageable page) {
		log.debug("REST request to get Practice Info : {}", pracName);
		Page<PracticeDTO> practiceDTO = practiceService.findByPracticeName(pracName,page);
		return new ResponseEntity<>(practiceDTO.getContent(), HttpStatus.OK);
	}
	

	/**
	 * DELETE /practices/:id : delete the "id" practice.
	 *
	 * @param id the id of the practiceDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/practices/{id}")
	@Timed

	public @ResponseBody ResponseEntity<Void> deletePractice(@PathVariable("id") Long id) {
		log.debug("REST request to delete Practice : {}", id);
		practiceService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
	/**
	 * GET /practicesSearch/:practiceName : get the all details of the practice.
	 *
	 * @param practiceName the practiceName of the practiceDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         practiceDTO, or with status 404 (Not Found)
	 */
	/*@GetMapping("/practicesSearch/{practiceName}")
	@Timed
	public @ResponseBody ResponseEntity<PracticeDTO> getPracticeInfo(@PathVariable("practiceName") String practiceName) {
		log.debug("REST request to get Practice : {}", practiceName);
		Optional<PracticeDTO> practiceDTO = practiceService.findPracticeInfo(practiceName);
		return ResponseUtil.wrapOrNotFound(practiceDTO);
	}*/

}
