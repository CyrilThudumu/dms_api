package com.dms.web.rest;

import io.github.jhipster.web.util.ResponseUtil;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.dms.domain.Patient;
import com.dms.security.AuthoritiesConstants;
import com.dms.service.PatientQueryService;
import com.dms.service.PatientService;
import com.dms.service.dto.DocumentsDTO;
import com.dms.service.dto.PatientCriteria;
import com.dms.service.dto.PatientDTO;
import com.dms.service.util.PatientUtil;
import com.dms.web.rest.errors.BadRequestAlertException;
import com.dms.web.rest.util.HeaderUtil;
import com.dms.web.rest.util.PaginationUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * REST controller for managing Patient.
 */
@RestController
@RequestMapping("/api")
public class PatientResource {

	private final Logger log = LoggerFactory.getLogger(PatientResource.class);

	private static final String ENTITY_NAME = "patient";

	private final PatientService patientService;

	private final PatientQueryService patientQueryService;

	public PatientResource(PatientService patientService, PatientQueryService patientQueryService) {
		this.patientService = patientService;
		this.patientQueryService = patientQueryService;
	}

	@GetMapping("/findPatientByCode/{patientCode}")
	@Timed
	public @ResponseBody ResponseEntity<String> findPatientByCode(@PathVariable String patientCode) throws URISyntaxException, IOException {
		if(patientCode != null){
			Optional<PatientDTO> patient = patientService.getPatientByCode(patientCode);
			if(patient.isPresent()){
				if(patient.get().getPatientCode() != null){
					throw new BadRequestAlertException("Patient is already exists with this patient Code", ENTITY_NAME, "patient is already exists");
				}
			}
			return new ResponseEntity<>(HttpStatus.OK);
			
		}else{throw new BadRequestAlertException("Please Enter Patient Code patient Id", ENTITY_NAME, "Please enter patientCode");}
		
	}
	
	@GetMapping("/findPatientInfo/{patientCode}/{fName}/{LName}/{DOB}")
	@Timed
	public @ResponseBody ResponseEntity<String> findIfPatientExists(@PathVariable String patientCode, @PathVariable String fName, 
			@PathVariable String LName, @PathVariable LocalDate DOB ) throws URISyntaxException, IOException {
		
		log.debug("fName: "+fName);
		log.debug("LName: "+LName);
		log.debug("dob: "+DOB);
		log.debug("patientCode: "+patientCode);
		/*
		String patientCode = params.get("patientCode").toString();
		String fName = params.get("firstName").toString();
		String lName = params.get("LastName").toString();
		String DOB = params.get("DOB").toString();*/
		
		if(patientCode != null){
			Optional<PatientDTO> patient = patientService.getPatientByCode(patientCode);
			log.debug("FromDB: ");
			log.debug("FName: "+patient.get().getFirstName());
			log.debug("LName:  "+patient.get().getLastName());
			log.debug("DOB:  "+patient.get().getDob());
			log.debug("patientCode:  "+patient.get().getPatientCode());
			log.debug("1: "+(patient.get().getFirstName().equalsIgnoreCase(fName)));
			log.debug("2:  "+(patient.get().getLastName().equalsIgnoreCase(LName)));
			log.debug("3:  "+(patient.get().getDob().equals(DOB)));
			if(patient.isPresent()){
				if((patient.get().getFirstName().equalsIgnoreCase(fName)) &&
						(patient.get().getLastName().equalsIgnoreCase(LName)) && 
						(patient.get().getDob().equals(DOB))){
						
							throw new BadRequestAlertException("Patient is already exists with this given patient Info", ENTITY_NAME, "Patient already exists");
						}
				
			}
			return new ResponseEntity<>(HttpStatus.OK);	
						
		}else{throw new BadRequestAlertException("Please Enter Patient Code patient Id", ENTITY_NAME, "Please enter patientCode");}
		
	}
	
	
	/**
	 * POST /patients : Create a new patient.
	 *
	 * @param patientDTO the patientDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         patientDTO, or with status 400 (Bad Request) if the patient has
	 *         already an ID
	 * @throws URISyntaxException   if the Location URI syntax is incorrect
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */

	
	
	@PostMapping(value = "/patients")
	@Timed
	public @ResponseBody ResponseEntity<PatientDTO> createPatient(@RequestBody String JSONArray) throws URISyntaxException, IOException {
		log.debug("REST request to save patient Types:"+JSONArray);

		ObjectMapper mapper = new ObjectMapper();
		PatientDTO patientDTO = mapper.readValue(JSONArray, PatientDTO.class);
		log.debug("patientDTO is: "+patientDTO);
		if (patientDTO.getId() != null) {
			throw new BadRequestAlertException("A new patient cannot already have an ID", ENTITY_NAME, "idexists");
		}
	
		/**/
		PatientDTO result = patientService.save(patientDTO);
		//PatientUtil patientUtil = new PatientUtil();
		//patientUtil.savePatietDemographic(patientDTO, file);
		return ResponseEntity.created(new URI("/api/patients/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}
	/**
	 * PUT /patients : Updates an existing patient.
	 *
	 * @param patientDTO the patientDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         patientDTO, or with status 400 (Bad Request) if the patientDTO is not
	 *         valid, or with status 500 (Internal Server Error) if the patientDTO
	 *         couldn't be updated
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PutMapping("/patients")
	@Timed
	@PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
	public ResponseEntity<PatientDTO> updatePatient(@Valid @RequestBody PatientDTO patientDTO)
			throws URISyntaxException {
		log.debug("REST request to update Patient : {}", patientDTO);
		if (patientDTO.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		PatientDTO result = patientService.save(patientDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, patientDTO.getId().toString())).body(result);
	}

	/**
	 * GET /patients : get all the patients.
	 *
	 * @param pageable the pagination information
	 * @param criteria the criteria which the requested entities should match
	 * @return the ResponseEntity with status 200 (OK) and the list of patients in
	 *         body
	 */
	@GetMapping("/patients")
	@Timed
	public ResponseEntity<List<PatientDTO>> getAllPatients(PatientCriteria criteria, Pageable pageable) {
		log.debug("REST request to get Patients by criteria: {}", criteria);
		Page<PatientDTO> page = patientQueryService.findByCriteria(criteria, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/patients");
		return ResponseEntity.ok().headers(headers).body(page.getContent());
	}

	/**
	 * GET /patients/count : count all the patients.
	 *
	 * @param criteria the criteria which the requested entities should match
	 * @return the ResponseEntity with status 200 (OK) and the count in body
	 */
	@GetMapping("/patients/count")
	@Timed
	public ResponseEntity<Long> countPatients(PatientCriteria criteria) {
		log.debug("REST request to count Patients by criteria: {}", criteria);
		return ResponseEntity.ok().body(patientQueryService.countByCriteria(criteria));
	}

	/**
	 * GET /patients/:id : get the "id" patient.
	 *
	 * @param id the id of the patientDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the patientDTO,
	 *         or with status 404 (Not Found)
	 */
	@GetMapping("/patients/{id}")
	@Timed
	public ResponseEntity<PatientDTO> getPatient(@PathVariable Long id) {
		log.debug("REST request to get Patient : {}", id);
		Optional<PatientDTO> patientDTO = patientService.findOne(id);
		return ResponseUtil.wrapOrNotFound(patientDTO);
	}
	
	
	
	 @GetMapping("/patientslist")
	    @Timed
	    public ResponseEntity<List<PatientDTO>> getPatientListBySearchCriteria(PatientCriteria criteria, Pageable pageable) {
	        log.debug("REST request to get Patients by Search criteria: {}", criteria);
	        Page<PatientDTO> page = patientQueryService.findByCriteria(criteria, pageable);
	        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/patients");
	        return ResponseEntity.ok().headers(headers).body(page.getContent());
	    }
	 
	 /**
		 * GET /patientdemographic/:practiceCode/:patientCode : get "practiceCode" "patientCode" of patient.
		 *
		 * @param practiceCode the practiceCode of the Patient from param
		 * @param patientCode the patientCode of the Patient from param
		 * @return the ResponseEntity with status 200 (OK)
		 */
	 @GetMapping("/patientdemographic/{practiceCode}/{patientCode}")
	 @Timed
	 public DocumentsDTO getPatientDemographic(@PathVariable String practiceCode, @PathVariable String patientCode) throws IOException {
 
	     PatientUtil patientUtil = new PatientUtil();	
	     File fileObje= patientUtil.getPatietDemographic(practiceCode, patientCode);
	     DocumentsDTO docsDTO = new DocumentsDTO();
	     //byte[] fileContent = Files.readAllBytes(fileObje.toPath());
	     
	     byte[] fileContent = Files.readAllBytes(fileObje.toPath());
	     docsDTO.setFileContent(fileContent);
	     HttpHeaders headers = new HttpHeaders();
	     headers.setContentType(MediaType.APPLICATION_PDF);
	     log.info("file name is: "+fileObje.getName());
	     String filename = fileObje.getName();
	     headers.setContentDispositionFormData(filename,filename);
	     headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
	    // ResponseEntity<byte[]> response = new ResponseEntity<>(fileContent, headers, HttpStatus.OK);
	     return docsDTO;
	 }
	 
	 @GetMapping("/patientdemographictest/{practiceCode}/{patientCode}")
	 @Timed
	 public ResponseEntity<byte[]> getPDF(@PathVariable String practiceCode, @PathVariable String patientCode) throws IOException {
 
		 log.debug("Practice Code {}", practiceCode);
		 log.debug("PatientCode: {}", patientCode);
	     PatientUtil patientUtil = new PatientUtil();	
	     
	     File fileObje= patientUtil.getPatietDemographic(practiceCode, patientCode);
	     log.debug("Practice Code After {}", practiceCode);
		 log.debug("Patient Code After: {}", patientCode);
	     byte[] fileContent = Files.readAllBytes(fileObje.toPath());
	     HttpHeaders headers = new HttpHeaders();
	     headers.setContentType(MediaType.APPLICATION_PDF);
	     log.info("file name is: "+fileObje.getName());
	     String filename = fileObje.getName();
	     headers.setContentDispositionFormData(filename,filename);
	     headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
	     ResponseEntity<byte[]> response = new ResponseEntity<>(fileContent, headers, HttpStatus.OK);
	     return response;
	 }
	

	 
	 /**
	 * DELETE /patients/:id : delete the "id" patient.
	 *
	 * @param id the id of the patientDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/patients/{id}")
	@Timed
	public ResponseEntity<Void> deletePatient(@PathVariable Long id) {
		log.debug("REST request to delete Patient : {}", id);
		patientService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
	
	/**
	 * GET /patientsAttorney/:attorneyCode : GET the "attorneyCode" patient.
	 *
	 * @param id the attorneyCode of the patientDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@GetMapping("/patientsAttorney/{attorneyCode}")
	@Timed
	public ResponseEntity<List<PatientDTO>> getAttorneyPatients(@PathVariable String attorneyCode, Pageable page) {
		log.debug("REST request to get Attorney Patients: {}", attorneyCode);
		if (attorneyCode == null) {
			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
		}
		
		Page<PatientDTO> patientDTO = patientService.getAttorneyPatients(attorneyCode, page);
		return new ResponseEntity<>(patientDTO.getContent(), HttpStatus.OK);
	}
	
	/**
	 * GET /patientsAttorney/:attorneyCode : GET the "attorneyCode" patient.
	 *
	 * @param id the attorneyCode of the patientDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@GetMapping("/patientsProcessedByAttorney/{attorneyCode}")
	@Timed
	public ResponseEntity<List<PatientDTO>> patientsProcessedByAttorney(@PathVariable String attorneyCode, Pageable page) {
		log.debug("REST request to get Attorney Patients: {}", attorneyCode);
		if (attorneyCode == null) {
			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
		}
		
		Page<PatientDTO> patientDTO = patientService.patientsProcessedByAttorney(attorneyCode, page);
		return new ResponseEntity<>(patientDTO.getContent(), HttpStatus.OK);
	}
	
	
	
	
	/**
	 * GET /patientsByPractice/:practiceCode : GET the list of patients.
	 *
	 * @param practiceCode the practiceCode of the patientDTO 
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@GetMapping("/patientsByPractice/{practiceCode}")
	@Timed
	public ResponseEntity<List<PatientDTO>> getPatientsForPractice(@PathVariable String practiceCode, Pageable page) {
		log.debug("REST request to get  Patients for the : {}", practiceCode);
		if (practiceCode == null) {
			throw new BadRequestAlertException("Invalid Practice Code", ENTITY_NAME, "practiceCode is null");
		}
		
		Page<PatientDTO> patientDTO = patientService.getPatientsForPractice(practiceCode, page);
		return new ResponseEntity<>(patientDTO.getContent(), HttpStatus.OK);
	}
	
	/**
	 * GET /patientsForDoctor/:doctorCode : GET the list of patients.
	 *
	 * @param doctorCode the doctorCode of the patientDTO 
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@GetMapping("/patientsForDoctor/{doctorCode}/{searchKey}")
	@Timed
	public ResponseEntity<List<PatientDTO>> docPatientSearch(@PathVariable String doctorCode, @PathVariable String searchKey,Pageable page) {
		log.debug("REST request to get  Patients for the : {}", doctorCode,searchKey );
		if (doctorCode == null) {
			throw new BadRequestAlertException("Invalid Doctor Code", ENTITY_NAME, "doctorCode is null");
		}
		if (searchKey == null) {
			throw new BadRequestAlertException("Search key must not be null", ENTITY_NAME, "search Key is null");
		}

		
		Page<PatientDTO> patientDTO = patientService.getDocPatientSearch(doctorCode, searchKey,page);
		return new ResponseEntity<>(patientDTO.getContent(), HttpStatus.OK);
	}
	
	
	
	/**
	 * GET /patientsForAttorney/:attorneyCode : GET the list of patients.
	 *
	 * @param attorneyCode the attorneyCode of the patientDTO 
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@GetMapping("/patientsForAttorney/{attorneyCode}/{searchKey}")
	@Timed
	public ResponseEntity<List<PatientDTO>> attorneyPatientSearch(@PathVariable String attorneyCode, @PathVariable String searchKey,Pageable page) {
		log.debug("REST request to get  Patients for the  : {}", attorneyCode,searchKey );
		if (attorneyCode == null) {
			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "attorneyCode is null");
		}
		if (searchKey == null) {
			throw new BadRequestAlertException("Search key must not be null", ENTITY_NAME, "search Key is null");
		}

		Page<PatientDTO> patientDTO = patientService.getAttorneyPatientSearch(attorneyCode, searchKey,page);
		return new ResponseEntity<>(patientDTO.getContent(), HttpStatus.OK);
	}
	
	
	
	
	/*@GetMapping("/patientsAttorney/{attorneyCode}")
	@Timed
	public Page<PatientDTO> getAttorneyPatients(@PathVariable String attorneyCode) {
		log.debug("REST request to get Attorney Patients: {}", attorneyCode);
		if (attorneyCode == null) {
			throw new BadRequestAlertException("Invalid Attorney Code", ENTITY_NAME, "AttorneyCode is null");
		}
		
		Page<PatientDTO> patientDTO = patientService.getAttorneyPatients(attorneyCode);
		return patientDTO;
	}*/
	
	/**
	 * GET /patientsDoctor/:doctorCode : GET the "doctorCode" patient.
	 *
	 * @param id the attorneyCode of the patientDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@GetMapping("/patientsDoctor/{doctorCode}")
	@Timed
	public ResponseEntity<List<PatientDTO>> getDoctorPatients(@PathVariable String doctorCode, Pageable page) {
		log.debug("REST request to get Attorney Patients: {}", doctorCode, page);
		if (doctorCode == null) {
			throw new BadRequestAlertException("Invalid Doctor Code", ENTITY_NAME, "DoctorCode is null");
		}
		
		Page<PatientDTO> patientDTO = patientService.getDoctorPatients(doctorCode, page);
		return new ResponseEntity<>(patientDTO.getContent(), HttpStatus.OK);
	}
	
	
	/**
	 * GET /attorneyPatientsHistory/:attorneyCode : GET the "attorneyCode" patient.
	 *
	 * @param id the attorneyCode of the patientDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@GetMapping("/patientByCode/{patientCode}")
	@Timed
	public Optional<PatientDTO> getPatientInfoByCode(@PathVariable String patientCode) {
		log.debug("REST request to get Attorney Patients: {}", patientCode);
		if (patientCode == null) {
			throw new BadRequestAlertException("Invalid patient Code ", ENTITY_NAME, "patientCode is null");
		}
		
		Optional<PatientDTO> patientDTO = patientService.getPatientByCode(patientCode);
		return patientDTO;
	}
	/**
	 * GET /patientInfoForAttorney/:attorneyCode : get the "id" patient.
	 *
	 * @param id the id of the patientDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the patientDTO,
	 *         or with status 404 (Not Found)
	 */
	
	@GetMapping("/patientInfoForAttorney/{attorneyCode}")
	@Timed
	public ResponseEntity<PatientDTO> getPatientInfo(@PathVariable String attorneyCode) {
		log.debug("REST request to get Patient : {}", attorneyCode);
		PatientDTO patientDTO = patientService.findOneByPatientCode(attorneyCode);
		 
		return new ResponseEntity<>(patientDTO, HttpStatus.OK);
	}
	
	   @GetMapping(value = "/patientSearch")
	    @ResponseBody
	    public List<Patient> findAllPatients(@RequestBody Patient patient) {
	        
		   return patientQueryService.searchPatient(patient);
	        
	    }
	
}
