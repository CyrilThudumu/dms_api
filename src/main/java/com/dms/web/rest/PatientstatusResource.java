package com.dms.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.dms.service.PatientstatusService;
import com.dms.web.rest.errors.BadRequestAlertException;
import com.dms.web.rest.util.HeaderUtil;
import com.dms.web.rest.util.PaginationUtil;
import com.dms.service.dto.PatientstatusDTO;
import com.dms.service.dto.PatientstatusCriteria;
import com.dms.service.PatientstatusQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Patientstatus.
 */
@RestController
@RequestMapping("/api")
public class PatientstatusResource {

    private final Logger log = LoggerFactory.getLogger(PatientstatusResource.class);

    private static final String ENTITY_NAME = "patientstatus";

    private final PatientstatusService patientstatusService;

    private final PatientstatusQueryService patientstatusQueryService;

    public PatientstatusResource(PatientstatusService patientstatusService, PatientstatusQueryService patientstatusQueryService) {
        this.patientstatusService = patientstatusService;
        this.patientstatusQueryService = patientstatusQueryService;
    }

    /**
     * POST  /patientstatuses : Create a new patientstatus.
     *
     * @param patientstatusDTO the patientstatusDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new patientstatusDTO, or with status 400 (Bad Request) if the patientstatus has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/patientstatuses")
    @Timed
    public ResponseEntity<PatientstatusDTO> createPatientstatus(@Valid @RequestBody PatientstatusDTO patientstatusDTO) throws URISyntaxException {
        log.debug("REST request to save Patientstatus : {}", patientstatusDTO);
        if (patientstatusDTO.getId() != null) {
            throw new BadRequestAlertException("A new patientstatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PatientstatusDTO result = patientstatusService.save(patientstatusDTO);
        return ResponseEntity.created(new URI("/api/patientstatuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /patientstatuses : Updates an existing patientstatus.
     *
     * @param patientstatusDTO the patientstatusDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated patientstatusDTO,
     * or with status 400 (Bad Request) if the patientstatusDTO is not valid,
     * or with status 500 (Internal Server Error) if the patientstatusDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/patientstatuses")
    @Timed
    public ResponseEntity<PatientstatusDTO> updatePatientstatus(@Valid @RequestBody PatientstatusDTO patientstatusDTO) throws URISyntaxException {
        log.debug("REST request to update Patientstatus : {}", patientstatusDTO);
        if (patientstatusDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PatientstatusDTO result = patientstatusService.save(patientstatusDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, patientstatusDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /patientstatuses : get all the patientstatuses.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of patientstatuses in body
     */
    @GetMapping("/patientstatuses")
    @Timed
    public ResponseEntity<List<PatientstatusDTO>> getAllPatientstatuses(PatientstatusCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Patientstatuses by criteria: {}", criteria);
        Page<PatientstatusDTO> page = patientstatusQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/patientstatuses");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * GET  /patientstatuses/count : count all the patientstatuses.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/patientstatuses/count")
    @Timed
    public ResponseEntity<Long> countPatientstatuses(PatientstatusCriteria criteria) {
        log.debug("REST request to count Patientstatuses by criteria: {}", criteria);
        return ResponseEntity.ok().body(patientstatusQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /patientstatuses/:id : get the "id" patientstatus.
     *
     * @param id the id of the patientstatusDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the patientstatusDTO, or with status 404 (Not Found)
     */
    @GetMapping("/patientstatuses/{id}")
    @Timed
    public ResponseEntity<PatientstatusDTO> getPatientstatus(@PathVariable Long id) {
        log.debug("REST request to get Patientstatus : {}", id);
        Optional<PatientstatusDTO> patientstatusDTO = patientstatusService.findOne(id);
        return ResponseUtil.wrapOrNotFound(patientstatusDTO);
    }

    /**
     * DELETE  /patientstatuses/:id : delete the "id" patientstatus.
     *
     * @param id the id of the patientstatusDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/patientstatuses/{id}")
    @Timed
    public ResponseEntity<Void> deletePatientstatus(@PathVariable Long id) {
        log.debug("REST request to delete Patientstatus : {}", id);
        patientstatusService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
