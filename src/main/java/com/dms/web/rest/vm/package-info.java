/**
 * View Models used by Spring MVC REST controllers.
 */
package com.dms.web.rest.vm;
