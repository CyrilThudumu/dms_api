package com.dms.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.dms.service.EdocumentQueryService;
import com.dms.service.EdocumentService;
import com.dms.service.dto.EdocumentCriteria;
import com.dms.service.dto.EdocumentDTO;
import com.dms.web.rest.errors.BadRequestAlertException;
import com.dms.web.rest.util.HeaderUtil;
import com.dms.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Edocument.
 */
@RestController
@RequestMapping("/api")
public class EdocumentResource {

    private final Logger log = LoggerFactory.getLogger(EdocumentResource.class);

    private static final String ENTITY_NAME = "edocument";

    private final EdocumentService edocumentService;

    private final EdocumentQueryService edocumentQueryService;

    public EdocumentResource(EdocumentService edocumentService, EdocumentQueryService edocumentQueryService) {
        this.edocumentService = edocumentService;
        this.edocumentQueryService = edocumentQueryService;
    }

    /**
     * POST  /edocuments : Create a new edocument.
     *
     * @param edocumentDTO the edocumentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new edocumentDTO, or with status 400 (Bad Request) if the edocument has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/edocuments")
    @Timed
    public ResponseEntity<EdocumentDTO> createEdocument(@Valid @RequestBody EdocumentDTO edocumentDTO) throws URISyntaxException {
        log.debug("REST request to save Edocument : {}", edocumentDTO);
        if (edocumentDTO.getId() != null) {
            throw new BadRequestAlertException("A new edocument cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EdocumentDTO result = edocumentService.save(edocumentDTO);
        return ResponseEntity.created(new URI("/api/edocuments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /edocuments : Updates an existing edocument.
     *
     * @param edocumentDTO the edocumentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated edocumentDTO,
     * or with status 400 (Bad Request) if the edocumentDTO is not valid,
     * or with status 500 (Internal Server Error) if the edocumentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/edocuments")
    @Timed
    public ResponseEntity<EdocumentDTO> updateEdocument(@Valid @RequestBody EdocumentDTO edocumentDTO) throws URISyntaxException {
        log.debug("REST request to update Edocument : {}", edocumentDTO);
        if (edocumentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EdocumentDTO result = edocumentService.save(edocumentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, edocumentDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /edocuments : get all the edocuments.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of edocuments in body
     */
    @GetMapping("/edocuments")
    @Timed
    public ResponseEntity<List<EdocumentDTO>> getAllEdocuments(EdocumentCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Edocuments by criteria: {}", criteria);
        Page<EdocumentDTO> page = edocumentQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/edocuments");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * GET  /edocuments/count : count all the edocuments.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/edocuments/count")
    @Timed
    public ResponseEntity<Long> countEdocuments(EdocumentCriteria criteria) {
        log.debug("REST request to count Edocuments by criteria: {}", criteria);
        return ResponseEntity.ok().body(edocumentQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /edocuments/:id : get the "id" edocument.
     *
     * @param id the id of the edocumentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the edocumentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/edocuments/{id}")
    @Timed
    public ResponseEntity<EdocumentDTO> getEdocument(@PathVariable Long id) {
        log.debug("REST request to get Edocument : {}", id);
        Optional<EdocumentDTO> edocumentDTO = edocumentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(edocumentDTO);
    }

    /**
     * DELETE  /edocuments/:id : delete the "id" edocument.
     *
     * @param id the id of the edocumentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/edocuments/{id}")
    @Timed
    public ResponseEntity<Void> deleteEdocument(@PathVariable Long id) {
        log.debug("REST request to delete Edocument : {}", id);
        edocumentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    
}
