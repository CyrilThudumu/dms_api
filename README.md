# DMSApi

You can test the rest api :

(1)
POST http://localhost:8080/api/authenticate 
Pass Header :
'Content-Type: application/json' --header 'Accept: */*' 
Pass Payload :
<code>
{
  "password": "admin",
  "rememberMe": true,
  "username": "admin"
}
</code>
<br>You should see a token in response header :
Ex: 
<pre>
eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTU1NDc0NTg1Mn0.anQ0eAIltw16WaygIc_NXBb0BWC8GX9srQ9FkGmrnBYuFt8WRqC669XIqjugaQ8-JZSgh5NX8vTVW-TVcvIYqQ
</pre>

Then in the subsequent Rest API's we need to pass this token in header

(2)

GET http://localhost:8080/api/users

Pass Header:
"Authorization": "Bearer 
eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTU1NDc0NTg1Mn0.anQ0eAIltw16WaygIc_NXBb0BWC8GX9srQ9FkGmrnBYuFt8WRqC669XIqjugaQ8-JZSgh5NX8vTVW-TVcvIYqQ"

Then we get output :
<pre>
[ {
  "id" : 1,
  "login" : "system",
  "firstName" : "System",
  "lastName" : "System",
  "email" : "system@localhost",
  "imageUrl" : "",
  "activated" : true,
  "langKey" : "en",
  "createdBy" : "system",
  "createdDate" : null,
  "lastModifiedBy" : "system",
  "lastModifiedDate" : null,
  "authorities" : [ "ROLE_USER", "ROLE_ADMIN" ]
}, {
  "id" : 3,
  "login" : "admin",
  "firstName" : "Administrator",
  "lastName" : "Administrator",
  "email" : "admin@localhost",
  "imageUrl" : "",
  "activated" : true,
  "langKey" : "en",
  "createdBy" : "system",
  "createdDate" : null,
  "lastModifiedBy" : "system",
  "lastModifiedDate" : null,
  "authorities" : [ "ROLE_USER", "ROLE_ADMIN" ]
}, {
  "id" : 4,
  "login" : "user",
  "firstName" : "User",
  "lastName" : "User",
  "email" : "user@localhost",
  "imageUrl" : "",
  "activated" : true,
  "langKey" : "en",
  "createdBy" : "system",
  "createdDate" : null,
  "lastModifiedBy" : "system",
  "lastModifiedDate" : null,
  "authorities" : [ "ROLE_USER" ]
} ]
</pre>